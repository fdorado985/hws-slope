<p align="center">
  <img src=".assets/logo.png" title="Logo">
</p>

---

[![platform_ios](.assets/platform.png)](https://www.apple.com/ios/ios-11-preview/)
[![ide_xcode9](.assets/ide.png)](https://developer.apple.com/xcode)
[![twitter](.assets/twitter.png)](https://twitter.com/fdorado985)
[![instagram](.assets/instagram.png)](https://www.instagram.com/juan_fdorado)

This collection of apps have been worked through the amazing collection of Paul... Hacking With Swift, this collection covers different books.

I invite you to take a look at them

Here are the projects and a little description of what are they made it for. Enjoy it and most important KEEP CODING!

All of this project has been worked day by day, month by month learning new stuffs from iOS Development from different sources.

So here is a big collection! 👍

## Hacking With Swift - Main Book
* [Storm-Viewer](projects/hws_book/storm-viewer) - The app will let you see an image from a list, this cover the basic topics of Swift
* [Guess-The-Flag](projects/hws_book/guess-the-flag) - This app is an easy game to guess the correct flag of three images, this covers, a little bit more components on Swift.
* [Social-Media](projects/hws_book/social-media-viewer) - This app is an update of the [Storm Viewer](projects/hws_book/storm-viewer) app to add a way to share images.
* [Easy-Browser](projects/hws_book/easy-browser) - This app let you open a website using WKWebKit, also its delegates and progressView.
* [Word-Scramble](projects/hws_book/word-scramble) - This is an easy game where you type a word with the letters of a given word, `Closures`, `Booleans`, `TextView` in `AlertController`.
* [Guess-The-Flag-AutoLayout](projects/hws_book/guess-the-flag-autolayout) - Copy of [Guess The Flag](projects/hws_book/guess-the-flag) project, but now able to use it on landscape mode... all this using `AutoLayout`.
* [AutoLayout-in-code](projects/hws_book/autolayout-in-code) - This is an easy example using labels to work with `AutoLayout` but... BY CODE.
* [Whitehouse-Petitions](projects/hws_book/whitehouse-petitions) - You will get some petitions from the Whitehouse API, you will know how to handle `JSON`, `UITableViewController`, `UINavigationController` and `UITabBarController`.
* [Swifty-Words](projects/hws_book/swifty-words) - Build a word-guessing game and master strings once and for all.
* [Whitehouse-Petitions-GCD](projects/hws_book/whitehouse-petitions-gcd) - This is a copy of the [whitehouse-petitions](projects/hws_book/whitehouse-petitions) project but using GCD to handle the API petitions.
* [Names-To-Faces](projects/hws_book/names-to-faces) - This app will save images and names to each, here we use `UUID`, `UICollectionViewController` and `UIImageViewController`.
* [Pachinko](projects/hws_book/pachinko) - This is the first `SpriteKit` game of these project section, here you will see, `SKPhysicsBody`, `Collisions`... much more.
* [Names-To-Faces](projects/hws_book/names-to-faces-userdefaults) - This app is the copy of [Names-To-Faces](projects/hws_book/names-to-faces), but storing data using `UserDefaults`.
* [Instafilter](projects/hws_book/instafilter) - This easy app will let you handle images and add filters on it... you'll see `CoreImage`, `CIContext`, `CIFilter`, `UIImagePickerViewController`.
* [Whacka-Penguin](projects/hws_book/whacka-penguin) - Build a game using `SKCropNode` and a sprinkling of `Grand Central Dispatch`
* [Animation](projects/hws_book/animation) - This easy app will handle `CGAffineTransform` to make some transformation used as animations.
* [Javascript-Injection](projects/hws_book/javascript-injection) - Extend Safari with a cool feature for Javascript Developers. [Swifty-Ninja](projects/hws_book/swifty-ninja) - Little clone of `Fruit Ninja` where you see about `SKShapeNode`, `AVAudioPlayer` and `UIBezierPath`.
* [Debugging](projects/hws_book/debugging) - No demo, this is just few ways to debug, using `print` and `assert`, `breakpoints` and `view hierarchy`
* [Capital-Cities](projects/hws_book/capital-cities) - This project is about maps... where you see about `MKMapView`, `MKAnnotation`, `MKPinAnnotationView`, `CLLocationCoordinate2D`.
* [Fireworks-Night](projects/hws_book/fireworks-night) - This project is a new Game... the new things here are `Motion` and the `Timer`, and what we already know about `SKSpriteNode`, `SKEmitterNode`... etc.
* [Local-Notifications](projects/hws_book/local-notifications) - A basic project where you'll see a little about local notifications... `UNUserNotificationCenter`.
* [Detect-A-Beacon](projects/hws_book/detect-a-beacon) - Learn to find and range iBeacons a distance using `CLLocationManager`, `CLBeaconRegion`, `didRangeBeacons`... etc.
* [Space-Race](projects/hws_book/space-race) - A basic collision game where you need to evade the enemies... this is a `SpriteKit` with topics that already seen on previous project on this section.
* [Swift-Extensions](projects/hws_book/swift-extensions) - Improving the built-in data types... here we'll see `extension` and `POP - Protocol-Oriented Programming` for beginners.
* [Selfie-Share](projects/hws_book/selfie-share) - Create a sharing image app that use a new topic on this list of apps `Multipeer Connectivity Framework`.
* [Marble-Maze](projects/hws_book/marble-maze) - An easy game to tilt a marble across screen using a new topic in here... the `Accelerometer` and the way to divide code for `Simulator` and `Device`
* [Core-Graphics](projects/hws_book/core-graphics) - Draw 2D shapes using `Core-Graphics`.
* [Secret-Swift](projects/hws_book/secret-swift) - Save user data using `biometrics` and `keychain`.
* [Exploding-Monkeys](projects/hws_book/exploding-monkeys) - A remake of classic Gorillas game where you'll see `destructible terrain` and `scene transitions`.
* [Multibrowser](projects/hws_book/multibrowser) - Get started with `UIStackView` and see just how easy iPad multitasking is.
* [Swift-Searcher](projects/hws_book/swift-searcher) - Add your app's content to iOS Spotlight and take advantage of Safari integration.
* [Four-In-A-Row](projects/hws_book/four-in-a-row) - Let iOS take over the AI in your games using `GameplayKit AI`.
* [Random-Numbers](projects/hws_book/random-numbers) - Let `GameplayKit` help you generate random numbers in ways you soon won't be able to live without.
* [Crashy-Plane](projects/hws_book/crashy-plane) - Create a clone of `Flappy Bird` where you see `Physics`, `Animation`, `Infinite Scrolling`, `SKAudioNode`, `GKRandomDistribution`.
* [Psychic-Tester](projects/hws_book/psychic-tester) - Create a psychic game for iOS and watchOS using `CAEmitterLayer`, `CAGradientLayer`, `IBDesignable`, `IBInspectable` and a 3D card flip effect.
* [Github-Commits](projects/hws_book/github-commits) - Get started with `Core Data` building an app to fetch and store Github commits, you'll find `NSFetchRequest`, `NSManagedObject`, `NSPredicate` and more.
* [Unit-Testing](projects/hws_book/unit-testing) - Write `Unit Tests` and `User Interface tests` using Xcode's build-in `testing framework`.

## Pro Swift
All this topics are not covering the basic parts... this is to you to see what else and more advanced topics you can do with the power of Swift.
* [Syntax](projects/pro_swift/01-syntax) - Learn the basics about `pattern matching`, `nil coalescing`, `guard`, `lazy`, `destructuring`, `labeled statements`, `nested`, `documentation markup`.
* [Types](projects/pro_swift/02-types) - Learn about `useful initializers`, `enums`, `arrays`, `dictionaries`, `sets`, `tuples and generics`.
* [References-And-Values](projects/pro_swift/03-syntax) - You will see the difference between `reference type` and `value type`, learn about `closures`, `boxing` (that combine Structs and Classes in one) and `immutability`.
* [Functions](projects/pro_swift/01-syntax) - Learn about `Variadic Functions`, `Operator Overloading`, `Closures` and `Match Operator (~=)`.
* [Errors](projects/pro_swift/01-syntax) - Learn about the `fundamentals`, the `propagation`, `throwing parameters`, `try|try?|try!` and `assertions`.
* [Functional-Programming](projects/pro_swift/01-syntax) - Take a look to the Functional Programming basics... `map`, `compact map`, `flat map`, `filter`, `reduce`, `sort`, `functional` and `lazy functions`.

## Advanced iOS [Volume 1]
You will see advanced topics that covers Spotlight, iMessage... etc.
* [Happy_Days](projects/advanced_ios_volume_one/happy_days) - On this project you will see `Spotlight`, `Recording`, `SpeechRecognition`.
* [TimeShare](projects/advanced_ios_volume_one/timeshare) - Take a look to the new `Message Framework`.
* [Choose Cruise](projects/advanced_ios_volume_one/choosecruise) - Use `SiriKit` and `Maps` for Request a Ride.

## Design Patterns
* [MVVM-Binding](Projects/mvvm-binding) - MVVM is an alternative to MVC for Apple development, this require a binding to let it be MVVM, here is an Swiftly class that make this binding works.

## Testing With Swift
