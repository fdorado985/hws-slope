#  TimeShare

It will allow one person to create a list of dates that an event could take place on, and send that list to other people in the chat.
They will be able to see how many votes have been cast for each date, then add their own votes. In a group chat, this should mean that a group can pick the best date from various options.

## Wrap Up
On this project you will see...
* Stack Views
* View controller containment
* URLQueryItem
* DateFormatter
* AttributedString
* Messages Framework

## Demo
![timeshare_demo](screenshots/timeshare_demo.gif)
