# Choose Cruise
## Description
This project – using SiriKit and Maps extensions will let you ask for a ride.

## Wrap Up
You will see...
- `SiriKit`
- `MapKit`
- `Intents`
- `Info.plist (Intents)`

## Demo
### Request Permissions
![choose_cruise_request_permissions_demo](.screenshots/choose_cruise_request_demo.gif)

### Full Ride Request
![choose_cruise_full_ride_request_demo](.screenshots/choosecruise_demo_request.gif)
