#  Happy Days

## Description

This is a cute and a little bit more complex app...
What are you going to see here... well...

* Import Photos
* Attaches Recordings
* Transcribes recordings
* Put them into Spotlight for searching

## Demo
![happy_days_demo](screenshots/happy_days_demo.gif)
