//
//  MemoryCell.swift
//  happy_days
//
//  Created by Juan Francisco Dorado Torres on 8/12/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MemoryCell: UICollectionViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgPhoto: UIImageView!
}
