//
//  FirstRunVC.swift
//  happy_days
//
//  Created by Juan Francisco Dorado Torres on 8/12/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Speech

class FirstRunVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var lblHelp: UILabel!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: IBActions
    
    @IBAction func btnRequestPermissionsTapped(_ sender: UIButton) {
        requestPhotosPermissions()
    }
    
    // MARK: Functions
    
    private func requestPhotosPermissions() {
        PHPhotoLibrary.requestAuthorization { [unowned self] (authStatus) in
            DispatchQueue.main.async {
                if authStatus == .authorized {
                    self.requestRecordPermissions()
                } else {
                    self.lblHelp.text = "Photos permission was declined; please enable it in settings then tap Continue again."
                }
            }
        }
    }
    
    private func requestRecordPermissions() {
        AVAudioSession.sharedInstance().requestRecordPermission { [unowned self] (allowed) in
            DispatchQueue.main.async {
                if allowed {
                    self.requestTranscribePermissions()
                } else {
                    self.lblHelp.text = "Recording permission was declined; please enable it in settings then tap Continue again."
                }
            }
        }
    }
    
    private func requestTranscribePermissions() {
        SFSpeechRecognizer.requestAuthorization { [unowned self] (authStatus) in
            DispatchQueue.main.async {
                if authStatus == .authorized {
                    self.authorizationComplete()
                } else {
                    self.lblHelp.text = "Transcription permission was declined; please enable it in settings then tap Continue again."
                }
            }
        }
    }
    
    private func authorizationComplete() {
        dismiss(animated: true)
    }
}

