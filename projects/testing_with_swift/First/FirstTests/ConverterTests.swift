//
//  ConverterTests.swift
//  FirstTests
//
//  Created by Juan Francisco Dorado Torres on 11/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest
@testable import First

class ConverterTests: XCTestCase {
    
    // MARK: Properties
    
    var sut: Converter!

    // MARK: Test cycle
    
    override func setUp() {
        sut = Converter()
    }

    override func tearDown() {
        sut = nil
    }

    // MARK: Tests
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func test_32Fahrenheit_IsZeroCelsius() {
        // Given
        let input = 32.0
        
        // When
        let celsius = sut.convertToCelsius(farenheit: input)
        
        // Then
        XCTAssertEqual(celsius, 0, accuracy: 0.000001)
    }
    
    func test_212Fahrenheit_Is100Celcius() {
        // Given
        let input = 212.0
        
        // When
        let celsius = sut.convertToCelsius(farenheit: input)
        
        // Then
        XCTAssertEqual(celsius, 100, accuracy: 0.000001)
    }
    
    func test_100Fahrenheit_Is37Celsius() {
        // Given
        let input = 100.0
        
        // When
        let celsius = sut.convertToCelsius(farenheit: input)
        
        // Then
        XCTAssertEqual(celsius, 37.777777, accuracy: 0.000001)
    }

}
