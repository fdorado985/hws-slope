//
//  LifecycleTests.swift
//  FirstTests
//
//  Created by Juan Francisco Dorado Torres on 2/18/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest

class LifecycleTests: XCTestCase {

    override class func setUp() {
        debugPrint("In class setUp")
    }
    
    override class func tearDown() {
        debugPrint("In class tearDown")
    }
    
    override func setUp() {
        debugPrint("In setUp")
    }

    override func tearDown() {
        debugPrint("In tearDown")
    }

    func testExample() {
        debugPrint("Starting test")
        
        addTeardownBlock {
            debugPrint("In first tearDownBlock")
        }
        
        debugPrint("In middle of test")
        
        addTeardownBlock {
            debugPrint("In second tearDownBlock")
        }
        
        debugPrint("Finishing test")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
