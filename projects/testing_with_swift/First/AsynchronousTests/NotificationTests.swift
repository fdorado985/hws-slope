//
//  NotificationTests.swift
//  AsynchronousTests
//
//  Created by Juan Francisco Dorado Torres on 2/19/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest
@testable import First

class NotificationTests: XCTestCase {

    // MARK: Test Cycle
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: Tests

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testUserUpgradedPostsNotification() {
        // given
        let user = User()
        let expectation = XCTNSNotificationExpectation(name: User.upgradedNotification)
        
        // when
        user.upgrade()
        
        // then
        wait(for: [expectation], timeout: 3)
    }
    
    func testUserUpgradedPostsNotificationWithHandler() {
        // given
        let user = User()
        let expectation = XCTNSNotificationExpectation(name: User.upgradedNotification)
        
        expectation.handler = { notification -> Bool in
            guard let level = notification.userInfo?["level"] as? String else {
                return false
            }
            
            if level == "gold" {
                return true
            } else {
                return false
            }
        }
        
        // when
        user.upgrade()
        
        // then
        wait(for: [expectation], timeout: 3)
        
    }
    
    func testUserUpgradedPostsNotificationWithDependencyInjection() {
        // given
        let center = NotificationCenter()
        let user = User()
        let expectation = XCTNSNotificationExpectation(name: User.upgradedNotification, object: nil, notificationCenter: center)
        
        expectation.handler = { notification -> Bool in
            guard let level = notification.userInfo?["level"] as? String else {
                return false
            }
            
            if level == "gold" {
                return true
            } else {
                return false
            }
        }
        
        // when
        user.upgrade(using: center)
        
        // then
        wait(for: [expectation], timeout: 3)
    }

}
