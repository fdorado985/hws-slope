//
//  AsynchronousTests.swift
//  AsynchronousTests
//
//  Created by Juan Francisco Dorado Torres on 2/19/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest
@testable import First

class AsynchronousTests: XCTestCase {

    // MARK: Test Cycle
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK: Tests
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPrimesUpTo100ShouldBe25() {
        // given
        let maximumCount = 100
        let expectation = XCTestExpectation(description: "Calculate primes up to \(maximumCount)")
        
        // when
        PrimeCalculator.calculate(upTo: maximumCount) {
            // then
            XCTAssertEqual($0.count, 25)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10)
    }
    
    func testPrimesUpTo100ShouldBe25Streaming() {
        // given
        let maximumCount = 100
        let expectation = XCTestExpectation(description: "Calculate primes up to \(maximumCount)")
        expectation.expectedFulfillmentCount = 25
        
        // when
        PrimeCalculator.calculateStreaming(upTo: maximumCount) { (number) in
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3)
    }
    
    func testPrimesUpTo100ShouldBe25StreamingWithPrimes() {
        // given
        let maximumCount = 100
        let primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
                      41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
        var primeCounter = 0
        
        let expectation = XCTestExpectation(description: "Calculate primes up to \(maximumCount)")
        expectation.expectedFulfillmentCount = 25
        
        // when
        PrimeCalculator.calculateStreaming(upTo: maximumCount) { (number) in
            XCTAssertEqual(primes[primeCounter], number)
            expectation.fulfill()
            primeCounter += 1
        }
        
        wait(for: [expectation], timeout: 3)
    }
    
    func testPrimesUpTo100ShouldBe25WithProgress() {
        // given
        let maximumCount = 100
        
        // when
        let progress = PrimeCalculator.calculateWithProgress(upTo: maximumCount) { XCTAssertEqual($0.count, 25) }
        
        // then
        let predicate = NSPredicate(format: "%@.completedUnitCount == %@", argumentArray: [progress, maximumCount])
        
        let expectation = XCTNSPredicateExpectation(predicate: predicate, object: progress)
        wait(for: [expectation], timeout: 10)
    }

}
