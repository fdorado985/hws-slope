//
//  User.swift
//  First
//
//  Created by Juan Francisco Dorado Torres on 2/19/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct User {
    
    // MARK: Properties
    
    static let upgradedNotification = Notification.Name("UserUpgraded")
    
    // MARK: Function
    
    func upgrade() {
        DispatchQueue.global().async {
            Thread.sleep(forTimeInterval: 1)
            let center = NotificationCenter.default
            center.post(name: User.upgradedNotification, object: nil, userInfo: ["level" : "gold"])
        }
    }
    
    func upgrade(using center: NotificationCenter = NotificationCenter.default) {
        DispatchQueue.global().async {
            Thread.sleep(forTimeInterval: 1)
            center.post(name: User.upgradedNotification, object: nil, userInfo: ["level": "gold"])
        }
    }
    
    
}
