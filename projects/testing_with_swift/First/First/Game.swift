//
//  Game.swift
//  First
//
//  Created by Juan Francisco Dorado Torres on 2/19/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

extension LocalizedError {
    var errorDescription: String? {
        return "\(self)"
    }
}

enum GameError: /*Error*/ LocalizedError {
    case notPurchased
    case notInstalled
    case parentalControlsDisallowed
}

struct Game {
    
    // MARK: Properties
    
    let name: String
    
    // MARK: Functions
    
    func play() throws {
        if name == "BioBlitz" {
            throw GameError.notPurchased
        } else if name == "Blastazap" {
            throw GameError.notInstalled
        } else if name == "Dead Storm Rising" {
            throw GameError.parentalControlsDisallowed
        } else {
            debugPrint("\(name) is OK to play!")
        }
    }
}
