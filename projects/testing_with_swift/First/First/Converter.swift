//
//  Converter.swift
//  First
//
//  Created by Juan Francisco Dorado Torres on 11/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Converter {
    
    func convertToCelsius(farenheit: Double) -> Double {
        let farenheit = Measurement(value: farenheit, unit: UnitTemperature.fahrenheit)
        let celsius = farenheit.converted(to: .celsius)
        return celsius.value
    }
}
