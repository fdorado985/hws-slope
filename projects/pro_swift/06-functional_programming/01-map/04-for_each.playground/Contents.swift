//: Playground - noun: a place where people can play

import UIKit

// MARK: - For Each

["Taylor", "Paul", "Adele"].forEach { print($0) }
