//: Playground - noun: a place where people can play

import UIKit

// MARK: - Map Examples

let fruits = ["Apple", "Cherry", "Orange", "Pineapple"]
let upperFruits = fruits.map { $0.uppercased() }
print(upperFruits)

let scores = [100, 80, 85]
let formatted = scores.map { "Your score was \($0)" }
print(formatted)

let scoresX = [100, 80, 85]
let passOrFail = scoresX.map { $0 > 85 ? "Pass" : "Fail" }
print(passOrFail)

let position = [50, 60, 40]
let averageResults = position.map { 45 ... 55 ~= $0 ? "Within average" : "Outside average" }
print(averageResults)

let numbers: [Double] = [4, 9, 25, 36, 49]
let result = numbers.map(sqrt)
print(result)
