//: Playground - noun: a place where people can play

import UIKit

// MARK: - Map Basics

// Example without map
func lenghtOf(strings: [String]) -> [Int] {
    var result = [Int]()
    
    for string in strings {
        result.append(string.count)
    }
    
    return result
}

func mapLenghtOf(strings: [String]) -> [Int] {
    return strings.map { $0.count }
}
