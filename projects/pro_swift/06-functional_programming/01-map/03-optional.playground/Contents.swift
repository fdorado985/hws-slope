//: Playground - noun: a place where people can play

import UIKit

// MARK: - Map Optional

enum Optional<Wrapped> {
    case none
    case some(Wrapped)
}

let i: Int? = 10
let j = i.map { $0 * 2 }
print(j)

func fetchUsername(id: Int) -> String? {
    if id == 1989 {
        return "Taylor Swift"
    } else {
        return nil
    }
}


// Example with Map

var username: String? = fetchUsername(id: 1989)
let formattedUsername = username.map { "Welcome, \($0)!" } ?? "Unknown user"
print(formattedUsername)

// Example without Map

let usernameX = fetchUsername(id: 1989)
let formattedUsernameX: String

// Long
if let username = username {
    formattedUsernameX = "Welcome, \(username)!"
} else {
    formattedUsernameX = "Unknown user"
}

// Ternary
let formattedUsernameY = username != nil ? "Welcome, \(username!)!" : "Unknown user"

print(formattedUsernameX)
print(formattedUsernameY)

