//: Playground - noun: a place where people can play

import UIKit

// MARK: - Filter

// This method loops over every item in a collection, and passes it into a function that you write.

let fibonacciNumbers = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
let evenFibonacci = fibonacciNumbers.filter { $0 % 2 == 0 }
print(evenFibonacci) // out - [2, 8, 34]

// Examples

let names = ["Michael Jackson", "Taylor Swift", "Michael Caine", "Adele Adkins", "Michael Jordan"]
let result = names.filter { $0.hasPrefix("Michael") }
print(result) // out - ["Michael Jackson", "Michael Caine", "Michael Jordan"]

let words = ["1989", "Fearless", "Red"]
let input = "My favorite album is Fearless"
let resultWords = words.filter { input.contains($0) }
print(resultWords) // out - ["Fearless"]
