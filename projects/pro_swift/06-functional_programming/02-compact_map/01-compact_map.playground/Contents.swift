//: Playground - noun: a place where people can play

import UIKit

// MARK: - Compact Map

// Compact Map is the function of map but with one more function... it removes the nils inside.

let albums: [String?] = ["Fearless", nil, "Speak Now", nil, "Red"]

// Using map
let result = albums.map { $0 }
print(result) // out - [Optional("Fearless"), nil, Optional("Speak Now"), nil, Optional("Red")]

// Using compactMap
let resultX = albums.compactMap { $0 }
print(resultX) // out - ["Fearless", "Speak Now", "Red"]

// Example 2
let scores = ["100", "90", "Fish", "85"]
let compactMapScores = scores.compactMap { Int($0) }
print(compactMapScores) // out - [100, 90, 85]

// Example 3
let files = (1...10).compactMap { try? String(contentsOfFile: "someFile-\($0).txt") }
print(files)
