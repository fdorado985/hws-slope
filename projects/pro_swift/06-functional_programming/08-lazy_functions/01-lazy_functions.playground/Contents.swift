//: Playground - noun: a place where people can play

import UIKit

// MARK: - Lazy Functions

// Lazy sequences can save you time, but you need to be careful because they can generate huge amounts of work.

let numbers = Array(1...10000)
let lazyFilter = numbers.lazy.filter { $0 % 2 == 0 }
let lazyMap = numbers.lazy.map { $0 * 2 }

print(lazyFilter.count)
print(lazyFilter.count)
print(lazyMap[5000])
print(lazyMap[5000])
