//: Playground - noun: a place where people can play

import UIKit

// MARK: - Flat Map

let numbers = [[1, 2], [3, 4], [5, 6]]
let joined = Array(numbers.joined())
print(joined) // out - [1, 2, 3, 4, 5, 6]

// Flat Map is the combination of map() and joined()

let joinedX = numbers.flatMap { $0 }
print(joinedX) // out - [1, 2, 3, 4, 5, 6]
