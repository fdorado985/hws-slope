//: Playground - noun: a place where people can play

import UIKit

// MARK: - Optional Flap Map

// Flat Map with optionals is similar to using map() with one important difference
// If your transformation closure returns an optional, flatMap() will combine that optional with the existing optional, whereas map() will keep them both.

// FlatMap will help you to avoid Optional(Optional())

let stringNumber: String? = "5"
let intNumber = stringNumber.map { Int($0) }
print(intNumber) // out - Optional(Optional(5))

let flatMapNumber = stringNumber.flatMap { Int($0) }
print(flatMapNumber)
