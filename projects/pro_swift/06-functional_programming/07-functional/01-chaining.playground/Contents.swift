//: Playground - noun: a place where people can play

import UIKit

// MARK: - Functional Chaining
// You are able to chain different functional functions to make the output be the input of the other.

let london = (name: "London", continent: "Europe", population: 8_539_000)
let paris = (name: "Paris", continent: "Europe", population: 2_244_000)
let lisbon = (name: "Lisbon", continent: "Europe", population: 530_000)
let rome = (name: "Rome", continent: "Europe", population: 2_627_000)
let tokyo = (name: "Tokyo", continent: "Asia", population: 13_350_000)

let cities = [london, paris, lisbon, rome, tokyo]

let totalPopulation = cities.reduce(0) { $0 + $1.population }
print(totalPopulation)

let europePopulation = cities
    .filter { $0.continent == "Europe" }
    .reduce(0) { $0 + $1.population }
print(europePopulation)

let biggestCities = cities
    .filter { $0.population > 2_000_000 }
    .sorted { $0.population > $1.population }
    .prefix(upTo: 3)
    .map { "\($0.name) is a big city, with a population of \($0.population)" }
    .joined(separator: "\n")
print(biggestCities)
