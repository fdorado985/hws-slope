//: Playground - noun: a place where people can play

import UIKit

// MARK: - Functional Composition

precedencegroup CompositionPrecedence {
    associativity: left
}

infix operator >>>: CompositionPrecedence

func >>><T, U, V>(lhs: @escaping (T) -> U, rhs: @escaping (U) -> V) -> (T) -> V {
    return { rhs(lhs($0)) }
}

func >>>(lhs: @escaping (Int) -> String, rhs: @escaping (String) -> [String]) -> (Int) -> [String] {
    return { rhs(lhs($0)) }
}

//func functionA(_ value: String) -> String { return String() }
//func functionB(_ value: String) -> String { return String() }
//func functionC() -> String { return String() }

//let foo = functionC(functionB(functionA()))
//let fooX = functionC >>> functionB >>> functionA
//let fooY = functionA >>> functionB >>> functionC

func generateRandomNumber(max: Int) -> Int {
    let number = Int(arc4random_uniform(UInt32(max)))
    print("Using number: \(number)")
    return number
}

func calculateFactors(number: Int) -> [Int] {
    return (1...number).filter { number % $0 == 0 }
}

func reduceToString(numbers: [Int]) -> String {
    return numbers.reduce("Factors: ") { $0 + String($1) + " " }
}

let result = reduceToString(numbers: calculateFactors(number: generateRandomNumber(max: 100)))
print(result)

let combined = generateRandomNumber >>> calculateFactors >>> reduceToString
print(combined(100))
