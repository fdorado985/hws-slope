//: Playground - noun: a place where people can play

import UIKit

// MARK: - Reduce

// This condenses an array into a single value byt applying a function to every item, each time the function is called, you get passed the previous value from your function as well as the next item in your array.

// Example 1

let scores = [100, 90, 95]
let sum = scores.reduce(0, +)
print(sum) // out - 285

// Example 2

let result = scores.reduce("") { $0 + String($1) }
print(result) // out - 1009095

// Signature
// func reduce(_ initial: T, _ combine: (T, Int) throws -> T)
// func reduce(_ initial: String, _ combine: (String, Int) throws -> String)

let names = ["Taylor", "Paul", "Adele"]
let count = names.reduce(0) { $0 + $1.count }
print(count)
