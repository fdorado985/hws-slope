//: Playground - noun: a place where people can play

import UIKit

// MARK: - Reduce Examples

// Reducing to find a value

// Get the longest name
let names = ["Taylor", "Paul", "Adele"]
let longest = names.reduce("") { $1.count > $0.count ? $1 : $0 }
print(longest)

let longest2 = names.max { $1.count > $0.count }
print(longest2) // Optional("Taylor")

// Reducing a multi-dimensional array
let numbers = [
    [1, 1, 2],
    [3, 5, 8],
    [13, 21, 34]
]

let flattened: [Int] = numbers.reduce([]) { $0 + $1 }
print(flattened) // out - [1, 1, 2, 3, 5, 8, 13, 21, 34]

let flattened2 = numbers.compactMap { $0 }
print(flattened2) // out - [[1, 1, 2], [3, 5, 8], [13, 21, 34]]

let flattened3 = Array(numbers.joined())
print(flattened3) // out - [1, 1, 2, 3, 5, 8, 13, 21, 34]
