//: Playground - noun: a place where people can play

import UIKit

// MARK: - Reducing To Boolean

// We can use reduce to check whetever all items in array evaluate to true for a given condition

let names = ["Taylor", "Paul", "Adele"]
let longEnough = names.reduce(true) { $0 && $1.count > 4 }
print(longEnough)

/*
 • false && false == false
 • true && false == false
 • false && true == false
 • true && true == true
*/
