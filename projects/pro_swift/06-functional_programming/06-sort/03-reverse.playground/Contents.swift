//: Playground - noun: a place where people can play

import UIKit

// MARK: - Reverse Sorting

let names = ["Taylor", "Paul", "Adele", "Justin"]
print(names) // out - ["Taylor", "Paul", "Adele", "Justin"]

let sorted = names.sorted().reversed()
print(sorted) // out - ReversedCollection<Array<String>>(_base: ["Adele", "Justin", "Paul", "Taylor"])

let sortedArray = Array(names.sorted().reversed())
print(sortedArray) // out - ["Taylor", "Paul", "Justin", "Adele"]
