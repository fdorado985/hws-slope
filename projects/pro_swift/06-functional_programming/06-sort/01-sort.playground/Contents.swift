//: Playground - noun: a place where people can play

import UIKit

// MARK: - Sort

// Nothing much to say... what does do sort? well... sort elements.

let scoresString = ["100", "95", "85", "90", "100"]
let sortedString1 = scoresString.sorted()
print(sortedString1) // out - ["100", "100", "85", "90", "95"]

let scoresInt = scoresString.compactMap { Int($0) }
let sortedInt = scoresInt.sorted()
print(sortedInt) // out - [85, 90, 95, 100, 100]
