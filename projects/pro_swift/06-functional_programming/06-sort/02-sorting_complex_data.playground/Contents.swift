//: Playground - noun: a place where people can play

import UIKit

// MARK: - Sorting Complex Data

struct Person {
    var name: String
    var age: Int
}

let taylor = Person(name: "Taylor", age: 26)
let paul = Person(name: "Paul", age: 36)
let justin = Person(name: "Justin", age: 22)
let adele = Person(name: "Adele", age: 27)

let people = [taylor, paul, justin, adele]

let sortedPeople = people.sorted { $0.name < $1.name }
print(sortedPeople)

// Using Comparable ===========

struct PersonX: Comparable {
    var name: String
    var age: Int
}

func <(lhs: PersonX, rhs: PersonX) -> Bool {
    return lhs.name < rhs.name
}

func ==(lhs: PersonX, rhs: PersonX) -> Bool {
    return lhs.name == rhs.name && lhs.age == rhs.age
}

let taylorX = PersonX(name: "Taylor", age: 26)
let paulX = PersonX(name: "Paul", age: 36)
let justinX = PersonX(name: "Justin", age: 22)
let adeleX = PersonX(name: "Adele", age: 27)
let peopleX = [taylorX, paulX, justinX, adeleX]

let sortedPeopleX = peopleX.sorted()
print(sortedPeopleX)
