//: Playground - noun: a place where people can play

import UIKit

// MARK: - Immutability

struct PersonStruct {
    var name: String
    var age: Int
}

final class PersonClass {
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}



var taylor1 = PersonStruct(name: "Taylor Swift", age: 26)
taylor1.name = "Justin Bieber"
taylor1 = PersonStruct(name: "Justin Bieber", age: 22)

let taylor2 = PersonStruct(name: "Taylor Swift", age: 26)
// taylor2.name = "Justin Bieber" // Cannot assign to property: 'taylorStruct' is a 'let' constant
//taylor2 = PersonStruct(name: "Justin Bieber", age: 22) // note: change 'let' to 'var' to make it mutable

var taylor3 = PersonClass(name: "Taylor Swift", age: 26)
taylor3.name = "Justin Bieber"
taylor3 = PersonClass(name: "Justin Bieber", age: 22)

let taylor4 = PersonClass(name: "Taylor Swift", age: 26)
taylor4.name = "Justin Bieber"
//taylor4 = PersonClass(name: "Justin Bieber", age: 22) // note: change 'let' to 'var' to make it mutable
