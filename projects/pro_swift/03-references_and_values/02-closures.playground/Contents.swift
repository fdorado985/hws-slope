//: Playground - noun: a place where people can play

import UIKit

// MARK: - Closures

// Clases and Closures are reference type

let printGreeting = { print("Hello!") }
printGreeting()

let copyGreeting = printGreeting
copyGreeting()

// The value is shared between them...

func createIncrementer() -> () -> Void {
    var counter = 0
    return {
        counter += 1
        print(counter)
    }
}

let incrementer = createIncrementer()
incrementer()
incrementer()

let incrementerCopy = incrementer
incrementerCopy()

incrementer()
