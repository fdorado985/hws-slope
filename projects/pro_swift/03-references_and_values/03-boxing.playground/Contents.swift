//: Playground - noun: a place where people can play

import UIKit

// MARK: - Boxing

/*
struct Person {
    var name: String
    var age: Int
    var favoriteIceCream: String
}

let taylor = Person(name: "Taylor Swift", age: 26, favoriteIceCream: "Chocolate")

final class PersonBox {
    var person: Person
    
    init(person: Person) {
        self.person = person
    }
}

let box = PersonBox(person: taylor)

final class TestContainer {
    var box: PersonBox!
}

let container1 = TestContainer()
let container2 = TestContainer()

container1.box = box
container2.box = box

print(container1.box.person.name)
print(container2.box.person.name)

box.person.name = "Not Taylor"

print(container1.box.person.name)
print(container2.box.person.name)
*/

struct Person {
    var name: String
    var age: Int
    var favoriteIceCream: String
}

final class Box<T> {
    var value: T
    
    init(value: T) {
        self.value = value
    }
}

final class TestContainer {
    var box: Box<Person>!
}



let taylor = Person(name: "Taylor Swift", age: 26, favoriteIceCream: "Chocolate")
let box = Box<Person>(value: taylor)

let container1 = TestContainer()
let container2 = TestContainer()

container1.box = box
container2.box = box

print(container1.box.value.name)
print(container2.box.value.name)

box.value.name = "Not Taylor"

print(container1.box.value.name)
print(container2.box.value.name)
