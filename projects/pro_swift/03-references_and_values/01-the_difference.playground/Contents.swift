//: Playground - noun: a place where people can play

import UIKit

// MARK: - What's the Difference Between Reference Values and Type Values

// Change struct with class to see the print
struct Target {
    var location: String = ""
}

class Pilot {
    var target: Target
    
    init() {
        target = Target()
    }
}

var luke = Pilot()
var wedge = Pilot()


var target = Target()
target.location = "Death Star"
luke.target = target

target.location = "Rebel Base"
wedge.target = target

print(wedge.target.location)
print(luke.target.location)

// If it is a class - It is a reference type - So everybody are gonna point to the same.
// If it is a struct - It is a value type - It just have just one owner


var a = [1, 2, 3]
var b = a
a.append(4)
print(b.count)

let ax = [1, 2, 3]
let bx = Array(1 ... 3)
if ax == bx { print("Equal") }
