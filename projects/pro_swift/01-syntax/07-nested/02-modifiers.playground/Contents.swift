//: Playground - noun: a place where people can play

import UIKit

// MARK: - Nested Modifiers

// Enum London example
enum London {
    static let coordinates = (lat: 51.407222, long: -0.1275)
    
    enum SubwayLines {
        case bakerloo, centra, circle, disctrict, elizabeth, hammersmithCity, jubilee, metropolitan, northern, piccadilly, victoria, waterlooCity
    }
    
    enum Places {
        case buckinghamPalace, cityHall, oldBailey, piccadilly, stPaulsCathedral
    }
}

// Enum Resources example
enum R {
    enum Storyboards: String {
        case main, detail, upgrade, share, help
    }
    
    enum Images: String {
        case welcome, home, about, button
    }
}


// Struct Cat example
struct Cat {
    enum Breed {
        case britishShortHair, burmese, persian, ragdoll, russianBlue, scottishFold, siamese
    }
    
    var name: String
    var breed: Breed
}

// Struct Deck example
struct Deck {
    struct Card {
        private enum Suit {
            case hearts, diamons, clubs, spades
        }
        
        var rank: Int
        private var suit: Suit
    }
    
    var cards = [Card]()
}

// Properties example w/ enums|structs before

let home = R.Images.home
let burmese = Cat.Breed.burmese
// let hearts = Deck.Card.Suit.hearts // <== This doesn't work cause is not longer reachable
