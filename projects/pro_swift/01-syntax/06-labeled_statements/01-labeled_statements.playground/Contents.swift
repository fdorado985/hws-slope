//: Playground - noun: a place where people can play

import UIKit

// MARK: - Labeled Statements
// You can give a name to an specific statement...
// Here is an example for loops (You can use it even in an 'if' for example)

var board = [[String]](repeating: [String](repeating: "", count: 10), count: 5)
board[3][5] = "x"

rowLoop: for (rowIndex, cols) in board.enumerated() {
    for (colIndex, col) in cols.enumerated() {
        if col == "x" {
            print("Found the treasure at row \(rowIndex) col \(colIndex)")
            break rowLoop
        }
    }
}
