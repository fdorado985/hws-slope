//: Playground - noun: a place where people can play

import UIKit

// MARK: - Guard ===============
// Guard is used for inmediately return
// Using a guard, Xcode will remember you to use return on the else

func giveAward(to name: String) {
    guard name == "Taylor Swift" else {
        print("No way!")
        return
    }
    
    print("Congratulations, \(name)!")
}

giveAward(to: "Taylor Swift")


func giveAwardWithIf(to name: String) -> String {
    let message: String
    
    if name == "Taylor Swift" {
        message = "Congratulations \(name)!"
    } else {
        message = "No way!"
    }
    
    return message
}

// Take a look using guard the variable stays un scope...
func giveAwardInScope(to name: String?) {
    guard let winner = name else {
        print("No one won the award")
        return
    }
    
    print("Congratulations, \(winner)!")
}
