//: Playground - noun: a place where people can play

import UIKit

// MARK: - Guard w/ Loops

for i in 1 ... 100 {
    guard i % 8 == 0 else { continue }
    print(i)
}
