//: Playground - noun: a place where people can play

import UIKit

// MARK: - Documentation MarkUp

/**
 Call this function to grok some globs.
 Place text in `backticks` to mark code; on your keyboard there usually share a key with tilde, ~.
 * You can write bullets by starting with an asterisk then a space.
    * Indent your asterisk to create sublists
 1. You can write numbered listed by starting with 1.
 1. Subsequent items can also be numbered 1. and Xcode will renumber them automatically
 If you want to write a link, [place your text in brackets](and your link in parentheses)
 # Headings start with a # symbol
 ## Subheadings start with ##
  Sub-subheadings start with   and are the most common heading style you'll come across
 Write a *single asterisk* around words to make them italic
 Write **two asterisks** around words to make them bold
 */
func myGreatFunction() {
    // do stuff
}

/**
 Call this function to grok some globs.
 - Returns: A string containing a date formatted as RFC-822
 - Parameter album: The name of a Taylor Swift album
 - Parameter track: The track number to load
 - Throws: LoadError.networkFailed, LoadError.writeFailed
 - Precondition: inputArray.count > 0
 - Complexity: 0(1)
 - Authors: Juan Dorado
 */
func anotherGreatFunction() {
    // do stuff
}
