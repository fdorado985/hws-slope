//: Playground - noun: a place where people can play

import UIKit

// MARK: - Simple Switch

let name = "fdorado985"

switch name {
case "bilbo":
    print("Hello, Bilbo Baggins!")
case "fdorado985":
    print("Hello, Juan Dorado!")
default:
    print("Authentication failed")
}

// MARK: - Switch w/Tuple (Separated Variables)

let user = "fdorado985"
let password = "12345"

switch (name, password) {
case ("bilbo", "bagg1n5"):
    print("Hello, Bibo Baggins!")
case ("fdorado985", "12345"):
    print("Hello, Juan Dorado!")
default:
    print("Who are you?")
}

// MARK: - Switch w/Tuple (Using Tuple as Variable)

let authentication = (name: "fdorado985", password: "12345")

switch (name, password) {
case ("bilbo", "bagg1n5"):
    print("Hello, Bibo Baggins!")
case ("fdorado985", "12345"):
    print("Hello, Juan Dorado!")
default:
    print("Who are you?")
}
