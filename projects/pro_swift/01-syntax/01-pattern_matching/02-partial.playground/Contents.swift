//: Playground - noun: a place where people can play

import UIKit

// MARK: - Partial Match (You care some values, but not others)

let authentication = (name: "fdorado985", password: "fd0r4d0", ipAddress: "127.0.0.1")

switch authentication {
case ("Bilbo", "bagg1n5", _):
    print("Hello, Bilbo Baggins!")
case ("fdorado985", "fd0r4d0", _):
    print("Hello, Juan Dorado!")
default:
    print("Who are you?")
}

// NOTE: Swift will take the first match...
// The next example will let you see how...

switch authentication {
case (_, _, _):
    print("This will always happen...")
case ("Bilbo", "bagg1n5", _):
    print("Hello, Bilbo Baggins!")
case ("fdorado985", "fd0r4d0", _):
    print("Hello, Juan Dorado!")
default:
    print("Who are you?")
}

// If you want to match a part... but even you want to know the other part...
// Take a look to this example...

switch authentication {
case ("Bilbo", "bagg1n5", _):
    print("Hello, Bilbo Baggins!")
case ("fdorado985", let password, _):
    print("Hello, Juan Dorado: your password was \(password)!")
default:
    print("Who are you?")
}
