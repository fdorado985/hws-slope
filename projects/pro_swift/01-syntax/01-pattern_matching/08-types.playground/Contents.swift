//: Playground - noun: a place where people can play

import UIKit

// Pattern Matching w/ Types

let view: AnyObject = UIButton()

switch view {
case is UIButton:
    print("Found a button")
case is UILabel:
    print("Found a label")
case is UISwitch:
    print("Found a view")
default:
    print("Found something else")
}

// Loop with types

for label in view.subviews where label is UILabel {
    print("Found a label with frame \(label.frame)")
}

for case let label as UILabel in view.subviews {
    print("Found a label with text \(label.text)")
}
