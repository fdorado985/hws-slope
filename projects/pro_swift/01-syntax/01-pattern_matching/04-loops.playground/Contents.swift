//: Playground - noun: a place where people can play

import UIKit

// Let you loop with a criteria we specified

let fdorado = (name: "fdorado985", password: "fd0r4d0")
let bilbo = (name: "bilbo", password: "bagg1n5")
let taylor = (name: "taylor", password: "fr0st1es")

let users = [fdorado, bilbo, taylor]

for user in users {
    print(user.name)
}

for case ("fdorado985", "fd0r4d0") in users {
    print("User fdorado has the password fd0r4d0")
}

for case (let name, let password) in users {
    print("User \(name) has the password \(password)")
}

for case let (name, password) in users {
    print("User \(name) has the password \(password)")
}

for case let (name, "fr0st1es") in users {
    print("User \(name) has the password \"fr0st1es\"")
}
