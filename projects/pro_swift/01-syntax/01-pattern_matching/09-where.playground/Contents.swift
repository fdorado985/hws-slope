//: Playground - noun: a place where people can play

import UIKit

// Matching Patterns w/ where clousures

// Simple loop using 'where'
let celebrities = ["Michael Jackson", "Taylor Swift", "Michael Caine", "Adele Adkins", "Michael Jordan"]

for name in celebrities where !name.hasPrefix("Michael") {
    print(name)
}

for name in celebrities where name.hasPrefix("Michael") && name.count == 13 {
    print(name)
}

// Loop with nil values

let nilCelebrities: [String?] = ["Michael Jackson", nil, "Michael Caine", nil, "Michael Jordan"]

// Without handle the optionality
for name in nilCelebrities where name != nil {
    print(name)
}

// Loop handle the optionality
for case let name? in nilCelebrities {
    print(name)
}
