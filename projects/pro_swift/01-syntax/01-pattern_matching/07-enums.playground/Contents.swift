//: Playground - noun: a place where people can play

import UIKit

// Pattern Matching w/ Enums

enum WeatherType {
    case cloudy(coverage: Int)
    case sunny
    case windy
}

let today = WeatherType.cloudy(coverage: 100)

switch today {
case .cloudy(let coverage) where coverage < 100:
    print(print("It's cloudy with \(coverage)% coverage"))
case .cloudy(let coverage) where coverage == 100:
    print("You must live in UK")
case .windy:
    print("It's windy")
default:
    print("It's sunny")
}

// Enums w/ Ranges
switch today {
case .cloudy(let coverage) where coverage == 0:
    print("You must live in Death Valley")
case .cloudy(let coverage) where (1 ... 50).contains(coverage):
    print("It's a bit cloudy with \(coverage)% coverage")
case .cloudy(let coverage) where (51 ... 99).contains(coverage):
    print("It's very cloudy with \(coverage)% coverage")
case .cloudy(let coverage) where coverage == 100:
    print("You must live in UK")
case .windy:
    print("It's windy")
default:
    print("It's sunny")
}

// Enums with values in a loop
let forecast: [WeatherType] = [.cloudy(coverage: 40), .sunny, .windy, .cloudy(coverage: 100), .sunny]

for case .cloudy(40) in forecast {
    print("It's cloudy with 40% coverage")
}
