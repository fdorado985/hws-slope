//: Playground - noun: a place where people can play

import UIKit

let name: String? = "fdorado985"
//let password: String? = "fd0r4d0"
let password: String? = nil

switch (name, password) {
case let (.some(name), .some(password)):
    print("Hello \(name)")
case let (.some(name), .none):
    print("Please enter a password")
default:
    print("Who are you?")
}

switch (name, password) {
case let (.some(matchedName), .some(matchedPassword)):
    print("Hello, \(matchedName)")
case let (.some(matchedName), .none):
    print("Please enter a password")
default:
    print("Who are you?")
}

// As you can see here the name doesn't matter 'cause it is just local variable

switch (name, password) {
case let (name?, password?):
    print("Hello, \(name)")
case let (username?, nil):
    print("Please enter a password")
default:
    print("Who are you?")
}


// ================ Loops using optionals


let data: [Any?] = ["Bill", nil, 69, "Ted"]

for case let .some(datum) in data {
    print(datum)
}

for case let datum? in data {
    print(datum)
}
