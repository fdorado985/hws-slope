//: Playground - noun: a place where people can play

import UIKit

let age = 36

// Using ranges inside a switch
switch age {
case 0 ..< 18:
    print("You have the energy and time, but not the money")
case 18 ..< 70:
    print("You have the energy and money, but not the time")
default:
    print("You have the time and money, but not the energy")
}

// Using ranges with if case (not readable way)
if case 0 ..< 18 = age {
    print("You have the energy and time, but not the money")
} else if case 18 ..< 70 = age {
    print("You have the energy and money, but not the time")
} else {
    print("You have the time and money, but not the energy")
}

// Using ranges with if case (best readeable way)
if 0 ..< 18 ~= age {
    print("You have the energy and time, but not the money")
} else if 18 ..< 70 ~= age {
    print("You have the energy and money, but not the time")
} else {
    print("You have the time and money, but not the energy")
}

// Using ranges with if case (best readeable way 2)
if (0 ..< 18).contains(age) {
    print("You have the energy and time, but not the money")
} else if (18 ..< 70).contains(age) {
    print("You have the energy and money, but not the time")
} else {
    print("You have the time and money, but not the energy")
}


// Combining the past pattern matching with ranges
let user = (name: "fdorado985", password: "fd0r4d0", age: 36)

switch user {
case let (name, _, 0 ..< 18):
    print("You have the energy and time, but not the money")
case let (name, _, 18 ..< 70):
    print("You have the energy and money, but not the time")
default:
    print("You have the time and money, but not the energy")
}
