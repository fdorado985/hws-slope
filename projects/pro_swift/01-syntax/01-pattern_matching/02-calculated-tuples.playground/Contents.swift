//: Playground - noun: a place where people can play

import UIKit

let name = ("Juan", "Dorado")

func fizzbuzz(number: Int) -> String {
    switch (number % 3 == 0, number % 5 == 0) {
    case (true, false):
        return "Fizz"
    case (false, true):
        return "Buzz"
    case (true, true):
        return "FizzBuzz"
    case (false, false):
        return String(number)
    }
}

print(fizzbuzz(number: 15))
print(fizzbuzz(number: 14))
print(fizzbuzz(number: 12))
print(fizzbuzz(number: 10))
