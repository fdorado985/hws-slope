//: Playground - noun: a place where people can play

import UIKit

// MARK: - Destructuring

// Smart way to transfer data out from tuples
// It has three uses:
// 1. Pulling a tuple apart into multiple values
// 2. Assign multiple things simultanetly
// 3. Switching values

// Consider this...

let data = ("one", "two", "three")

// We get their values

let one = data.0
let two = data.1
let three = data.2

// That is the same if we use...

let (oneValue, twoValue, threeValue) = data


// Using methods

func getPerson() -> (String, Int) {
    return ("Taylor Swift", 26)
}

let (name, age) = getPerson()
print("\(name) is \(age) years old")


// What about pattern matching

let (_, ageValue) = getPerson()
print("That person is \(age) years old")

// Another approach
// This is really useful

// Given 2 integeres how do you swap it without a third variable

var a = 10
var b = 20

a = a + b
b = a - b
a = a - b

print(a)
print(b)

// Let replace them with destructuring

var ax = 10
var bx = 20

(ax, bx) = (ax, bx)

print(ax)
print(bx)
