//: Playground - noun: a place where people can play

import UIKit

// MARK: - Lazy - Properties

class Singer {
    
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    lazy var reversedName: String = {
        return "\(self.name.uppercased()) backwards is \(String(self.name.uppercased().reversed()))"
    }()
}

let taylor = Singer(name: "Taylor Swift")
print(taylor.reversedName)
