//: Playground - noun: a place where people can play

import UIKit

// MARK: - Lazy - Methods

class Singer {
    
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    lazy var reverserName: String = self.getReversedName()
    
    private func getReversedName() -> String {
        return "\(name.uppercased()) backwards is \(String(name.uppercased().reversed()))"
    }
}
