//: Playground - noun: a place where people can play

import UIKit

// MARK: - Nil Coalescing '??'

// This value gives a default value
// Example

let name: String? = "Taylor"
let unwrappedName = name ?? "Anonymous"

print(name) // This prints 'Optional("Taylor")'
print(unwrappedName) // This looks first for a value otherwise use the default value

// Using with a function ============

func returnsOptionalName() -> String? {
    return nil
}

let returnedName = returnsOptionalName() ?? "Anonymous"
print(returnedName)


// Using try ============

let savedText: String

do {
    savedText = try String(contentsOfFile: "saved.txt")
    print(savedText)
} catch {
    print("Failed to load saved text")
    savedText = "Hello World"
}

print(savedText)

// Refactor the try use ============

let newSavedText = (try? String(contentsOfFile: "saved.txt")) ?? "Hello, world!"
print(savedText)
