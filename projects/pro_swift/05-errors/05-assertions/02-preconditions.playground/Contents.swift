//: Playground - noun: a place where people can play

import UIKit

// MARK: - Preconditions

func *(lhs: [Int], rhs: [Int]) -> [Int] {
    precondition(lhs.count == rhs.count, "Arrays were not the same size") // Instead of line 9
    //guard lhs.count == rhs.count else { return lhs }
    
    var result = [Int]()
    for (index, int) in lhs.enumerated() {
        result.append(int * rhs[index])
    }
    
    return result
}
