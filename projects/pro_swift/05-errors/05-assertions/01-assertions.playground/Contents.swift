//: Playground - noun: a place where people can play

import UIKit

// MARK: - Assertions ==============

// assert(condition: Bool)
// assert(condition: Bool, message: String)

assert(1 == 1)
assert(1 == 1, "Danger, Will Robinson: mathematics failure!")

func runImportantOperation() -> Bool { return false }
let success = runImportantOperation()
//assert(success == true, "Important operation failed!") // Assertion failed: Important operation failed!: file 01-assertions.playground, line 16

// Assert Swift Function Signature ==============

//public func assert(_ condition: @autoclosure () -> Bool, _ message: @autoclosure () -> String = String(), file: StaticString = #file, line: UInt = #line)

// Assert Swift Function (Source code) ==============

//if _isDebugAssertConfiguration() {
//    if !_branchHint(condition(), expected: true) {
//        _assertionFailed("assertion failed", message(), file, line, flags: _fatalErrorFlags())
//    }
//}
