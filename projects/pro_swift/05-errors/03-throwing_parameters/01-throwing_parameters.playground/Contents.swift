//: Playground - noun: a place where people can play

import UIKit

// MARK: - Throwing Parameters

enum Failure: Error {
    case badNetwork(message: String)
    case broken
}

func fetchRemote() throws -> String {
    // complicated, failable work here
    throw Failure.badNetwork(message: "Firewall blocked port")
}

func fetchLocal() -> String {
    // This won't trow
    return "Taylor"
}

func fetchUserData(using closure: () throws -> String) {
    do {
        let userData = try closure()
        print("User data received: \(userData)")
    } catch Failure.badNetwork(let message) {
        print(message)
    } catch {
        print("Fetch error")
    }
}

fetchUserData(using: fetchLocal)
fetchUserData(using: fetchRemote)

// Use Error Propagation with a throwing closure parameter

func fetchUserDataX(using closure: () throws -> String) throws {
    let userData = try closure()
    print("User data received: \(userData)")
}

do {
    try fetchUserDataX(using: fetchLocal)
} catch Failure.badNetwork(let message) {
    print(message)
} catch {
    print("Fetch error")
}

// Smarter Solution

func fetchUserDataY(using closure: () throws -> String) rethrows {
    let userData = try closure()
    print("User data received: \(userData)")
}

do {
    try fetchUserDataY(using: fetchLocal)
} catch Failure.badNetwork(let message) {
    print(message)
} catch {
    print("Fetch Error")
}
