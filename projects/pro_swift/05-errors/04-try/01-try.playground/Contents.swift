//: Playground - noun: a place where people can play

import UIKit

// MARK: - try vs try? vs try!

// try - You must have a catch block to handle any errors that occur.
// try? - The function will automatically return nil if any errors are thrown... you don't need to catch them, but you need to be aware that your return value becomes optional.
// try! - The function will crash your application if any errors are thrown.


// try?

if let savedText = try? String(contentsOfFile: "saved.txt") {
    //loadText(savedText)
} else {
    //showFirstRunScreen()
}

let savedText = (try? String(contentsOfFile: "saved.txt")) ?? "Hello, world!"
