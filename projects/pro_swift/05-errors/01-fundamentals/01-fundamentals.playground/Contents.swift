//: Playground - noun: a place where people can play

import UIKit

// MARK: - Errors - Fundamentals

enum PasswordError: Error {
    case empty
    case short(minChars: Int)
    case obvious(message: String)
}

func encrypt(_ str: String, with password: String) throws -> String {
    // complicated encryption goes here
    if password == "12345" {
        throw PasswordError.obvious(message: "I have the same number on my luggage")
    }
    
    let encrypted = password + str + password
    return String(encrypted.reversed())
}

func testCatch() {
    do {
        let encrypted = try encrypt("Secret!", with: "T4yl0r")
        print(encrypted)
    } catch PasswordError.empty {
        print("You must provide a password")
    } catch PasswordError.short(let minChars) where minChars < 5 {
        print("We have a lax security policy: password must be at least \(minChars)")
    } catch PasswordError.short(let minChars) where minChars < 8 {
        print("We have a moderate security policy: passwords must be at least \(minChars)")
    } catch PasswordError.short(let minChars) {
        print("We have a serious security policy: password must be at least \(minChars)")
    } catch PasswordError.obvious(let message) {
        print("Your password is obvious: \(message)")
    } catch {
        print("Encryption failed")
    }
}

testCatch()
