//: Playground - noun: a place where people can play

import UIKit

// MARK: - Autoclosure

func printTest(_ result: @autoclosure () -> Void) {
    print("Before")
    result()
    print("After")
}

printTest(print("Hello"))

assert(1 == 1, "Maths failure!")
//assert(1 == 2, "Maths failure!")

func myReallySlowMethod() -> Bool { return false }
assert(myReallySlowMethod() == false, "The slow method returned false!")

//public static func &&(lhs: Bool, rhs: @autoclosure () throws -> Bool) rethrows -> Bool {
//    return lhs ? try rhs() : false
//}

// Combining @autoclosure and @escaping
var queuedClosures = [() -> Void]()
func queueClosure(_ closure: @autoclosure @escaping () -> Void) {
    queuedClosures.append(closure)
}

queueClosure(print("Running closure 1"))
