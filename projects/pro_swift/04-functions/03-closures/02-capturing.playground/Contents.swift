//: Playground - noun: a place where people can play

import UIKit

// MARK: - Closures Capturing (Closure - REFERENCE VALUE)

func testCapture() -> () -> Void {
    var counter = 0
    return {
        counter += 1
        print("Counter is now \(counter)")
    }
}

let greetPerson = testCapture()
greetPerson()
greetPerson()
greetPerson()

let greetCopy = greetPerson
greetCopy()
greetPerson()
greetCopy()
