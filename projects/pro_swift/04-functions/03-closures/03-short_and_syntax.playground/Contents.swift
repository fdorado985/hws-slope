//: Playground - noun: a place where people can play

import UIKit

// MARK: - Closure Short And Syntax

let names = ["Michael Jackson", "Taylor Swift", "Michael Caine", "Adele Adkins", "Michael Jordan"]

// Basic - common way
let result1 = names.filter({ (name) -> Bool in
    if name.hasPrefix("Michael") {
        return true
    } else {
        return false
    }
})

// No Types
let result2 = names.filter({ name in
    if name.hasPrefix("Michael") {
        return true
    } else {
        return false
    }
})

// Return condition
let result3 = names.filter({ name in
    return name.hasPrefix("Michael")
})

// No parentheses
let result4 = names.filter { name in
    return name.hasPrefix("Michael")
}

// Without return word (Filter already knows that need to return a boolean)
let result5 = names.filter { name in
    name.hasPrefix("Michael")
}

// Without parameter name (Parameters are numerated and with prefix $)
let result6 = names.filter { $0.hasPrefix("Michael") }
