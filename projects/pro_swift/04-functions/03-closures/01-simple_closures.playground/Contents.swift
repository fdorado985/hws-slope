//: Playground - noun: a place where people can play

import UIKit

// MARK: - Simple Closures

// Block of code that can be passed around and be stored as a variable (Reference Types)

let greetPerson = {
    print("Hello there!")
}

greetPerson()

// Using them as parameters =================

let greetCopy = greetPerson
greetCopy()

func runSomeClosure(_ closure: () -> Void) {
    closure()
}

runSomeClosure(greetPerson)

// Let closures accept parameters =================

let greetPersonWithParameter = { (name: String) in
    print("Hello \(name)!")
}
greetPersonWithParameter("Taylor")

func runSomeClosureWithParameter(_ closure: (String) -> Void) {
    closure("Taylor")
}

runSomeClosureWithParameter(greetPersonWithParameter)
