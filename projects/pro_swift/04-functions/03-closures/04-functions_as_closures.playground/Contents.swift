//: Playground - noun: a place where people can play

import UIKit

// MARK: - Functions As Closures

// Example 1
let input = "My favorite album is Fearless"
input.contains("album")
input.contains { (str) -> Bool in
    return true
}

// Example 2
let words = ["1989", "Fearless", "Red"]
let inputX = "My favorite album is Fearless"
words.contains(where: inputX.contains)

// Example 3
let numbers = [1, 3, 5, 7, 9]
numbers.reduce(0) { (int1, int2) -> Int in
    return int1 + int2
}

// Example 4
numbers.reduce(0, +)
