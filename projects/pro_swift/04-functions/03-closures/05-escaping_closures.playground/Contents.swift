//: Playground - noun: a place where people can play

import UIKit

// MARK: - Escaping Closures

var queuedClosures: [() -> Void] = []

func queuedClosure(_ closure: @escaping () -> Void) {
    queuedClosures.append(closure)
}

queuedClosure { print("Running closure 1") }
queuedClosure { print("Running closure 2") }
queuedClosure { print("Running closure 3") }

func executeQueuedClosures() {
    for closure in queuedClosures {
        closure()
    }
}

executeQueuedClosures()
