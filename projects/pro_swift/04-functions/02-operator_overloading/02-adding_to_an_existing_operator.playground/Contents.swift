//: Playground - noun: a place where people can play

import UIKit

// MARK: - Adding To An Existing Operator

func *(lhs: [Int], rhs: [Int]) -> [Int] {
    guard lhs.count == rhs.count else { return lhs }
    
    var result = [Int]()
    for (index, int) in lhs.enumerated() {
        result.append(int * rhs[index])
    }
    
    return result
}

let result = [1, 2, 3] * [1, 2, 3]
