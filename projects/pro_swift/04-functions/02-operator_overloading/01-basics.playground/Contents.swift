//: Playground - noun: a place where people can play

import UIKit

// MARK: - Operator Overloading - Basics

struct MyTypeA {}
struct MyTypeB {}

func ==(lhs: MyTypeA, rhs: MyTypeB) -> Bool {
    return true
}

if MyTypeA() == MyTypeB() {
    print("Match!")
} else {
    print("No match!")
}

// ==============

// Default Precedence
precedencegroup AdditionPrecedence {
    associativity: left
    higherThan: RangeFormationPrecedence
}

precedencegroup MultiplicationPrecedence {
    associativity: left
    higherThan: AdditionPrecedence
}

infix operator * : RangeFormationPrecedence //MultiplicationPrecedence
infix operator + : AdditionPrecedence
infix operator - : AdditionPrecedence

let i = 5 * 10 + 1
let ix = 10 - 5 - 1
