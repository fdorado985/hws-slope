//: Playground - noun: a place where people can play

import UIKit

// MARK: - Computed Properties

enum Color {
    case unknown, blue, green, pink, purple, red
    
    var description: String {
        switch self {
        case .unknown:
            return "The color of magic"
        case .blue:
            return "The color of the sky"
        case .green:
            return "The color of the grass"
        case .pink:
            return "The color of the carnations"
        case .purple:
            return "The color of the rain"
        case .red:
            return "The color of the desire"
        }
    }
    
    func forBoys() -> Bool {
        return true
    }
    
    func forGirls() -> Bool {
        return true
    }
}

struct Toy {
    let name: String
    let color: Color
}


let barbie = Toy(name: "Barbie", color: .pink)
print("This \(barbie.name) toy is \(barbie.color.description)")
