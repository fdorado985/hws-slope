//: Playground - noun: a place where people can play

import UIKit

// MARK: - Enums

enum Color {
    case unknown
    case blue
    case green
    case pink
    case purple
    case red
    
    // case unknown, blue, green, pink, purple, red // This is other way for this enum
}

struct Toy {
    
    let name: String
    let color: Color
}

let barbie = Toy(name: "Barbie", color: .pink)
let raceCar = Toy(name: "Lightning MacQueen", color: .red)

// If you want a raw value you can set it with : after enum name and the type of your value

enum Planet: Int {
    case mercury = 1
    case venus
    case earth
    case mars
    case unknown
}

let marsNumber = Planet.mars.rawValue
let mars = Planet(rawValue: 4) ?? Planet.unknown

enum ColorNames: String {
    case unknown
    case blue
    case green
    case pink
    case purple
    case red
    
    // case unknown, blue, green, pink, purple, red // This is other way for this enum
}

let pink = ColorNames.pink.rawValue
print(pink)
