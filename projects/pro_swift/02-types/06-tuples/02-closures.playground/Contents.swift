//: Playground - noun: a place where people can play

import UIKit

// MARK: - Tuples Closures

var singer = (first: "Taylor", last: "Swift", sing: { (lyrics: String) in
    print("La la la \(lyrics)")
    //print("My name is \(first): \(lyrics)") // error: use of unresolved identifier 'first'
})

singer.sing("Haters gonna hate")
