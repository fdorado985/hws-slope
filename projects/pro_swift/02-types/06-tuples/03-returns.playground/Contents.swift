//: Playground - noun: a place where people can play

import UIKit

// MARK: - Tuples Returns

/*
func fetchWeather() -> (type: String, cloudCover: Int, high: Int, low: Int) {
    return ("Sunny", 50, 32, 26)
}

let weather = fetchWeather()
print(weather.type)

let (type, cloud, high, low) = fetchWeather()
*/


/*
func fetchWeather() -> [Any] {
    return ["Sunny", 50, 32, 26]
}

let weather = fetchWeather()
let weatherType = weather[0] as! String
let weatherCloud = weather[1] as! Int
let weatherHigh = weather[2] as! Int
let weatherLow = weather[3] as! Int
*/

func fetchWeather(type: inout String, cloudCover: inout Int, high: inout Int, low: inout Int) {
    type = "Sunny"
    cloudCover = 50
    high = 32
    low = 26
}

var weatherType = ""
var weatherCloud = 0
var weatherHigh = 0
var weatherLow = 0

fetchWeather(type: &weatherType, cloudCover: &weatherCloud, high: &weatherHigh, low: &weatherLow)
weatherType
