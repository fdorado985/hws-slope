//: Playground - noun: a place where people can play

import UIKit

// MARK: - Tuples Typealias

/*
let father = (first: "Scott", last: "Swift")
let mother = (first: "Andrea", last: "Finlay")

func marryTaylorsParents(man: (first: String, last: String), woman: (first: String, last: String)) -> (husband: (first: String, last: String), wife: (first: String, last: String)) {
    return (man, (woman.first, man.last))
}
*/

typealias Name = (first: String, last: String)

func marryTaylorsParents(man: Name, woman: Name) -> (husband: Name, wife: Name) {
    return (man, (woman.first, man.last))
}
