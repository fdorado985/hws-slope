//: Playground - noun: a place where people can play

import UIKit

// MARK: - Optional Tuples

let optionalElements: (String?, String?) = ("Taylor", nil)
let optionalTuple: (String, String)? = ("Taylor", "Swift")
let optionalBoth: (String?, String?)? = (nil, "Swift")
