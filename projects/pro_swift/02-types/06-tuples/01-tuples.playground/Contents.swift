//: Playground - noun: a place where people can play

import UIKit

// MARK: - Tuples

func doNothing() { }

let result = doNothing()

let int1: (Int) = 1
let int2: Int = (1) // If there is one element inside parentheses... the value type is the value type inside

// You cannot add method to tuples... or implement protocols (instead use a struct)

var singer = ("Taylor", "Swift")
//singer = ("Taylor", "Swift", 26) //Cannot assign value of type '(String, String, Int)' to type '(String, String)'

print(singer.0)

var newSinger = (first: "Taylor", last: "Swift", address: ("555 Taylor Swift Avenue", "No, this isn't real", "Nashville"))
print(newSinger.2.2)
print(newSinger.last)

var anotherSingerTest = (first: "Taylor", last: "Swift")
//anotherSingerTest = (first: "Justin", fish: "Trout") // ERROR!
