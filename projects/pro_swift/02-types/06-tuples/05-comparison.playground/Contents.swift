//: Playground - noun: a place where people can play

import UIKit

// MARK: - Tuple Comparison

let singer = (first: "Taylor", last: "Swift")
let alien = (first: "Justin", last: "Bieber")

if singer == alien {
    print("Match!")
} else {
    print("No match!")
}


let bird = (name: "Taylor", breed: "Swift")

if singer == bird {
    print("Match!")
} else {
    print("No match!")
}

// Swift ignores the elements names when is comparing, it just cares about the values in there.
