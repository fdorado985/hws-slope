//: Playground - noun: a place where people can play

import UIKit

// MARK: - Limiting Generics

/*
func square<T: BinaryInteger>(_ value: T) -> T {
    return value * value
}
*/

func square<T: Numeric>(_ value: T) -> T {
    return value * value
}

square(42)
square(42.556)
