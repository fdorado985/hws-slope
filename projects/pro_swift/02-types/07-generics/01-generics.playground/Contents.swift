//: Playground - noun: a place where people can play

import UIKit

// MARK: - Generics

// Function

// Non generic ======
func inspectString(_ value: String) {
    print("Received String with the value \(value)")
}
inspectString("Haters gonna hate")

func inspectInt(_ value: Int) {
    print("Received Int with the value \(value)")
}
inspectInt(42)

// Generic =====
func inspect<T>(_ value: T) {
    print("Received \(type(of: value)) with the value \(value)")
}

inspect("Haters gonna hate")
inspect(56)
