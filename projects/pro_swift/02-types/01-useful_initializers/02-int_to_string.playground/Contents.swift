//: Playground - noun: a place where people can play

import UIKit

// MARK: - Int To String

// bad way
let someInteger = 101
let str1 = "\(someInteger)"

// Better way - Dedicated initializers
let str2 = String(someInteger)
let int1 = Int("elephant")

if let int2 = Int("1989") {
    print(int2)
}

// By nil coalesing
let int3 = Int("1989") ?? 0
print(int3)

let str3 = String(28, radix: 16)
let str4 = String(28, radix: 16, uppercase: true)
let int4 = Int("1C", radix: 16)


