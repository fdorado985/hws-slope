//: Playground - noun: a place where people can play

import UIKit

// MARK: - Repeating Values

let heading = "This is a heading"
let underline = String(repeating: "=", count: heading.count)

let equalsArray = [String](repeating: "=", count: heading.count)

var board = [[String]](repeating: [String](repeating: "", count: 10), count: 10)

// Printing values

print(heading)
print(underline)

print(equalsArray)
print(board)
