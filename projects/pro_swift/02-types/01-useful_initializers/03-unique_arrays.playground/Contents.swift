//: Playground - noun: a place where people can play

import UIKit

// MARK: - Unique Arrays

// Make them unique using Set's

let scores = [5, 3, 6, 1, 3, 5, 3, 9]
let scoresSet = Set(scores)
let uniqueScores = Array(scoresSet)
