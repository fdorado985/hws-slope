//: Playground - noun: a place where people can play

import UIKit

// MARK: - Dictionary Capacities

var dictionary = Dictionary<String, String>(minimumCapacity: 100)

var array = [String]()
array.reserveCapacity(100)
