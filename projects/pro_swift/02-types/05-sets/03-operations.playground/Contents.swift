//: Playground - noun: a place where people can play

import UIKit

// MARK: - Set Operations

let spaceships1 = Set(["Serenity", "Nostromo", "Enterprise"])
let spaceships2 = Set(["Voyager", "Serenity", "Executor"])
let spaceships3 = Set(["Galactica", "Sulaco", "Minbari"])

// Union avoiding duplicates

let union = spaceships1.union(spaceships2)

// Get what exists in both sets

let intersection = spaceships1.intersection(spaceships2)

// Get what is the different elementx on index

let difference = spaceships1.symmetricDifference(spaceships2)

// ======= Sub|Super Set

let spaceships1and2 = spaceships1.union(spaceships2)

spaceships1.isSubset(of: spaceships1and2) // true
spaceships1.isSubset(of: spaceships1) // true
spaceships1.isSubset(of: spaceships2) // false

spaceships1.isStrictSubset(of: spaceships1and2) // true
spaceships1.isStrictSubset(of: spaceships1) // false // This is because is identical - it is not allowed

spaceships1and2.isSuperset(of: spaceships2) // true
spaceships1and2.isSuperset(of: spaceships3) // false // There items in 3 that are not in 1 or 2

spaceships1and2.isStrictSuperset(of: spaceships1) // true

spaceships1.isDisjoint(with: spaceships2) // false
