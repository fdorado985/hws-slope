//: Playground - noun: a place where people can play

import UIKit

// MARK: - Sets Basics

// Unordered elements not duplicated

var set1 = Set<Int>([1, 2, 3, 4, 5])
var set2 = Set(1...100)
set1.insert(6)
set1.insert(7)

if set1.contains(3) {
    print("Number 3 is in there")
}

set1.remove(3)
