//: Playground - noun: a place where people can play

import UIKit

// MARK: - Set Arrays

var set1 = Set<Int>([1, 2, 3, 4, 5])
var array1 = Array(set1)
var set2 = Set(array1)

for number in set1 {
    print(number)
}

for number in set1.sorted() {
    print(number)
}
