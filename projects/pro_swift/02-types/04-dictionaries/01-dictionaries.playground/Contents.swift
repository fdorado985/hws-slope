//: Playground - noun: a place where people can play

import UIKit

// MARK: - Dictionaries

let cities = ["Shangai" : 24_256_800, "Karachi" : 23_500_000, "Beijing" : 21_516_000, "Seoul" : 9_995_000]
let roundedCities = cities.mapValues {
    "\($0 / 1_000_000) million people"
}

print(roundedCities)

let groupedCities = Dictionary(grouping: cities.keys) { $0.first! }
print(groupedCities)

let groupedCities2 = Dictionary(grouping: cities.keys) { $0.count }
print(groupedCities2)

// ========

let person = ["name" : "Taylor", "city" : "Nashville"]
//let name = person["name", default: "Anonymous"]
let name = person["name"] ?? "Anonymous"

// ========

var favoriteTVShows = ["Red Dwarf", "Blackadder", "Fawlty Towers", "Red Dwarf"]
var favoriteCounts = [String : Int]()

for show in favoriteTVShows {
    favoriteCounts[show, default: 0] += 1
}
