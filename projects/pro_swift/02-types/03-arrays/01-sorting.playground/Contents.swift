//: Playground - noun: a place where people can play

import UIKit

// MARK: - Arrays Sorting

var names = ["Taylor", "Timothy", "Tyler", "Thomas", "Tobias", "Tabitha"]
let numbers = [4, 3, 1, 9, 5, 2, 7, 8]

let sorted = names.sorted()

names.sort {
    print("Comparing \($0) and \($1)")
    
    if ($0 == "Taylor") {
        return true
    } else if $1 == "Taylor" {
        return false
    } else {
        return $0 < $1
    }
}

let lowest = numbers.min()
let highest = numbers.max()
