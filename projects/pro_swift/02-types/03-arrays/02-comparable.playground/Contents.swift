//: Playground - noun: a place where people can play

import UIKit

// MARK: - Arrays Comparable

struct Dog: Comparable {
    
    var breed: String
    var age: Int
    
    static func < (lhs: Dog, rhs: Dog) -> Bool {
        return lhs.breed < rhs.breed
    }
    
    static func == (lhs: Dog, rhs: Dog) -> Bool {
        return lhs.age == rhs.age
    }
}

let poppy = Dog(breed: "Poodle", age: 5)
let rusty = Dog(breed: "Labrador", age: 2)
let rover = Dog(breed: "Corgi", age: 11)
var dogs = [poppy, rusty, rover]

