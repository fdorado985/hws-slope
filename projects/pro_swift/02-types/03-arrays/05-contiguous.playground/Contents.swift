//: Playground - noun: a place where people can play

import UIKit

// MARK: - Array Contiguous

//let array1 = [Int]()
//let array2 = Array<Int>()
let array2 = Array<Int>(1 ... 1000000)
let array3 = ContiguousArray<Int>()

var start = CFAbsoluteTimeGetCurrent()
array2.reduce(0, +)
var end = CFAbsoluteTimeGetCurrent() - start
print("Took \(end) seconds")

start = CFAbsoluteTimeGetCurrent()
array3.reduce(0, +)
end = CFAbsoluteTimeGetCurrent() - start
print("Took \(end) seconds")
