#  AutoLayout In Code
## Description
This app is used to show 5 different labels and make them to show correctly on a `UIView` this, using `AutoLayout` but this is just by code.
This is with an easy way called: `Visual Format Language`

Of course, most people recommend you to as much as you can inside Interface Builder, anyway... use `AutoLayout` by code, is not prohibited, so you might find yourself mixing them all to get the best results.

## Demo
### Portrait
![demo_portrait](screenshots/demo_portrait.png)

### Landscape
![demo_portrait](screenshots/demo_landscape.png)

