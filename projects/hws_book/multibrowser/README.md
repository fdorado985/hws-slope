#  Multibrowser

### Description
This project will work as a multibrowser where you be able to open as many website as you can see on your iPad

## Wrap Up
On this project you will see...
- `StackViews`
- `Multitasking (iPad ONLY)`
- `UIWebView | UIWebViewDelegate`
- `UITextFieldDelegate`

## Demo
![multibrowser_demo](screenshots/multibrowser_demo.gif)
