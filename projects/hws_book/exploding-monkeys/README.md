#  Exploding Monkeys

### Description
Remake a classic DOS game and learn about destructible terrain and scene transitions (The Remake is of a classic game called Gorillas)

## Wrap Up
You will see on this project...
- `destructible terrain`
- `scene transitions`

## Demo
![exploding_monkeys_demo](screenshots/exploding_monkeys_demo.gif)

