//
//  GameScene.swift
//  space-race
//
//  Created by Juan Francisco Dorado Torres on 7/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // MARK: - Properties
    
    // Emitter of starfield object
    var starfield: SKEmitterNode!
    // Node for player
    var player: SKSpriteNode!
    
    // Node for the label
    var scoreLabel: SKLabelNode!
    // Score property
    var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }
    
    // Array that store the possible enemmies in game
    var possibleEnemies = ["ball", "hammer", "tv"]
    // The timer for the game
    var gameTimer: Timer!
    // Flag to handle if game is over
    var isGameOver = false
    
    // MARK: - View Cycle
    
    override func didMove(to view: SKView) {
        // Set background to black
        backgroundColor = UIColor.black
        
        // Create the starfield node
        starfield = SKEmitterNode(fileNamed: "Starfield")!
        // Set the position to the right edge of the screen and half way up
        starfield.position = CGPoint(x: 1024, y: 384)
        // Ask SpriteKit to simulate 10 seconds passing in the emitter, this have the effect to filling our screen with star particles
        starfield.advanceSimulationTime(10)
        // Add the particles to our scene
        addChild(starfield)
        // Set the zPosition -1 to be behind everything
        starfield.zPosition = -1
        
        // Create your player node
        player = SKSpriteNode(imageNamed: "player")
        // Set the position to be in the middle of screen and almost in the bottom
        player.position = CGPoint(x: 100, y: 384)
        // Add the physics body with it's own texture and size
        player.physicsBody = SKPhysicsBody(texture: player.texture!, size: player.size)
        // A mask that defines which categories of bodies cause intersection notifications with this physics body.
        player.physicsBody?.contactTestBitMask = 1
        // Add the player to the scene
        addChild(player)
        
        // Create your label node
        scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        // Set the position to bottom left
        scoreLabel.position = CGPoint(x: 16, y: 16)
        // Set the alignment to the left
        scoreLabel.horizontalAlignmentMode = .left
        // Add your score to the scene
        addChild(scoreLabel)
        
        // Initialize your score on 0
        score = 0
        
        // Add gravity to the physicsWorld
        physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        // Set the delegate to itself
        physicsWorld.contactDelegate = self
        
        // Initialize timer to begin create enemies
        gameTimer = Timer.scheduledTimer(timeInterval: 0.35, target: self, selector: #selector(createEnemy), userInfo: nil, repeats: true)
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        // Create the explosion emitter node
        let explosion = SKEmitterNode(fileNamed: "explosion")!
        // Add the position of the explosion equals as player position
        explosion.position = player.position
        // Add the explosion to the scene
        addChild(explosion)
        
        // Remove the player from scene
        player.removeFromParent()
        
        // Set the game over flag to true... you have LOST!
        isGameOver = true
    }
    
    // MARK: - Functions
    
    @objc func createEnemy() {
        // Shuffle the possible enemies array
        possibleEnemies = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: possibleEnemies) as! [String]
        // Generate a random number between 50 and 736
        let randomDistribution = GKRandomDistribution(lowestValue: 50, highestValue: 736)
        
        // Create the spriteNode enemy
        let sprite = SKSpriteNode(imageNamed: possibleEnemies[0])
        // Set the position, always same x position but different y position using randomDistribution
        sprite.position = CGPoint(x: 1200, y: randomDistribution.nextInt())
        // Add you enemy to scene
        addChild(sprite)
        
        // Add the physics body to the sprite
        sprite.physicsBody = SKPhysicsBody(texture: sprite.texture!, size: sprite.size)
        // Define to which mask belongs this physics body
        sprite.physicsBody?.categoryBitMask = 1
        // Add the speed for the physics body
        sprite.physicsBody?.velocity = CGVector(dx: -500, dy: 0)
        // Add the angular speed
        sprite.physicsBody?.angularVelocity = 5
        // This property reduces the body's linear velocity
        sprite.physicsBody?.linearDamping = 0
        // This property reduces the body's rotational velocity
        sprite.physicsBody?.angularDamping = 0
    }
    
    // MARK: - Touches Handler
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Get the first touch of the multiple touches
        guard let touch = touches.first else { return }
        // Get the location of the touch on current scene
        var location = touch.location(in: self)
        
        // Check for location less than 100
        if location.y < 100 {
            // The location limit must be 100
            location.y = 100
        } else if location.y > 668 {
            // Otherwise the biggest value in location y must be 668
            location.y = 668
        }
        
        // Add the location to the player position
        player.position = location
    }
    
    // MARK: - Update
    
    override func update(_ currentTime: TimeInterval) {
        // Loop through children
        for node in children {
            // Check for node position x out of screen
            if node.position.x < -300 {
                // Remove it from the scene
                node.removeFromParent()
            }
        }
        
        // Check if game is not over
        if !isGameOver {
            // Increase the score in 1
            score += 1
        }
    }
}
