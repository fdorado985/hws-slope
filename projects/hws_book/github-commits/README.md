#  Github Commits
### Description
Get started with `Core Data` by building an app to fetch and store GitHub commits for Swift

## Wrap Up
On this project you will see...
- `Core Data`
- `NSFetchRequest`
- `NSManagedObject`
- `NSPredicate`
- `NSSortDescriptor`
- `NSFetchedResultsController`
- `ISO8601DateFormatter`

## Demo
![github_commits_demo](screenshots/github_commits_demo.gif)
