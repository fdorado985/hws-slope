//
//  DetailViewController.swift
//  github-commits
//
//  Created by Juan Francisco Dorado Torres on 7/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var detailLabel: UILabel!
    
    // MARK: - Properties
    
    var detailItem: Commit?
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let detail = self.detailItem {
            detailLabel.text = detail.message
            
            // navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Commit 1/\(detail.author.commits.count)", style: .plain, target: self, action: #selector(showAuthorCommits))
        }
    }

}
