//
//  Commit+CoreDataClass.swift
//  github-commits
//
//  Created by Juan Francisco Dorado Torres on 7/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Commit)
public class Commit: NSManagedObject {

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
        print("Init called!")
    }
}
