#  Easy Browser

## Description

Of a list of three webpages, you will be able to open them through a WebKit element, even how to use a WebKit Delegate, to show name, allow or cancel url responses, let the swipe gesture to work.

## Wrap Up
On this project you see...
- loadView()
- WKWebView
- Delegation
- Classes
- Structs
- URL
- URLRequest
- UIToolBar
- UIProgressView
- KVO

## Demo
![easy_browser_demo](screenshots/easy_browser_demo.gif)

