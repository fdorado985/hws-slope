//
//  MainVC.swift
//  detect-a-beacon
//
//  Created by Juan Francisco Dorado Torres on 7/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import CoreLocation

class MainVC: UIViewController, CLLocationManagerDelegate {
  
  // MARK: - IBOutlet
  
  @IBOutlet weak var distanceReading: UILabel!
  
  // MARK: - Properties
  
  // Create the location manager to get the location
  var locationManager: CLLocationManager!
  
  // MARK: - View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Initialize the locationManager
    locationManager = CLLocationManager()
    // Set the delegate itself
    locationManager.delegate = self
    // Ask for the authorization
    locationManager.requestAlwaysAuthorization()
    
    // Add background color
    view.backgroundColor = UIColor.gray
  }
  
  // MARK: - Function
  
  func startScanning() {
    let uuid = UUID(uuidString: "5A4BCFCE-174E-4BAC-A814-092E77F6B7E5")!
    let beaconRegion = CLBeaconRegion(proximityUUID: uuid, major: 123, minor: 456, identifier: "MyBeacon")
    
    locationManager.startMonitoring(for: beaconRegion)
    locationManager.startRangingBeacons(in: beaconRegion)
  }
  
  func update(distance: CLProximity) {
    UIView.animate(withDuration: 0.8) { [unowned self] in
      switch distance {
      case .unknown:
        self.view.backgroundColor = UIColor.gray
        self.distanceReading.text = "UNKNOWN"
      case .far:
        self.view.backgroundColor = UIColor.blue
        self.distanceReading.text = "FAR"
      case .near:
        self.view.backgroundColor = UIColor.orange
        self.distanceReading.text = "NEAR"
      case .immediate:
        self.view.backgroundColor = UIColor.red
        self.distanceReading.text = "RIGHT HERE"
      }
    }
  }
  
  // MARK: - CLLocationManager Delegate
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    // Verify for change status
    if status == .authorizedAlways {
      // Verify for a Beacon region available
      if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
        // Verify if the range is available
        if CLLocationManager.isRangingAvailable() {
          startScanning()
        }
      }
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
    if beacons.count > 0 {
      let beacon = beacons[0]
      update(distance: beacon.proximity)
    } else {
      update(distance: .unknown)
    }
  }
}

