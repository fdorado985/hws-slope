#  Detect A Beacon
### Description
Learn to find and range iBeacons using our first project for a physical device, this will change label and background color depending the distance of the beacon.

## Wrap Up
On this project you will see...
- `CLLocationManager | Delegate`
- `didChangeAuthorization`
- `CLBeaconRegion`
- `didRangeBeacons`

## Demo
![detect_a_beacon_demo](screenshots/detect_a_beacon_demo.png)
