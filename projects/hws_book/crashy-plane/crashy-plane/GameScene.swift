//
//  GameScene.swift
//  crashy-plane
//
//  Created by Juan Francisco Dorado Torres on 7/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import SpriteKit
import GameplayKit

enum GameState {
    case showingLogo
    case playing
    case dead
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // MARK: - Properties
    
    /// Handle the player is going to be in the game
    var player: SKSpriteNode!
    /// The label to show the score for the player
    var scoreLabel: SKLabelNode!
    /// Save the score of the user
    var score = 0 {
        didSet {
            scoreLabel.text = "SCORE: \(score)"
        }
    }
    /// Handle the background music of the game
    var backgroundMusic: SKAudioNode!
    /// Handle the logo of the game
    var logo: SKSpriteNode!
    /// Handle the game over logo
    var gameOver: SKSpriteNode!
    /// Handle the current state of the game
    var gameState = GameState.showingLogo
    
    // MARK - View cycle
    
    override func didMove(to view: SKView) {
        createPlayer()
        createSky()
        createBackground()
        createGround()
        createScore()
        createLogos()
        
        // Add the physicsworld gravity to game
        physicsWorld.gravity = CGVector(dx: 0.0, dy: -5.0)
        physicsWorld.contactDelegate = self
        
        // Handle the background music
        if let musicURL = Bundle.main.url(forResource: "music", withExtension: "m4a") {
            backgroundMusic = SKAudioNode(url: musicURL)
            addChild(backgroundMusic)
        }
    }
    
    // MARK: - Public
    
    /// Create the player that is going to be in the game
    func createPlayer() {
        // Representation of the image in SpriteKit
        let playerTexture = SKTexture(imageNamed: "player-1")
        // Create the Player node using the texture
        player = SKSpriteNode(texture: playerTexture)
        // Set the position to be in front of everything
        player.zPosition = 10
        // Set the position to be at left and a little bit over the center on screen
        player.position = CGPoint(x: frame.width / 6, y: frame.height * 0.75)
        
        // Handle the physics for the player
        // Create the physics body for the player
        player.physicsBody = SKPhysicsBody(texture: playerTexture, size: playerTexture.size())
        // Tell us whenever the player collides with anything
        player.physicsBody!.contactTestBitMask = player.physicsBody!.collisionBitMask
        // Makes the plane respond to physics
        player.physicsBody?.isDynamic = false
        
        player.physicsBody?.collisionBitMask = 0
        
        
        // Add the player to the scene
        addChild(player)
        
        // Create the second texture for the player
        let frame2 = SKTexture(imageNamed: "player-2")
        // Create the third texture for the player
        let frame3 = SKTexture(imageNamed: "player-3")
        // Create the animation using the 3 textures using 0.01 is like to say "as fast as possible"
        let animation = SKAction.animate(with: [playerTexture, frame2, frame3, frame2], timePerFrame: 0.01)
        // Create the loop using the animation before
        let runForever = SKAction.repeatForever(animation)
        
        // Run the animation on the player
        player.run(runForever)
    }
    
    /// Create the sky that is gonna be in the game
    func createSky() {
        // Create the topsky just with color and height
        let topSky = SKSpriteNode(color: UIColor(hue: 0.55, saturation: 0.14, brightness: 0.97, alpha: 1), size: CGSize(width: frame.width, height: frame.height * 0.67))
        // Change the position to measure from their center top instead
        topSky.anchorPoint = CGPoint(x: 0.5, y: 1)
        
        // Create the bottom sky just with color and height to complete the whole screen
        let bottomSky = SKSpriteNode(color: UIColor(hue: 0.55, saturation: 0.16, brightness: 0.96, alpha: 1), size: CGSize(width: frame.width, height: frame.height * 0.33))
        // Change the position to measure from their center top instead
        bottomSky.anchorPoint = CGPoint(x: 0.5, y: 1)
        
        // Set the position for the top sky
        topSky.position = CGPoint(x: frame.midX, y: frame.height)
        // Set the position for the bottom sky
        bottomSky.position = CGPoint(x: frame.midX, y: bottomSky.frame.height)
        
        // Add the top sky node to scene
        addChild(topSky)
        // Add the bottom sky node to scene
        addChild(bottomSky)
        
        // Add the bottom sky node to be behind everything
        bottomSky.zPosition = -40
        // Add the top sky node to be behind everything
        topSky.zPosition = -40
    }
    
    /// Create the background for the game
    func createBackground() {
        // Create the texture for the background
        let backgroundTexture = SKTexture(imageNamed: "background")
        
        for i in 0 ... 1 {
            // Create the background using the texture before
            let background = SKSpriteNode(texture: backgroundTexture)
            // Add it behind everything but over the sky node
            background.zPosition = -30
            // Set the anchor point of the texture position itself from the left edge
            background.anchorPoint = CGPoint.zero
            // Calculate the X position of each mountain
            background.position = CGPoint(x: (backgroundTexture.size().width * CGFloat(i)) - CGFloat(1 * i), y: 100)
            
            // Create the movement to the left a distance equals to its width
            let moveLeft = SKAction.moveBy(x: -backgroundTexture.size().width, y: 0, duration: 20)
            // Then jump back another distance equal to its width
            let moveReset = SKAction.moveBy(x: backgroundTexture.size().width, y: 0, duration: 0)
            // Create the sequence
            let moveLoop = SKAction.sequence([moveLeft, moveReset])
            // Repeat it indefinitely
            let moveForever = SKAction.repeatForever(moveLoop)
            // Add the sequence to the background node
            background.run(moveForever)
            
            // Add background to the scene
            addChild(background)
        }
    }
    
    /// Create the ground that is gonna be in the bottom of screen
    func createGround() {
        // Create the texture for the ground
        let groundTexture = SKTexture(imageNamed: "ground")
        
        for i in 0 ... 1 {
            // Create the ground using the texture before
            let ground = SKSpriteNode(texture: groundTexture)
            // Add it behind everything but over the sky node
            ground.zPosition = -10
            // Calculate the X position of each ground
            ground.position = CGPoint(x: (groundTexture.size().width / 2.0 + (groundTexture.size().width * CGFloat(i))), y: groundTexture.size().height / 2)
            
            // Handle the collision
            // Create the collision body for the ground
            ground.physicsBody = SKPhysicsBody(texture: ground.texture!, size: ground.texture!.size())
            // Don't allow the ground be affected for the gravity
            ground.physicsBody?.isDynamic = false
            
            // Add the ground to scene
            addChild(ground)
            
            // Create the movement to the left a distance equals to its width
            let moveLeft = SKAction.moveBy(x: -groundTexture.size().width, y: 0, duration: 20)
            // Then jump back another distance equal to its width
            let moveReset = SKAction.moveBy(x: groundTexture.size().width, y: 0, duration: 0)
            // Create the sequence
            let moveLoop = SKAction.sequence([moveLeft, moveReset])
            // Repeat it indefinitely
            let moveForever = SKAction.repeatForever(moveLoop)
            // Add the sequence to the background node
            ground.run(moveForever)
        }
    }
    
    /// Create the collision for the plane
    func createRocks() {
        let rockTexture = SKTexture(imageNamed: "rock")
        
        let topRock = SKSpriteNode(texture: rockTexture)
        
        // Handle the physics
        // Create the physics body for the topRock
        topRock.physicsBody = SKPhysicsBody(texture: rockTexture, size: rockTexture.size())
        // Don't let the physics world to affect the rocks
        topRock.physicsBody?.isDynamic = false
        
        topRock.zRotation = .pi
        topRock.xScale = -1.0
        
        let bottomRock = SKSpriteNode(texture: rockTexture)
        
        // Handle the physics
        // Create the physics body for the topRock
        bottomRock.physicsBody = SKPhysicsBody(texture: rockTexture, size: rockTexture.size())
        // Don't let the physics world to affect the rocks
        bottomRock.physicsBody?.isDynamic = false
        
        topRock.zPosition = -20
        bottomRock.zPosition = -20
        
        let rockCollision = SKSpriteNode(color: UIColor.clear, size: CGSize(width: 32, height: frame.height))
        
        // Handle the physics
        // Create the physics body for the topRock
        rockCollision.physicsBody = SKPhysicsBody(rectangleOf: rockCollision.size)
        // Don't let the physics world to affect the rocks
        rockCollision.physicsBody?.isDynamic = false
        
        rockCollision.name = "scoreDetect"
        
        addChild(topRock)
        addChild(bottomRock)
        addChild(rockCollision)
        
        let xPosition = frame.width + topRock.frame.width
        
        let max = Int(frame.height / 3)
        let rand = GKRandomDistribution(lowestValue: -50, highestValue: max)
        let yPosition = CGFloat(rand.nextInt())
        
        // This next valye affects the width of the gap between rocks
        // Make it smaller to make your game harder - if you're feeling evil!
        let rockDistance: CGFloat = 70
        
        topRock.position = CGPoint(x: xPosition, y: yPosition + topRock.size.height + rockDistance)
        bottomRock.position = CGPoint(x: xPosition, y: yPosition - rockDistance)
        rockCollision.position = CGPoint(x: xPosition + (rockCollision.size.width * 2), y: frame.midY)
        
        let endPosition = frame.width + (topRock.frame.width * 2)
        
        let moveAction = SKAction.moveBy(x: -endPosition, y: 0, duration: 6.2)
        let moveSequence = SKAction.sequence([moveAction, SKAction.removeFromParent()])
        topRock.run(moveSequence)
        bottomRock.run(moveSequence)
        rockCollision.run(moveSequence)
    }
    
    /// Create and loop rocks until the player lose
    func startRocks() {
        let create = SKAction.run { [unowned self] in
            self.createRocks()
        }
        
        let wait = SKAction.wait(forDuration: 3)
        let sequence = SKAction.sequence([create, wait])
        let repeatForever = SKAction.repeatForever(sequence)
        
        run(repeatForever)
    }
    
    /// Create the Score for the game
    func createScore() {
        scoreLabel = SKLabelNode(fontNamed: "Optima-Extra-Black")
        scoreLabel.fontSize = 24
        
        scoreLabel.position = CGPoint(x: frame.midX, y: frame.maxY - 60)
        scoreLabel.text = "SCORE: 0"
        scoreLabel.fontColor = UIColor.black
        
        addChild(scoreLabel)
    }
    
    /// Create the logos
    func createLogos() {
        logo = SKSpriteNode(imageNamed: "logo")
        logo.position = CGPoint(x: frame.midX, y: frame.midY)
        addChild(logo)
        
        gameOver = SKSpriteNode(imageNamed: "gameover")
        gameOver.position = CGPoint(x: frame.midX, y: frame.midY)
        gameOver.alpha = 0
        addChild(gameOver)
    }
    
    // MARK: - Touch Handler
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        switch gameState {
        case .showingLogo:
            gameState = .playing
            
            let fadeOut = SKAction.fadeOut(withDuration: 0.5)
            let remove = SKAction.removeFromParent()
            let wait = SKAction.wait(forDuration: 0.5)
            let activatePlayer = SKAction.run { [unowned self] in
                self.player.physicsBody?.isDynamic = true
                self.startRocks()
            }
            
            let sequence = SKAction.sequence([fadeOut, wait, activatePlayer, remove])
            logo.run(sequence)
        case .playing:
            // This neutralize existing upward velocity the player has before applying the new movement.
            player.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            // Give the player a push upwards every time the player taps the screen
            player.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 20))
        case .dead:
            let scene = GameScene(fileNamed: "GameScene")!
            let transition = SKTransition.moveIn(with: SKTransitionDirection.right, duration: 1)
            self.view?.presentScene(scene, transition: transition)
        }
    }
    
    // MARK: - Update (Frame Rendered)
    
    override func update(_ currentTime: TimeInterval) {
        guard player != nil else { return }
        let value = player.physicsBody!.velocity.dy * 0.0001
        let rotate = SKAction.rotate(byAngle: value, duration: 0.1)
        player.run(rotate)
    }
    
    // MARK: - SKPhysicsContact Delegate
    
    func didBegin(_ contact: SKPhysicsContact) {
        if contact.bodyA.node?.name == "scoreDetect" || contact.bodyB.node?.name == "scoreDetect" {
            if contact.bodyA.node == player {
                contact.bodyB.node?.removeFromParent()
            } else {
                contact.bodyA.node?.removeFromParent()
            }
            
            let sound = SKAction.playSoundFileNamed("coin.wav", waitForCompletion: false)
            run(sound)
            
            score += 1
            
            return
        }
        
        guard contact.bodyA.node != nil && contact.bodyB.node != nil else {
            return
        }
        
        if contact.bodyA.node == player || contact.bodyB.node == player {
            if let explosion = SKEmitterNode(fileNamed: "PlayerExplosion") {
                explosion.position = player.position
                addChild(explosion)
            }
            
            let sound = SKAction.playSoundFileNamed("explosion.wav", waitForCompletion: false)
            run(sound)
            
            gameOver.alpha = 1
            gameState = .dead
            backgroundMusic.run(SKAction.stop())
            
            player.removeFromParent()
            speed = 0
        }
    }
}
