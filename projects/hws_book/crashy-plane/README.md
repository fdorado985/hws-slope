#  Crashy Plane
### Description
Crashy Plane? Did you remember the classic and famous Flappy Bird game... well... same here.

## Wrap Up
On this project you will see...
- `Physics`
- `Animation`
- `Infinite Scrolling`
- `SKAudioNode`
- `GKRandomDistribution`

## Demo
![crashy_plane_demo](screenshots/crashy_plane_demo.gif)
