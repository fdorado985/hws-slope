//
//  MainVC.swift
//  storm-viewer
//
//  Created by Juan Francisco Dorado Torres on 6/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UITableViewController {
  
  // MARK: - Properties
  
  var pictures = [String]()
  
  // MARK: - Initialization
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // This will be the title read for the NavigationController
    title = "Storm Viewer"
    
    // This will allow the large titles on navigationcontroller titles
    navigationController?.navigationBar.prefersLargeTitles = true
    
    // This will let us work with the filesystem
    // In our case will be used to look for files
    let fm = FileManager.default
    
    // This is to set the resource path of our app's bundle
    // The bundle is a directory containing our compiled program and all our assets.
    let path = Bundle.main.resourcePath!
    
    // This is a collection of the names of all the files that were found in the resource directory of our app.
    let items = try! fm.contentsOfDirectory(atPath: path)
    
    for item in items {
      if item.hasPrefix("nssl") {
        pictures.append(item)
      }
    }
    
    print(pictures)
  }
  
  // MARK: - TableView Delegate|Datasource
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return pictures.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
    cell.textLabel?.text = pictures[indexPath.row]
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // 1: Try to load the "Detail" view controller and typecasting it to be DetailViewController
    if let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailVC {
      // 2. Success! Set its selected image property
      vc.selectedImage = pictures[indexPath.row]
      
      // 3. Now push it onto the navigation controller
      navigationController?.pushViewController(vc, animated: true)
    }
  }
}

