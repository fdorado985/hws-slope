#  Storm Viewer

## Description
This app will let you choose an image of a given list, that is take it from the files inside the projects, to let you see it in a Detail View.

## Wrap Up
You will see on this project
* Constants and Variables
* Function Overrides
* Table Views, image views
* App bundles
* File Manager
* typecasting
* arrays
* loops
* optionals
* view controllers
* storyboards
* outlets
* Auto Layout
* UIImage

## Demo
![demo](screenshots/storm_viewer_demo.gif)
