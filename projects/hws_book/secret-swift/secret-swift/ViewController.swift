//
//  ViewController.swift
//  secret-swift
//
//  Created by Juan Francisco Dorado Torres on 7/11/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var txtSecret: UITextView!
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard(_:)), name: .UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard(_:)), name: .UIKeyboardWillChangeFrame, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(saveSecretMessage), name: .UIApplicationWillResignActive, object: nil)
        
        title = "Nothing to see here"
    }
    
    // MARK: - Public
    
    @objc func adjustForKeyboard(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let NSKeyboardScreenEndFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
                return
        }
        
        let keyboardScreenEndFrame = NSKeyboardScreenEndFrame.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        txtSecret.contentInset = notification.name == Notification.Name.UIKeyboardWillHide ? UIEdgeInsets.zero : UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        txtSecret.scrollIndicatorInsets = txtSecret.contentInset
        
        let selectedRange = txtSecret.selectedRange
        txtSecret.scrollRangeToVisible(selectedRange)
    }
    
    func unlockSecretMessage() {
        txtSecret.isHidden = false
        title = "Secret stuff!"
        
        if let text = KeychainWrapper.standard.string(forKey: "SecretMessage") {
            txtSecret.text = text
        }
    }
    
    @objc func saveSecretMessage() {
        if !txtSecret.isHidden {
            _ = KeychainWrapper.standard.set(txtSecret.text, forKey: "SecretMessage")
            txtSecret.resignFirstResponder()
            txtSecret.isHidden = true
            title = "Nothing to see here"
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnAuthenticateTapped(_ sender: UIButton) {
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [unowned self] (success, authenticationError) in
                DispatchQueue.main.async {
                    if success {
                        self.unlockSecretMessage()
                    } else {
                        let ac = UIAlertController(title: "Authentication failed", message: "You could not be verified; please try again.", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                    }
                }
            }
        } else {
            let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
}

