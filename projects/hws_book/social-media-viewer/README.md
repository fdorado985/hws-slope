#  Social Media (Storm Viewer Update)

## Description
This app takes the `storm-viewer` app that was the first app created for this list of projects, and we add a way to share the images.

## Wrap Up
- UIBarButtonItem
- UIActivityViewController

## Demo
![demo](screenshots/social_media_demo.gif)

