//
//  ViewController.swift
//  guess-the-flag
//
//  Created by Juan Francisco Dorado Torres on 6/16/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import GameplayKit

class ViewController: UIViewController {
  
  // MARK: - IBOutlet

  @IBOutlet weak var btnTopFlag: UIButton!
  @IBOutlet weak var btnMiddleFlag: UIButton!
  @IBOutlet weak var btnBottomFlag: UIButton!
  
  // MARK: - Properties
  
  var countries = [String]()
  var correctAnswer = 0
  var score = 0
  
  // MARK: - Initialization
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    countries += ["estonia", "france", "germany",
                  "ireland", "italy", "monaco",
                  "nigeria", "poland", "russia",
                  "spain", "uk", "us"]
    
    btnTopFlag.layer.borderWidth = 1
    btnMiddleFlag.layer.borderWidth = 1
    btnBottomFlag.layer.borderWidth = 1
    
    btnTopFlag.layer.borderColor = UIColor.lightGray.cgColor
    btnMiddleFlag.layer.borderColor = UIColor.lightGray.cgColor
    btnBottomFlag.layer.borderColor = UIColor.lightGray.cgColor
    
    askQuestion()
  }
  
  // MARK: - IBActions
  
  @IBAction func btnFlagTapped(_ sender: UIButton) {
    if sender.tag == correctAnswer {
      title = "Correct"
      score += 1
    } else {
      title = "Wrong"
      score -= 1
    }
    
    let alert = UIAlertController(title: title, message: "Your score is \(score)", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: askQuestion))
    present(alert, animated: true)
  }
  
  // MARK: - Functions
  
  func askQuestion(action: UIAlertAction! = nil) {
    countries = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: countries) as! [String]
    correctAnswer = GKRandomSource.sharedRandom().nextInt(upperBound: 3)
    title = countries[correctAnswer].uppercased()
    
    btnTopFlag.setImage(UIImage(named: countries[0]), for: .normal)
    btnMiddleFlag.setImage(UIImage(named: countries[1]), for: .normal)
    btnBottomFlag.setImage(UIImage(named: countries[2]), for: .normal)
  }
}

