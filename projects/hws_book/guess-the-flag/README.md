#  Guess The Flag

## Description

This is a game using UIKit, where you can see how is integrated buttons, integers, colors and actions.


## Wrap Up

On this projects we see
- Interface builder
- Auto Layout
- Outlets

What is new?
- Learn about @2x and @3x images
- Asset catalogs
- Integers
- Doubles
- Floats
- Operators (+= and -=)
- UIButton
- Enums
- CALayer
- UIColor
- Random Numbers
- Actions
- String Interpolation
- UIAlertController

## Demo

![guess_the_flag_demo](screenshots/guess_the_flag_demo.gif)
