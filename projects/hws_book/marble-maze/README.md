#  Marble Maze

### Description
Respond to device tilting by steering a ball around a vortex maze.

## Wrap Up
On thins project you will see...
- `Accelerometer`
- `Multiple Contact Bitmasks`
- `Code that works just on Simulator not on devices (or vice versa)`

## Demo
![marble_maze_demo](screenshots/marble_maze_demo.gif)

