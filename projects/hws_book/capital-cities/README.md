#  Capital Cities

### Description
Teach users about geography while you learn about MKMapView and annotations.

## Wrap Up
On this project you will see...
- `MKMapView`
- `MKAnnotation`
- `MKPinAnnotationView`
- `CLLocationCoordinate2D`
- `dequeueReusableAnnotationView`

## Demo
![capital_cities_demo](screenshots/capital_cities_demo.gif)
