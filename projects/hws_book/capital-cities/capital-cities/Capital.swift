//
//  Capital.swift
//  capital-cities
//
//  Created by Juan Francisco Dorado Torres on 6/29/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit

class Capital: NSObject, MKAnnotation {
  
  // MARK: - Properties
  
  var title: String?
  var coordinate: CLLocationCoordinate2D
  var info: String
  
  // MARK - Init(title: coordinate: info:)
  
  init(title: String, coordinate: CLLocationCoordinate2D, info: String) {
    self.title = title
    self.coordinate = coordinate
    self.info = info
  }
}
