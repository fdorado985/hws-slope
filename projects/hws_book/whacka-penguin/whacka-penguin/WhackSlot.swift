//
//  WhackSlot.swift
//  whacka-penguin
//
//  Created by Juan Francisco Dorado Torres on 6/22/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import SpriteKit

/**
 This encapsulate all hole related functionality
 
 * Important: SKNode doesn't draw images like sprites or hold text like labels; it just sits in our scene at a position, holding other nodes as children.
 */
class WhackSlot: SKNode {
  
  // MARK: - Properties
  
  // This property will store the penguin picture node
  var charNode: SKSpriteNode!
  // Let us know if the penguin is visible or not for the player
  var isVisible = false
  // Let us know if the player already hits the penguin (this avoid to be hitted twice or more)
  var isHit = false

  // MARK: - Functions
  
  func configure(at position: CGPoint) {
    // We will set this our current position
    self.position = position
    // We get the SKSpriteNode of the hole image
    let sprite = SKSpriteNode(imageNamed: "whackHole")
    // After those configurations we add it to the scene
    addChild(sprite)
    
    // We create a new SKCropNode
    let cropNode = SKCropNode()
    // We set it slightyly higher than the slot itself
    cropNode.position = CGPoint(x: 0, y: 15)
    // We set the zPosition to put it to the front of other nodes, and stops it from appearing behing the hole
    cropNode.zPosition = 1
    // Set the Mask that you will need to hide the penguin
    cropNode.maskNode = SKSpriteNode(imageNamed: "whackMask")
    
    // We create the caracter node of penguinGood
    charNode = SKSpriteNode(imageNamed: "penguinGood")
    // We set the position that is just under 90 in y position, to make it be under the hole
    charNode.position = CGPoint(x: 0, y: -90)
    // We set the name used as identifier for the penguin
    charNode.name = "character"
    // Finally we add the penguin on the Crop Node to make it show overt the cropNode
    cropNode.addChild(charNode)
    
    // After all configuration we add the cropNode to the scene
    addChild(cropNode)
  }
  
  func show(hideTime: Double) {
    // We verify if the penguin is already visible
    if isVisible { return }
    
    // Because we shrink when we hit a bad penguin we need to reset that shrink when it appears
    // Reset the shrink animation of the penguin to its original before be shown in xScale
    charNode.xScale = 1
    // Reset the shrink animation of the penguin to its original before be shown in yScale
    charNode.yScale = 1
    
    // This create a movement action, to make our penguin appear
    charNode.run(SKAction.moveBy(x: 0, y: 80, duration: 0.05))
    // After make it appear we change the flags now that it is visible
    isVisible = true
    // It just appear so it is not hitted yet
    isHit = false
    
    // This random numbers goes through 0 to 2... so just when it is 0 is the good penguin
    if RandomInt(min: 0, max: 2) == 0 {
      // If we get a 0 we change the current penguin for the good one
      charNode.texture = SKTexture(imageNamed: "penguinGood")
      // We change the name that works as identifier to know that is the good one
      charNode.name = "charFriend"
    } else {
      // If we get 1 or 2 we change the current penguin for the bad one
      charNode.texture = SKTexture(imageNamed: "penguinEvil")
      // We change the name that works as identifier to know that is the bad one
      charNode.name = "charEnemy"
    }
    
    // After appearing we already have a little time delay that will let us hide the penguin
    DispatchQueue.main.asyncAfter(deadline: .now() + (hideTime * 3.5)) { [unowned self] in
      self.hide()
    }
  }
  
  func hide() {
    // We verify if the penguin is not visible to don't move it down more
    if !isVisible { return }
    
    // This create a movement action, to make our penguin disappear
    charNode.run(SKAction.moveBy(x: 0, y: -80, duration: 0.05))
    // After make it disappear we change the flag of the current slot that is not visible
    isVisible = false
  }
  
  func hit() {
    // At the time we hit a penguin we change the flag to avoid double hitting on same penguin
    isHit = true
    
    // This delay is an action that waits for a period of time (mesure in seconds)
    let delay = SKAction.wait(forDuration: 0.25)
    // This will produce the movement of the penguin to hide it
    let hide = SKAction.moveBy(x: 0, y: -80, duration: 0.5)
    // This will run any code we want, provides as a closure (we hide the penguin as a sequence because we want it to be hidden after it gets hide)
    let notVisible = SKAction.run { [unowned self] in
      self.isVisible = false
    }
    
    // After those three parameters, we add those as sequence, this will execute them in order (Each action won't start executing until the previous one finished)
    charNode.run(SKAction.sequence([delay, hide, notVisible]))
  }
}
