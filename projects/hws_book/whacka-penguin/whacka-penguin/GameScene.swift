//
//  GameScene.swift
//  whacka-penguin
//
//  Created by Juan Francisco Dorado Torres on 6/22/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
  
  // MARK: - Properties
  
  // This will save the slots (container) that we are gonna be using in the game (holes, penguins)
  var slots = [WhackSlot]()
  // This popUp time will help us to create a new enemy a bit faster than once a second (this will be decreased each time)
  var popupTime = 0.85
  // This will let us handle the limit of rounds... this will be 30
  var numRounds = 0
  // This is the gamescore label that you will see on screen
  var gameScore: SKLabelNode!
  // This computed property save the score and update the label with it
  var score = 0 {
    didSet {
      gameScore.text = "Score: \(score)"
    }
  }
  
  // MARK: - View Cycle
  
  override func didMove(to view: SKView) {
    // ADD THE IMAGE BACKGROUND
    // Get the image from the Bundle as SKSpriteNode
    let background = SKSpriteNode(imageNamed: "whackBackground")
    // Set the image on the center of the screen
    background.position = CGPoint(x: 512, y: 384)
    // This blender will redraw completely the image.
    background.blendMode = .replace
    // This allow to set the image behind everyhing and use it as backogrund
    background.zPosition = -1
    // After those configurations, finally we add the image on the scene
    addChild(background)
    
    // ADD THE GAMESCORE LABEL
    // Init the label with Chalkduster font
    gameScore = SKLabelNode(fontNamed: "Chalkduster")
    // An init value on the label
    gameScore.text = "Score: 0"
    // Set the position where you want your label... here in the bottom left
    gameScore.position = CGPoint(x: 8, y: 8)
    // The text alignment to be left
    gameScore.horizontalAlignmentMode = .left
    // Change the font size 48
    gameScore.fontSize = 48
    // After the configurations, add it to the scene
    addChild(gameScore)
    
    // Loops to create Slots
    // This creates the 5 holes at the top of the scene
    for i in 0..<5 {
      createSlot(at: CGPoint(x: 100 + (i * 170), y: 410))
    }
    
    // This creates the 4 holes at second row up to down
    for i in 0..<4 {
      createSlot(at: CGPoint(x: 180 + (i * 170), y: 320))
    }
    
    // This creates the 5 holes at third row
    for i in 0..<5 {
      createSlot(at: CGPoint(x: 100 + (i * 170), y: 230))
    }
    
    // This creates the 4 holes at the bottom
    for i in 0..<4 {
      createSlot(at: CGPoint(x: 180 + (i * 170), y: 140))
    }
    
    // We call create enemy giving a delay of 1 second that let the user focus before the game start
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [unowned self] in
      self.createEnemy()
    }
  }
  
  // MARK: - Functions
  
  /**
   This method will be used to handle the slot creation
   
   - parameter position: The position where the slot will be created
   */
  func createSlot(at position: CGPoint) {
    // We create a new Slot from our WhackSlot class
    let slot = WhackSlot()
    // We configure our slot to be created on the given position
    slot.configure(at: position)
    // After configure it, we add it to the scene
    addChild(slot)
    // We save this slot on our array
    slots.append(slot)
  }
  
  func createEnemy() {
    // Because of the popupTime is going to decrese, is gonna be difficult to play after a couple of minutes
    // He we are gonna limit the game to 30 rounds of enemies, each round is one call to createEnemy()
    numRounds += 1
    
    // Here we verify if the num of rounds still being less than 30, to keep appearing penguins
    if numRounds >= 30 {
      // We take a look for each slot to hide it (REMEMBER, the game is over at the time you get in here)
      for slot in slots {
        slot.hide()
      }
      
      // We create a new SpriteNode that have the image GameOver
      let gameOver = SKSpriteNode(imageNamed: "gameOver")
      // We set our SpriteNode on the middle of scene
      gameOver.position = CGPoint(x: 512, y: 384)
      // We set our SpriteNode in front of everything in our scene
      gameOver.zPosition = 1
      // We add the spriteNode to our scene
      addChild(gameOver)
      
      // We finish this function to avoid generate more enemies
      return
    }
    
    // We decrease popUpTime each time it's called. I'm going to multiply it by 0.991 rather than substracting a fixed amount.
    popupTime *= 0.991
    
    // We shuffle the list of available slots
    slots = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: slots) as! [WhackSlot]
    // We make the first slot show itself, passing in the current value of popupTime for the method to use later.
    slots[0].show(hideTime: popupTime)
    
    // We generate 4 new random numbers to see if more slots should be shown, potentially up to five slots could be shown at once
    if RandomInt(min: 0, max: 12) > 4 {
      slots[1].show(hideTime: popupTime)
    }
    
    if RandomInt(min: 0, max: 12) > 8 {
      slots[2].show(hideTime: popupTime)
    }
    
    if RandomInt(min: 0, max: 12) > 10 {
      slots[3].show(hideTime: popupTime)
    }
    
    if RandomInt(min: 0, max: 12) > 11 {
      slots[4].show(hideTime: popupTime)
    }
    
    // We get the minDelay that is the current popupTime / 2.0
    let minDelay = popupTime / 2.0
    // We get the maxDelagu that is current popupTime * 2
    let maxDelay = popupTime * 2
    // We get the delay using the two values before
    let delay = RandomDouble(min: minDelay, max: maxDelay)
    
    // We call itself again after the random delay we already generate
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [unowned self] in
      self.createEnemy()
    }
  }
  
  // MARK: - Touches Handler
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    // We get the first touch when the user tapped
    if let touch = touches.first {
      // We get the location of the touch itself.
      let location = touch.location(in: self)
      // We get the whole nodes at that location (To know what nodes does the user tapped)
      let tappedNodes = nodes(at: location)
      
      // We are going to look for each node in tappedNodes
      for node in tappedNodes {
        // We verify if the node tapped is a goodPenguin otherwise is a badPenguin (line 181)
        if node.name == "charFriend" {
          // They shouldn't have whacked this penguin
          // We get the whackSlot... this is the sequence, you tap the penguin that is the child of cropNode that this is child of the whackSlot
          let whackSlot = node.parent!.parent as! WhackSlot
          // Then we verify if the whackSlot (we care about the penguin) is not visible... then go out continue the loop
          if !whackSlot.isVisible { continue }
          // Then we verify if the whackSlot (we care about the penguin) is already hit... then go out and continue the loop
          if whackSlot.isHit { continue }
          
          // Here we are call the hit function, because the user hit a penguin
          whackSlot.hit()
          // We substract five points cause this was a good penguin
          score -= 5
          
          // This line plays a sound and optionally waits for the sound to finish playing before continuing - useful if you are using an action sequence.
          run(SKAction.playSoundFileNamed("whackBad.caf", waitForCompletion: false))
        } else if node.name == "charEnemy" {
          // They should have whacked this one
          // We get the whackSlot... this is the sequence, you tap the penguin that is the child of cropNode that this is child of the whackSlot
          let whackSlot = node.parent!.parent as! WhackSlot
          // Then we verify if the whackSlot (we care about the penguin) is not visible... then go out continue the loop
          if !whackSlot.isVisible { continue }
          // Then we verify if the whackSlot (we care about the penguin) is already hit... then go out and continue the loop
          if whackSlot.isHit { continue }
          
          // This line will let us shrink our character in xScale
          whackSlot.charNode.xScale = 0.85
          // This line will let us shrink our character in yScale
          whackSlot.charNode.yScale = 0.85
          
          // Here we are call the hit function, because the user hit a penguin
          whackSlot.hit()
          // We add 1 point for hit a bad Penguin
          score += 1
          
          // This line plays a sound and optionally waits for the sound to finish playing before continuing - useful if you are using an action sequence.
          run(SKAction.playSoundFileNamed("whack.caf", waitForCompletion: false))
        }
      }
    }
  }
}
