#  Whack A Penguin

### Description

Build a game using `SKCropNode` and a sprinkling of `Grand Central Dispatch`

## Wrap Up
Here you will see:
- `SKCropNode`
- `SKTexture`
- Some more types of `SKAction`
- `GCD` to execute closures after a delay

## Demo
![whacka_penguin_demo](screenshots/whacka_penguin_demo.gif)
