#  Unit Testing
### Description
On this app you will get a play of Shakespeare where you will get each word and a counter of each in a tableView... this will be used to see how `Unit|UI Test` works.

## Wrap Up
On this project you will see...
- `Unit Test`
- `UI Test`
- `NSCountedSet`

## Demo
![unit_testing_demo](screenshots/unit_testing_demo.gif)

## Unit Test
![unit_test_passed](screenshots/unit_test_passed.png)
