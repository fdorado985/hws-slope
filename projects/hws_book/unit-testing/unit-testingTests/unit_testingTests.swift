//
//  unit_testingTests.swift
//  unit-testingTests
//
//  Created by Juan Francisco Dorado Torres on 7/16/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest
@testable import unit_testing

class unit_testingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testAllWordsLoaded() {
        let playData = PlayData()
        XCTAssertEqual(playData.allWords.count, 18440, "allWords should be 384,001, instead it was \(playData.allWords.count)")
    }
    
    func testWordCountsAreCorrect() {
        let playData = PlayData()
        XCTAssertEqual(playData.wordCounts.count(for: "home"), 174, "Home does not appear 174 times")
        XCTAssertEqual(playData.wordCounts.count(for: "fun"), 4, "Fun does not appear 4 times")
        XCTAssertEqual(playData.wordCounts.count(for: "mortal"), 41, "Mortal does not appear 41 times")
    }
    
    func testWordsLoadQuickly() {
        measure {
            _ = PlayData()
        }
    }
    
    func testUserFilterWorks() {
        let playData = PlayData()
        
        playData.applyUserFilter("100")
        XCTAssertEqual(playData.filteredWords.count, 495, "Using filter '100' should appear 495 times, instead we got \(playData.filteredWords.count)")
        
        playData.applyUserFilter("1000")
        XCTAssertEqual(playData.filteredWords.count, 55, "Using filter '1000' should appear 55 times, instead we got \(playData.filteredWords.count)")
        
        playData.applyUserFilter("10000")
        XCTAssertEqual(playData.filteredWords.count, 1, "Using filter '10000' should appear 1 times, instead we got \(playData.filteredWords.count)")
        
        playData.applyUserFilter("test")
        XCTAssertEqual(playData.filteredWords.count, 56, "Using filter 'test' should appear 56 times, instead we got \(playData.filteredWords.count)")
        
        playData.applyUserFilter("swift")
        XCTAssertEqual(playData.filteredWords.count, 7, "Using filter 'swift' should appear 7 times, instead we got \(playData.filteredWords.count)")
        
        playData.applyUserFilter("objective-c")
        XCTAssertEqual(playData.filteredWords.count, 0, "Using filter 'objective-c' should appear 0 times, instead we got \(playData.filteredWords.count)")
    }
}
