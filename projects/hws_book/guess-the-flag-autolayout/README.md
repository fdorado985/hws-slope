#  Guess The Flag

## Description

This app is a copy of the Project 2 (Guess The Flag), same intructions... it is an easy game where you need to guess which is the flag of the given name, the difference in here is that you can rotate your device and you can still playing without problem. How do we do this... `AUTOLAYOUT`...

So in this project you will be able to see how the `AutoLayout` rules work on a `Storyboard`.

## Demo
![guess_the_flag_demo](screenshots/guess_the_flag_demo.gif)

