#  Names To Faces
## Description
This app is just a fix of the original project `Names To Faces`, the difference is that here we'll see how to store data

## Wrap Up
Here you will see:
- `UserDefaults`
- `NSCoding`
- `Codable`

## Demo
![names_to_faces_demo](screenshots/names_to_faces_demo.gif)
