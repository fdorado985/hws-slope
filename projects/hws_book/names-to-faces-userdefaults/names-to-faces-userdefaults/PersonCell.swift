//
//  PersonCell.swift
//  names-to-faces
//
//  Created by Juan Francisco Dorado Torres on 6/20/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var name: UILabel!
    
}
