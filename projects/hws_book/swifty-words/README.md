#  Swifty Words

## Description
Build a word-guessing game and master strings once and for all, here you are going to guess a word according to some clues, you will create the words with the word suggestion at the bottom.

## Wrap-Up
Here you see about
- `addTarget()`
- `enumerated()`
- `index(of:)`
- `joined()`
- `replacingOccurrences()`
- `property`
- `observers`
- `range Operators`

## Demo
![demo](screenshots/swifty_words_demo.gif)

