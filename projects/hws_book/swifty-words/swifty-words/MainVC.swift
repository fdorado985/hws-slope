//
//  MainVC.swift
//  swifty-words
//
//  Created by Juan Francisco Dorado Torres on 6/19/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import GameplayKit

class MainVC: UIViewController {
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var lblClues: UILabel!
  @IBOutlet weak var lblAnswers: UILabel!
  @IBOutlet weak var lblCurrentAnswer: UITextField!
  @IBOutlet weak var lblScore: UILabel!
  
  // MARK: - Properties
  
  var letterButtons = [UIButton]()
  var activatedButtons = [UIButton]()
  var solutions = [String]()
  
  var score = 0 {
    didSet {
      lblScore.text = "Score: \(score)"
    }
  }
  var level = 1
  
  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
    
    for subview in view.subviews where subview.tag == 1001 {
      let btn = subview as! UIButton
      letterButtons.append(btn)
      btn.addTarget(self, action: #selector(letterTapped), for: .touchUpInside)
    }
    
    loadLevel()
  }
  
  // MARK: - IBActions
  
  @IBAction func submitTapped(sender: UIButton) {
    if let solutionPosition = solutions.index(of: lblCurrentAnswer.text!) {
      activatedButtons.removeAll()
      
      var splitAnswers = lblAnswers.text!.components(separatedBy: "\n")
      splitAnswers[solutionPosition] = lblCurrentAnswer.text!
      lblAnswers.text = splitAnswers.joined(separator: "\n")
      lblCurrentAnswer.text = ""
      score += 1
      
      if score % 7 == 0 {
        let alert = UIAlertController(title: "Well done!", message: "Are you ready for the next level?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Let's go!", style: .default, handler: levelUp))
        present(alert, animated: true)
      }
    }
  }
  
  @IBAction func clearTapped(sender: UIButton) {
    lblCurrentAnswer.text = ""
    
    for btn in activatedButtons {
      btn.isHidden = false
    }
    
    activatedButtons.removeAll()
  }
  
  // MARK: - Functions
  
  func loadLevel() {
    var strClue = ""
    var strSolution = ""
    var bitsLetter = [String]()
    
    if let levelFilePath = Bundle.main.path(forResource: "level\(level)", ofType: "txt") {
      if let levelContents = try? String(contentsOfFile: levelFilePath) {
        var lines = levelContents.components(separatedBy: "\n")
        lines = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: lines) as! [String]
        
        for (index, line) in lines.enumerated() {
          let parts = line.components(separatedBy: ": ")
          let answer = parts[0]
          let clue = parts[1]
          strClue += "\(index + 1). \(clue)\n"
          
          let solutionWord = answer.replacingOccurrences(of: "|", with: "")
          strSolution += "\(solutionWord.count) letters\n"
          solutions.append(solutionWord)
          
          let bits = answer.components(separatedBy: "|")
          bitsLetter += bits
        }
      }
    }
    
    // Now configure the buttons and labels
    lblClues.text = strClue.trimmingCharacters(in: .whitespacesAndNewlines)
    lblAnswers.text = strSolution.trimmingCharacters(in: .whitespacesAndNewlines)
    bitsLetter = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: bitsLetter) as! [String]
    
    if bitsLetter.count == letterButtons.count {
      for i in 0..<bitsLetter.count {
        letterButtons[i].setTitle(bitsLetter[i], for: .normal)
      }
    }
  }
  
  func levelUp(action: UIAlertAction) {
    level += 1
    solutions.removeAll(keepingCapacity: true)
    loadLevel()
    
    for btn in letterButtons {
      btn.isHidden = false
    }
  }
  
  @objc func letterTapped(btn: UIButton) {
    lblCurrentAnswer.text = lblCurrentAnswer.text! + btn.titleLabel!.text!
    activatedButtons.append(btn)
    btn.isHidden = true
  }
}

