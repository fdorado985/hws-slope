#  Local Notifications

### Description
Send reminders, prompts and alerts even when your app isn't running

## Wrap Up
On this project you will see about...
- `UNUserNotificationCenterDelegate`
- `UNUserNotificationCenter`
- `UNMutableNotificationContent`
- `DateComponents`
- `UNTimeIntervalNotificationTrigger`
- `UNNotificationRequest`
- `UNNotificationAction`
- `UNNotificationCategory`

## Demo
![local_notifications_demo](screenshots/local_notifications_demo.gif)
