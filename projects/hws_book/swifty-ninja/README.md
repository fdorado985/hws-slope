#  Swifty Ninja

#### Description
Little clone of the popular game `Fruit Ninja` using penguins instead of fruits.

## Wrap Up
Here you are gonna learn more stuffs for Sprite Kit like
- `SKShapeNode`
- `AVAudioPlayer`
- `UIBezierPath`

And all those elements that you already know...

## Demo
![swifty_ninja_demo](screenshots/swifty_ninja_demo.gif)

