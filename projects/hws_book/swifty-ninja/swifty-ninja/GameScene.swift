//
//  GameScene.swift
//  swifty-ninja
//
//  Created by Juan Francisco Dorado Torres on 6/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import SpriteKit
import AVFoundation

// This will handle the sequence to create enemies
enum SequenceType: Int {
  case oneNoBomb, one, twoWithOneBomb, two, three, four, chain, fastChain
}

// This enum will handle the situations to appear of a bomb
enum ForceBomb {
  case never, always, random
}

class GameScene: SKScene {
  
  // MARK: - Properties
  
  // The label to show the score
  var gameScore: SKLabelNode!
  // The score value that will be changing to our label
  var score = 0 {
    didSet {
      gameScore.text = "Score: \(score)"
    }
  }
  
  // We store the iamges of the lives here (hearts)
  var livesImages = [SKSpriteNode]()
  // The amount of lives that the user will have per game.
  var lives = 3
  
  // Shape for the foreground slice
  var activeSliceBG: SKShapeNode!
  // Shape for the background slice
  var activeSliceFG: SKShapeNode!
  
  // Here we save our swipe points
  var activeSlicePoints = [CGPoint]()
  
  // A flag that let us handle the sound to avoid be playing it one over other
  var isSwooshSoundActive = false
  // A property that will handle the bomb sound
  var bombSoundEffect: AVAudioPlayer!
  
  // This array will store the current enemies active on scene
  var activeEnemies = [SKSpriteNode]()
  
  // This is the amount of time to wait between the last enemy being destroyed and a new one being created
  var popupTime = 0.9
  // This array of our sequenceType defines what enemies to create
  var sequence: [SequenceType]!
  // This store where we are right now in the game
  var sequencePosition = 0
  // This property store how long to wait before creating a new enemy when the sequence type is .chain or .fastChain
  var chainDelay = 3.0
  // With this we know when all the enemies are destroyed and we're ready to create more
  var nextSequenceQueued = true
  
  // Flag that let us know if the game is already finished
  var gameEnded = false
  
  // MARK: - View Cycle
  
  override func didMove(to view: SKView) {
    // Set the background
    // Create the SKSpriteNode with the image
    let background = SKSpriteNode(imageNamed: "sliceBackground")
    // We set it in the middle of the screen
    background.position = CGPoint(x: 512, y: 384)
    // We redraw completely the image
    background.blendMode = .replace
    // We set it just behind of everything in the scene
    background.zPosition = -1
    // We add it to the scene
    addChild(background)
    
    physicsWorld.gravity = CGVector(dx: 0, dy: -6)
    // This speed gravity default is -0.98 like the earth gravity, slightly lower will make the items stay up in the air a little bit longer
    physicsWorld.speed = 0.85
    
    createScore()
    createLives()
    createSlices()
    
    // We initialize the sequence with the first 7 sequences
    sequence = [.oneNoBomb, .oneNoBomb, .twoWithOneBomb, .twoWithOneBomb, .three, .one, .chain]
    
    // Through a loop we are gonna be adding 1000 sequences more
    for _ in 0 ... 1000 {
      let nextSequence = SequenceType(rawValue: RandomInt(min: 2, max: 7))!
      sequence.append(nextSequence)
    }
    
    // Then create enemies after 2 seconds delay
    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { [unowned self] in
      self.tossEnemies()
    }
    
  }
  
  // MARK: - Functions
  
  func createScore() {
    // Create and init our label with a specific font
    gameScore = SKLabelNode(fontNamed: "Chalkduster")
    // Add the beginning text to show on label
    gameScore.text = "Score: 0"
    // Align the text to the left side
    gameScore.horizontalAlignmentMode = .left
    // Set the font size you want on the label
    gameScore.fontSize = 48
    // Add the SKLabelNode to our scene
    addChild(gameScore)
    
    // Adjust the positon of it, where you can to see it in scene
    gameScore.position = CGPoint(x: 8, y: 8)
  }
  
  func createLives() {
    // For loop that let create the images for the game (the hearts)
    for i in 0 ..< 3 {
      // We create our SKSpriteNode with the image for life
      let spriteNode = SKSpriteNode(imageNamed: "sliceLife")
      // We add the position, the same y value for all and the x value depends the life created
      spriteNode.position = CGPoint(x: CGFloat(834 + (i * 70)), y: 720)
      // We add the our image to scene
      addChild(spriteNode)
      // Finally we add it to our lives array to handle when the user lost one life.
      livesImages.append(spriteNode)
    }
  }
  
  func createSlices() {
    // We create our shape for background slice
    activeSliceBG = SKShapeNode()
    // We must set it in front of everything on our scene
    activeSliceBG.zPosition = 2
    
    // We create our shape for foreground slice
    activeSliceFG = SKShapeNode()
    // We must set it in front of everything on our scene
    activeSliceFG.zPosition = 2
    
    // We set our color to our stroke that is gonna be between red and green
    activeSliceBG.strokeColor = UIColor(red: 1, green: 0.9, blue: 0, alpha: 1)
    // We set our width
    activeSliceBG.lineWidth = 9
    
    // We set our color to our stroke that is gonna be white
    activeSliceBG.strokeColor = UIColor.white
    // We set out widht (you can see that is thinner than the background, is because it is gonna be over the other)
    activeSliceBG.lineWidth = 5
    
    // We add our background slice to our scene
    addChild(activeSliceBG)
    // We add our foreground slice to our scene
    addChild(activeSliceFG)
  }
  
  func redrawActiveSlice() {
    // Check to don't have less than 2 points
    if activeSlicePoints.count < 2 {
      // Otherwise we don't have enough data, we clear the shape and exit the method
      activeSliceBG.path = nil
      activeSliceFG.path = nil
      return
    }
    
    // Loop if we have more than 12 points
    while activeSlicePoints.count > 12 {
      // We remove the oldest ones until we have at most 12 (we avoid the swipe to be too long)
      activeSlicePoints.remove(at: 0)
    }
    
    // We create the path to draw the line
    let path = UIBezierPath()
    // We set where we want to start the movement
    path.move(to: activeSlicePoints[0])
    
    // Iterate through slice points
    for i in 1 ..< activeSlicePoints.count {
      // To be able to draw the line
      path.addLine(to: activeSlicePoints[i])
    }
    
    // We add the path to our slice to show it on scene
    activeSliceBG.path = path.cgPath
    // We add the path to our slice to show it on scene
    activeSliceFG.path = path.cgPath
  }
  
  func playSwooshSound() {
    // We set our flag to true
    isSwooshSoundActive = true
    
    // We create a randomNumber between 1 to 3
    let randomNumber = RandomInt(min: 1, max: 3)
    // To let us get a random SoundName
    let soundName = "swoosh\(randomNumber).caf"
    
    // We get the sound from a file name and set true the clousure for completion
    let swooshSound = SKAction.playSoundFileNamed(soundName, waitForCompletion: true)
    // We play our sound
    run(swooshSound) { [unowned self] in
      // After it finishes, our flag is back to false
      self.isSwooshSoundActive = false
    }
  }
  
  func createEnemy(forceBomb: ForceBomb = .random) {
    // We create the space to our enemy
    var enemy: SKSpriteNode
    
    // We get a random value between 0 to 6 expecting 0 being a bomb
    var enemyType = RandomInt(min: 0, max: 6)
    
    // Here we check our flag first...
    if forceBomb == .never {
      // If it is .never we change automatically our enemyType to 1 (not bomb)
      enemyType = 1
    } else if forceBomb == .always {
      // If it is .always we change automatically our enemyType to 0 (a bomb)
      enemyType = 0
    }
    
    // Here we validate what our enemy is...
    if enemyType == 0 {
      // This will be the container for our bomb
      enemy = SKSpriteNode()
      // We need to add this container to be in front of everything on our scene
      enemy.zPosition = 1
      // We add our id to our bomb container
      enemy.name = "bombContainer"
      
      // We create our bomb
      let bombImage = SKSpriteNode(imageNamed: "sliceBomb")
      // We add its identifier
      bombImage.name = "bomb"
      // We add our bomb to our container
      enemy.addChild(bombImage)
      
      // Verify if the bombSoundEffect is playing or is already created
      if bombSoundEffect != nil {
        // Stop the sound of the bomb
        bombSoundEffect.stop()
        // Destroy the object
        bombSoundEffect = nil
      }
      
      // Look for the sliceBomb sound on the contentFolder
      let path = Bundle.main.path(forResource: "sliceBombFuse.caf", ofType: nil)!
      // Create the url using our gotten path
      let url = URL(fileURLWithPath: path)
      // Create the sound with the gotten url
      let sound = try! AVAudioPlayer(contentsOf: url)
      // Initialize our object with the sound before
      bombSoundEffect = sound
      // Play the sound
      sound.play()
      
      // Create the emitter fuse of our bomb
      let emitter = SKEmitterNode(fileNamed: "sliceFuse")!
      // Add the position, this is the position inside the container
      emitter.position = CGPoint(x: 76, y: 64)
      // Add the emitter inside our bomb container
      enemy.addChild(emitter)
      
      
    } else {
      // We initialize our enemy with a penguin
      enemy = SKSpriteNode(imageNamed: "penguin")
      // Then we run a sound of launch for our enemy
      run(SKAction.playSoundFileNamed("launch.caf", waitForCompletion: false))
      // We add the identifier to our enemy
      enemy.name = "enemy"
    }
    
    // Get a random position off the bottom edge of the screen
    let randomPosition = CGPoint(x: RandomInt(min: 64, max: 960), y: -128)
    // Add this random position to our enemy
    enemy.position = randomPosition
    
    // Create a random angular velocity, to know how fast something should spin
    let randomAngularVelocity = CGFloat(RandomInt(min: -6, max: 6)) / 2.0
    
    // Create a random X velocity (how far to move horizontally)
    var randomXVelocity = 0
    // We check first the position of our enemy
    if randomPosition.x < 256 {
      randomXVelocity = RandomInt(min: 8, max: 15)
    } else if randomPosition.x < 512 {
      randomXVelocity = RandomInt(min: 3, max: 5)
    } else if randomPosition.x < 768 {
      randomXVelocity = -RandomInt(min: 3, max: 5)
    } else {
      randomXVelocity = -RandomInt(min: 8, max: 15)
    }
    
    // Create a random Y velocity just to make things fly at different speeds
    let randomYVelocity = RandomInt(min: 24, max: 32)
    
    // We add a circular physics body to our enemy where the collisionBitMask is set to 0 so they don't collide
    enemy.physicsBody = SKPhysicsBody(circleOfRadius: 64)
    // We add the speed velocity to our enemy with the values before
    enemy.physicsBody?.velocity = CGVector(dx: randomXVelocity * 40, dy: randomYVelocity * 40)
    // We add the angular velocity to make it spin while is dropped
    enemy.physicsBody?.angularVelocity = randomAngularVelocity
    // We add the collisionBitMask to 0 to avoid them to collide between them.
    enemy.physicsBody?.collisionBitMask = 0
    
    // We add our enemy to the scene
    addChild(enemy)
    // We add the enemy to our active enemies array in scene
    activeEnemies.append(enemy)
  }
  
  func tossEnemies() {
    // Verifiy if the game has ended
    if gameEnded {
      // If the game ended... just go out from the method
      return
    }
    
    // The popupTime will be decreasing each time
    popupTime *= 0.991
    // The delay is gonna be decreasing too
    chainDelay *= 0.99
    // The speed on the world is gonna be increasing
    physicsWorld.speed *= 1.02
    
    // We get the sequence on our game
    let sequenceType = sequence[sequencePosition]
    // Then we check for the current sequence on it
    switch sequenceType {
    case .oneNoBomb:
      // If is one without bomb... this will create a new enemy
      createEnemy(forceBomb: .never)
    case .one:
      // If it is one... this will aleatory create something enemy or bomb
      createEnemy()
    case .twoWithOneBomb:
      // If it is two with one bomb... you will see two nodes, at least one bomb
      createEnemy(forceBomb: .never)
      createEnemy(forceBomb: .always)
    case .two:
      // If it is just two... could be enemies or bombs or alternate
      createEnemy()
      createEnemy()
    case .three:
      // Same as two... but three
      createEnemy()
      createEnemy()
      createEnemy()
    case .four:
      // Same as two and three but four
      createEnemy()
      createEnemy()
      createEnemy()
      createEnemy()
    case .chain:
      // This is a chain... we will create an enemy and with a delay we will creating four more enemies
      createEnemy()
      
      DispatchQueue.main.asyncAfter(deadline: .now() + (chainDelay / 5.0)) { [unowned self] in
        self.createEnemy()
      }
      
      DispatchQueue.main.asyncAfter(deadline: .now() + (chainDelay / 5.0 * 2.0)) { [unowned self] in
        self.createEnemy()
      }
      
      DispatchQueue.main.asyncAfter(deadline: .now() + (chainDelay / 5.0 * 3.0)) { [unowned self] in
        self.createEnemy()
      }
      
      DispatchQueue.main.asyncAfter(deadline: .now() + (chainDelay / 5.0 * 4.0)) { [unowned self] in
        self.createEnemy()
      }
    case .fastChain:
      // This is a chain... we will create an enemy and with a delay we will creating four more enemies but faster
      createEnemy()
      
      DispatchQueue.main.asyncAfter(deadline: .now() + (chainDelay / 10.0)) { [unowned self] in
        self.createEnemy()
      }
      
      DispatchQueue.main.asyncAfter(deadline: .now() + (chainDelay / 10.0 * 2.0)) { [unowned self] in
        self.createEnemy()
      }
      
      DispatchQueue.main.asyncAfter(deadline: .now() + (chainDelay / 10.0 * 3.0)) { [unowned self] in
        self.createEnemy()
      }
      
      DispatchQueue.main.asyncAfter(deadline: .now() + (chainDelay / 10.0 * 4.0)) { [unowned self] in
        self.createEnemy()
      }
    }
    
    // Then we increase the sequence position
    sequencePosition += 1
    // This is to be able to create more enemies
    nextSequenceQueued = false
  }
  
  func substractLife() {
    // Remove one life
    lives -= 1
    // Play a sound when you lost an opportunity
    run(SKAction.playSoundFileNamed("wrong.caf", waitForCompletion: false))
    
    // Create the property to save a life
    var life: SKSpriteNode
    // Verify how many lives do you still have
    if lives == 2 {
      life = livesImages[0]
    } else if lives == 1 {
      life = livesImages[1]
    } else {
      life = livesImages[2]
      // If you removed all your lives - the game ends
      endGame(triggeredByBomb: false)
    }
    
    // Add a texture of life's gone
    life.texture = SKTexture(imageNamed: "sliceLifeGone")
    
    // Change the scale in 1.3 x and y
    life.xScale = 1.3
    life.yScale = 1.3
    // Run the scale on the life
    life.run(SKAction.scale(to: 1, duration: 0.1))
  }
  
  func endGame(triggeredByBomb: Bool) {
    // Verifiy if the game has ended
    if gameEnded {
      // If the game ended... just go out from the method
      return
    }
    
    // Change the gameEnded flag to true
    gameEnded = true
    // Turn of the speed of the physicsWorld
    physicsWorld.speed = 0
    // Disable the user interaction on screen
    isUserInteractionEnabled = false
    
    // Verify if the bombSoundEffect is playing or is already created
    if bombSoundEffect != nil {
      // Stop the sound of the bomb
      bombSoundEffect.stop()
      // Destroy the object
      bombSoundEffect = nil
    }
    
    // If you died by a bomb you are gonna add texture on lifes as you lose
    if triggeredByBomb {
      livesImages[0].texture = SKTexture(imageNamed: "sliceLifeGone")
      livesImages[1].texture = SKTexture(imageNamed: "sliceLifeGone")
      livesImages[2].texture = SKTexture(imageNamed: "sliceLifeGone")
    }
  }
  
  // MARK: - Update Scene
  
  override func update(_ currentTime: TimeInterval) {
    // Here we check for active enemies at first... it must be more than one
    if activeEnemies.count > 0 {
      // We are gonna iterate through each node enemy
      for node in activeEnemies {
        // Then we verify if it is out of our scene
        if node.position.y < -140 {
          // Remove all the actions active in node
          node.removeAllActions()
          
          // Verify if the node is an enemy or a bomb
          if node.name == "enemy" {
            // Remove the name (id) to avoid multiples slices on same node
            node.name = ""
            // Remove a life
            substractLife()
            
            // It it is, we will remove it from the scene
            node.removeFromParent()
            
            // We get the index of the enemy that we delete from scene
            if let index = activeEnemies.index(of: node) {
              // And we delete it from our array of active enemies
              activeEnemies.remove(at: index)
            }
          } else if node.name == "bombContainer" {
            // Remove the name (id) to avoid multiples slices on same node
            node.name = ""
            
            // It it is, we will remove it from the scene
            node.removeFromParent()
            
            // We get the index of the enemy that we delete from scene
            if let index = activeEnemies.index(of: node) {
              // And we delete it from our array of active enemies
              activeEnemies.remove(at: index)
            }
          }
        }
      }
    } else {
      // If the next sequence is not queued we are able to create a new one
      if !nextSequenceQueued {
        // We do it with a little delay
        DispatchQueue.main.asyncAfter(deadline: .now() + popupTime) { [unowned self] in
          self.tossEnemies()
        }
        
        // And we add our property to true
        nextSequenceQueued = true
      }
    }
    
    // This is gonna be the counter of bombs on scene
    var bombCount = 0
    // Look for each node on activeEnemies array
    for node in activeEnemies {
      // Check that our node is a bombContainer
      if node.name == "bombContainer" {
        // If it is... increase the counter
        bombCount += 1
        break
      }
    }
    
    // Verify if we don't have bombs on scene
    if bombCount == 0 {
      // No bombs - Stop the fuse sound!
      if bombSoundEffect != nil {
        bombSoundEffect.stop()
        bombSoundEffect = nil
      }
    }
  }
  
  // MARK: - Touches Handler
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    // We remove the activeSlicePoints on our array because this is a fresh slice
    activeSlicePoints.removeAll(keepingCapacity: true)
    
    // We get and verify for the first user touch
    if let touch = touches.first {
      // We get the location of the touch
      let location = touch.location(in: self)
      // We add the current touch to our activeSlicePoints array
      activeSlicePoints.append(location)
      // Call the method to draw the slice
      redrawActiveSlice()
      
      // We remove the current actions on our shapes (think... they could be in the middle of a fadeOut)
      activeSliceFG.removeAllActions()
      // We remove the current actions on our shapes (think... they could be in the middle of a fadeOut)
      activeSliceBG.removeAllActions()
      
      // Set foreground slice to have 1 on alpha to be fully visible
      activeSliceFG.alpha = 1
      // Set background slice to have 1 on alpha to be fully visible
      activeSliceBG.alpha = 1
    }
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    // Verifiy if the game has ended
    if gameEnded {
      // If the game ended... just go out from the method
      return
    }
    
    // We get and verify for the first user touch
    guard let touch = touches.first else { return }
    // We get the location of the touch
    let location = touch.location(in: self)
    // We add the current touch to our activeSlicePoints array
    activeSlicePoints.append(location)
    // Call the method to draw the slice
    redrawActiveSlice()
    
    // We verify if at the time we moved the sound is not active
    if !isSwooshSoundActive {
      // Otherwise we call a slice sound
      playSwooshSound()
    }
    
    // Take all the nodes in a location
    let nodesAtPoint = nodes(at: location)
    // Loop through each node to see which it is
    for node in nodesAtPoint {
      // Check for an enemy node (penguin)
      if node.name == "enemy" {
        // Create the particles for the sliceHit
        let emitter = SKEmitterNode(fileNamed: "sliceHitEnemy")!
        // Give the position you want to show the particles
        emitter.position = node.position
        // Add the emitter particles on scene
        addChild(emitter)
        
        // Remove the name (id) to avoid slice over it several times
        node.name = ""
        
        // Disable the isDynamic property of physics body so that is doesn't carry on falling
        node.physicsBody?.isDynamic = false
        
        // Create the scale you want your penguin
        let scaleOut = SKAction.scale(to: 0.001, duration: 0.2)
        // Create the fadeOut you want on your penguin
        let fadeOut = SKAction.fadeOut(withDuration: 0.2)
        // Group both actions you want at same time
        let group = SKAction.group([scaleOut, fadeOut])
        
        // Create the sequence you want to execute on the node
        let seq = SKAction.sequence([group, SKAction.removeFromParent()])
        // Add this sequence to our node (penguin)
        node.run(seq)
        
        // Increase the score
        score += 1
        
        // Get the index of the current node on your activeEnemies array
        let index = activeEnemies.index(of: node as! SKSpriteNode)!
        // Remove the node from the activeEnemies array
        activeEnemies.remove(at: index)
        
        // Run an action - The sound you want when we destroy our penguin
        run(SKAction.playSoundFileNamed("whack.caf", waitForCompletion: false))
      } else if node.name == "bomb" {
        // Otherwise the node is a bomb
        
        // Create the particles for the sliceHit on bomb
        let emitter = SKEmitterNode(fileNamed: "sliceHitBomb")!
        // Give the position you want to show the particles (we take the container position)
        emitter.position = node.parent!.position
        // Add the emitter particles on scene
        addChild(emitter)
        
        // Remove the name (id) to avoid slice over it several times
        node.name = ""
        
        // Disable the isDynamic property of physics body so that is doesn't carry on falling
        node.parent?.physicsBody?.isDynamic = false
        
        // Create the scale you want your penguin
        let scaleOut = SKAction.scale(to: 0.001, duration: 0.2)
        // Create the fadeOut you want on your penguin
        let fadeOut = SKAction.fadeOut(withDuration: 0.2)
        // Group both actions you want at same time
        let group = SKAction.group([scaleOut, fadeOut])
        
        // Create the sequence you want to execute on the node
        let seq = SKAction.sequence([group, SKAction.removeFromParent()])
        // Add this sequence to our node (penguin)
        node.parent?.run(seq)
        
        // Get the index of the current node on your activeEnemies array
        let index = activeEnemies.index(of: node.parent as! SKSpriteNode)!
        // Remove the node from the activeEnemies array
        activeEnemies.remove(at: index)
        
        // Run an action - The sound you want when we destroy our penguin
        run(SKAction.playSoundFileNamed("explosion.caf", waitForCompletion: false))
        
        // The game finish when you touch a bomb
        endGame(triggeredByBomb: true)
      }
    }
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    // After finish our touch the slice will be fading out with a duration of 0.25
    activeSliceBG.run(SKAction.fadeOut(withDuration: 0.25))
    // After finish our touch the slice will be fading out with a duration of 0.25
    activeSliceFG.run(SKAction.fadeOut(withDuration: 0.25))
  }
  
  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    // We call the touches Ended to DRY (Don't repeat yourself)
    touchesEnded(touches, with: event)
  }
}
