//
//  MainVC.swift
//  whitehouse-petitions
//
//  Created by Juan Francisco Dorado Torres on 6/18/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

// MARK: -  GCD Clousures

/*class MainVC: UITableViewController {
  
  // MARK: - Properties
  
  var petitions = [[String : String]]()

  // MARK: - Initialization
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let urlString: String
    
    if navigationController?.tabBarItem.tag == 0 {
      urlString = "https://api.whitehouse.gov/v1/petitions.json?limit=100"
    } else {
      urlString = "https://api.whitehouse.gov/v1/petitions.json?signatureCountFloor=10000&limit=100"
    }
    
    
    DispatchQueue.global(qos: .userInitiated).async { [unowned self] in
      if let url = URL(string: urlString) {
        if let data = try? String(contentsOf: url) {
          let json = JSON(parseJSON: data)
          if json["metadata"]["responseInfo"]["status"].intValue == 200 {
            self.parse(json: json)
            return
          }
        }
      }
    }
    
    showError()
  }
  
  // MARK: - Functions
  
  func parse(json: JSON) {
    for result in json["results"].arrayValue {
      let title = result["title"].stringValue
      let body = result["body"].stringValue
      let sigs = result["signature-Count"].stringValue
      let obj = ["title" : title, "body" : body, "sigs" : sigs]
      petitions.append(obj)
    }
    
    DispatchQueue.main.async { [unowned self] in
      self.tableView.reloadData()
    }
  }
  
  func showError() {
    DispatchQueue.main.async { [unowned self] in
      let alert = UIAlertController(title: "Loading error", message: "There was a problem loading the feed; please check your connection and try again.", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Ok", style: .default))
      self.present(alert, animated: true)
    }
  }
  
  // MARK: - UITableView Delegate|Datasource
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return petitions.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    let petition = petitions[indexPath.row]
    cell.textLabel?.text = petition["title"]
    cell.detailTextLabel?.text = petition["body"]
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let vc = DetailVC()
    vc.detailItem = petitions[indexPath.row]
    navigationController?.pushViewController(vc, animated: true)
  }
}*/

// MARK: -  GCD Selectors

class MainVC: UITableViewController {
  
  // MARK: - Properties
  
  var petitions = [[String : String]]()
  
  // MARK: - Initialization
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    performSelector(inBackground: #selector(fetchJSON), with: nil)
  }
  
  // MARK: - Functions
  
  @objc func fetchJSON() {
    let urlString: String
    
    if navigationController?.tabBarItem.tag == 0 {
      urlString = "https://api.whitehouse.gov/v1/petitions.json?limit=100"
    } else {
      urlString = "https://api.whitehouse.gov/v1/petitions.json?signatureCountFloor=10000&limit=100"
    }
    
    
    DispatchQueue.global(qos: .userInitiated).async { [unowned self] in
      if let url = URL(string: urlString) {
        if let data = try? String(contentsOf: url) {
          let json = JSON(parseJSON: data)
          if json["metadata"]["responseInfo"]["status"].intValue == 200 {
            self.parse(json: json)
            return
          }
        }
      }
    }
    
    performSelector(onMainThread: #selector(showError), with: nil, waitUntilDone: false)
  }
  
  func parse(json: JSON) {
    for result in json["results"].arrayValue {
      let title = result["title"].stringValue
      let body = result["body"].stringValue
      let sigs = result["signature-Count"].stringValue
      let obj = ["title" : title, "body" : body, "sigs" : sigs]
      petitions.append(obj)
    }
    
    DispatchQueue.main.async { [unowned self] in
      self.tableView.reloadData()
    }
  }
  
  @objc func showError() {
    DispatchQueue.main.async { [unowned self] in
      let alert = UIAlertController(title: "Loading error", message: "There was a problem loading the feed; please check your connection and try again.", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Ok", style: .default))
      self.present(alert, animated: true)
    }
  }
  
  // MARK: - UITableView Delegate|Datasource
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return petitions.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    let petition = petitions[indexPath.row]
    cell.textLabel?.text = petition["title"]
    cell.detailTextLabel?.text = petition["body"]
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let vc = DetailVC()
    vc.detailItem = petitions[indexPath.row]
    navigationController?.pushViewController(vc, animated: true)
  }
}
