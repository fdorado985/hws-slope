#  Whitehouse Petitions

## Description

This app will make an `API` call, at the same time here we work with  `JSON` and is a basic task that you are gonna be using time and time again in your Swift career.

In case you want to take this project and extend it some more... take a look to the documentation at the original API [white house](https://petitions.whitehouse.gov/developers)

As you can see this is a copy of the original Whitehouse-Petitions project before, the difference is that here we are using GCD to handle the api call on background

The `MainVC` has a bunch of code commented that handle the GCD using clousures and the other one using the `#selector()`, so feel free to see how both works, the uncommented `#selector()` way is easier to follow than the `clousures`.

## Demo
![whitehouse_petitions_demo](screenshots/whitehouse_petitions_demo.gif)

