#  Names To Faces
## Description
This app could be good to you to help you to remember the names of people.

## Wrap Up
Here you will see:
- `UICollectionViewController`
- `UIImagePickerViewController`
- `UINavigationControllerDelegate`
- `UIAlertController`
- `Documents Directory`
- `UUID`

## Demo
![names_to_faces_demo](screenshots/names_to_faces_demo.gif)

