//
//  Person.swift
//  names-to-faces
//
//  Created by Juan Francisco Dorado Torres on 6/20/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class Person: NSObject {
  
  var name: String
  var image: String
  
  init(name: String, image: String) {
    self.name = name
    self.image = image
  }

}
