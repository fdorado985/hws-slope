//
//  DetailVC.swift
//  whitehouse-petitions
//
//  Created by Juan Francisco Dorado Torres on 6/18/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import WebKit

class DetailVC: UIViewController {
  
  // MARK: - Properties
  
  var webView: WKWebView!
  var detailItem: [String : String]!
  
  // MARK: - Initialization
  
  override func loadView() {
    webView = WKWebView()
    view = webView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    guard detailItem != nil else { return }
    if let body = detailItem["body"] {
      var html = "<html>"
      html += "<head>"
      html += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
      html += "<style> body { font-size: 150%; } </style>"
      html += "</head>"
      html += "<body>"
      html += body
      html += "</body>"
      html += "</html>"
      webView.loadHTMLString(html, baseURL: nil)
    }
  }
}
