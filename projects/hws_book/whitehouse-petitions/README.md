#  Whitehouse Petitions

## Description

This app will make an `API` call, at the same time here we work with  `JSON` and is a basic task that you are gonna be using time and time again in your Swift career.

In case you want to take this project and extend it some more... take a look to the documentation at the original API [white house](https://petitions.whitehouse.gov/developers)

## Demo
![whitehouse_petitions_demo](screenshots/whitehouse_petitions_demo.gif)

