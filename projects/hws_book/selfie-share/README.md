#  Selfie Share

### Description
Multipeer photo sharing app in just 150 lines of codes

## Wrap Up
On this project you will see about...
- `CollectionView`
- `ImagePicker`
- `GCD`
- `Multipeer Connectivity Framework`

## Demo
**Note that the additional alerts are coming from the Macbook**
![selfie_share_demo](screenshots/selfie_share_demo.gif)
