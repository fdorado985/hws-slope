#  Swift Searcher
### Description
Add your app's content to iOS Spotlight and take advantage of Safari integration.

## Wrap Up
On this project you will see...
- `SFSafariViewController`
- `Core Spotlight`
- `UITableViewCell automatic sizing`

## Demo
![swift_searcher_demo](screenshots/swift_searcher_demo.gif)
