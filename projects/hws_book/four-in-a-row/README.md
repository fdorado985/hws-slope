#  Four In A Row

### Description
Let iOS take over the AI in your games using GameplayKit AI.

## Wrap Up
On this project you will see...
- `GameplayKit`
- `IA`

## Demo
![four_in_a_row_demo](screenshots/four_in_a_row_demo.gif)
