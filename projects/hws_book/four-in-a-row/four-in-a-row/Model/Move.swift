//
//  Move.swift
//  four-in-a-row
//
//  Created by Juan Francisco Dorado Torres on 7/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import GameplayKit

class Move: NSObject, GKGameModelUpdate {
    
    // MARK: - Properties
    
    var value: Int = 0
    var column: Int
    
    // MARK: - View init
    
    init(column: Int) {
        self.column = column
    }
}
