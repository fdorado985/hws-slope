//: Playground - noun: a place where people can play

import UIKit
import GameplayKit

// Common random numbers
// this generates numbers between 0 - 4,294,967,295 giving a range of 2 to the power of 32 - 1
print(arc4random())
print(arc4random())
print(arc4random())
print(arc4random())


// Common ways to create random numbers
// 1. Widely used but problematic way of generating numbers in a range
print(arc4random() % 6)

// 2. Proper way to generate random numbers in a range
print(arc4random_uniform(6))


// Now... what if you need numbers between a range for example... 10 and 20, 100 and 500?
func RandomInt(min: Int, max: Int) -> Int {
    if max < min { return min }
    return Int(arc4random_uniform(UInt32((max - min) + 1))) + min
}

print(RandomInt(min: 5, max: 15))
print(RandomInt(min: 5, max: 15))



// ==== THE BEST WAY IS USING GAMEPLAYKIT

// The most common way with gameplaykit is using sharedRandom()
// Using nextInt you produce a truly random number

print(GKRandomSource.sharedRandom().nextInt())

// Now nextInt comes with a warning that could be not guaranteed to random for a specific situations.
// As alternative, try using nextInt(upperBound:) method

print(GKRandomSource.sharedRandom().nextInt(upperBound: 6))

// Now let's see three different amazing options of random numbers

// - GKLinearCongruentialRandomSource: has high performance but the lowest randomness
// - GKMersenneTwisterRandomSource: has high randomness but the lowest performance
// - GKARC4RandomSource: has good performance and good randomness - in the words of Apple, "It's going to be your Goldilocks random source"

// Now for example if you are gonna do a random number between 0 - 19 using an ARC4 random source that you can save to disk

let arc4 = GKARC4RandomSource()
arc4.nextInt(upperBound: 20)

// If you really want the maximum possible randomness for your app or game, try the Mersenne Twister source instead.

let mersenne = GKMersenneTwisterRandomSource()
mersenne.nextInt(upperBound: 20)

// TAKE IN COUNT THAT THIS NUMBERS ARE NOT RECOMMENDED FOR CRYPTOGRAPHY

// Example for six-sided dice.

let d6 = GKRandomDistribution.d6()
d6.nextInt()

// Now.. do you want a 20-sided dice

let d20 = GKRandomDistribution.d20()
d20.nextInt()

// Oh maybe you want a 11,539-sided dice...

let crazy = GKRandomDistribution(lowestValue: 1, highestValue: 11539)
crazy.nextInt()

// But not everything is gold!, you need to be careful... this a possible way to crash if you specify highest|lowest values and also intend to use nextInt(upperBound:)

//let distribution = GKRandomDistribution(lowestValue: 10, highestValue: 20)
//print(distribution.nextInt(upperBound: 9))


// If you want one particular randomSource, there are special constructors for you

let rand = GKMersenneTwisterRandomSource()
let distribution = GKRandomDistribution(randomSource: rand, lowestValue: 10, highestValue: 20)
print(distribution.nextInt())

// Shuffling an array with GameplayKit: arrayByShufflingObjects(in:)

extension Array {
    
    mutating func shuffle() {
        for i in 0 ..< (count - 1) {
            let j = Int(arc4random_uniform(UInt32(count - i))) + 1
            swapAt(i, j)
        }
    }
}

// Using GamePlayKit

let lotteryBalls = [Int](0...49)
let shuffledBalls = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: lotteryBalls)
print(shuffledBalls[0])
print(shuffledBalls[1])
print(shuffledBalls[2])
print(shuffledBalls[3])
print(shuffledBalls[4])

// Using a seed to predictable values

let fixedLotteryBalls = [Int](0...49)
let fixedShuffledBalls = GKMersenneTwisterRandomSource(seed: 1001).arrayByShufflingObjects(in: fixedLotteryBalls)
print(fixedShuffledBalls[0])
print(fixedShuffledBalls[1])
print(fixedShuffledBalls[2])
print(fixedShuffledBalls[3])
print(fixedShuffledBalls[4])
