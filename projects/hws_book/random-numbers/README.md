# Random Numbers
### Description
This is just a playground where you will see some of different techniques using GameplayKit for create random items.

## Wrap Up
You will see on this playground...
- `GameplayKit`
- `arc4random`
- `arc4random_uniform`
- `GKRandomSource`
- `sharedRandom`
- `nextInt`
- `GKARC4RandomSource`
- `GKMersenneTwisterRandomSource`
- `GKRandomDistribution`
- `GKRandomDistribution d6 | d20`
- `GKRandomDistribution(lowestValue:, highestValue)`
- `GKRandomDistribution(randomSource:, lowestValue:, highestValue:)`
- `arrayByShufflingObjects(in:)`
- `GKMersenneTwisterRandomSource(seed:)`
