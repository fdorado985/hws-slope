//
//  GameScene.swift
//  fireworks-night
//
//  Created by Juan Francisco Dorado Torres on 6/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
  
  // MARK: - Properties
  
  var gameTimer: Timer!
  var fireworks = [SKNode]()
  
  let leftEdge = -22
  let bottomEdge = -22
  let rightEdge = 1024 + 22
  
  var score = 0 {
    didSet {
      // The score code here
    }
  }
  
  // MARK: - View Cycle
  
  override func didMove(to view: SKView) {
    // Add the background
    let background = SKSpriteNode(imageNamed: "background")
    background.position = CGPoint(x: 512, y: 384)
    background.blendMode = .replace
    background.zPosition = -1
    addChild(background)
    
    // Set the timer to shoot a firework
    gameTimer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(launchFireworks), userInfo: nil, repeats: true)
  }
  
  // MARK: - Touches Handler
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    checkTouches(touches)
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesMoved(touches, with: event)
    checkTouches(touches)
  }
  
  // MARK: - Update Frames
  
  override func update(_ currentTime: TimeInterval) {
    for (index, firework) in fireworks.enumerated().reversed() {
      if firework.position.y > 900 {
        // This uses a position high above so that rockets can explode off screen
        fireworks.remove(at: index)
        firework.removeFromParent()
      }
    }
  }
  
  // MARK: - Functions
  
  @objc func launchFireworks() {
    let movementAmount: CGFloat = 1800
    
    switch GKRandomSource.sharedRandom().nextInt(upperBound: 4) {
    case 0:
      // Fire five, straight up
      createFirework(xMovement: 0, x: 512, y: bottomEdge)
      createFirework(xMovement: 0, x: 512 - 200, y: bottomEdge)
      createFirework(xMovement: 0, x: 512 - 100, y: bottomEdge)
      createFirework(xMovement: 0, x: 512 + 100, y: bottomEdge)
      createFirework(xMovement: 0, x: 512 + 200, y: bottomEdge)
    case 1:
      // Fire five, in a fan
      createFirework(xMovement: 0, x: 512, y: bottomEdge)
      createFirework(xMovement: -200, x: 512 - 200, y: bottomEdge)
      createFirework(xMovement: -100, x: 512 - 100, y: bottomEdge)
      createFirework(xMovement: 100, x: 512 + 100, y: bottomEdge)
      createFirework(xMovement: 200, x: 512 + 200, y: bottomEdge)
    case 2:
      // Fire five, from the left to the right
      createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge + 400)
      createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge + 300)
      createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge + 200)
      createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge + 100)
      createFirework(xMovement: movementAmount, x: leftEdge, y: bottomEdge)
    case 3:
      // Fire five, from the right to the left
      createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge + 400)
      createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge + 300)
      createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge + 200)
      createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge + 100)
      createFirework(xMovement: -movementAmount, x: rightEdge, y: bottomEdge)
    default:
      break
    }
  }
  
  func createFirework(xMovement: CGFloat, x: Int, y: Int) {
    let node = SKNode()
    node.position = CGPoint(x: x, y: y)
    
    let firework = SKSpriteNode(imageNamed: "rocket")
    firework.colorBlendFactor = 1
    firework.name = "firework"
    node.addChild(firework)
    
    switch GKRandomSource.sharedRandom().nextInt(upperBound: 3) {
    case 0:
      firework.color = .cyan
    case 1:
      firework.color = .green
    case 2:
      firework.color = .red
    default:
      break
    }
    
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 0, y: 0))
    path.addLine(to: CGPoint(x: xMovement, y: 1000))
    
    let move = SKAction.follow(path.cgPath, asOffset: true, orientToPath: true, speed: 200)
    node.run(move)
    
    let emitter = SKEmitterNode(fileNamed: "fuse")!
    emitter.position = CGPoint(x: 0, y: -22)
    node.addChild(emitter)
    
    fireworks.append(node)
    addChild(node)
  }
  
  func checkTouches(_ touches: Set<UITouch>) {
    //  We get the first touch
    guard let touch = touches.first else { return }
    
    // Get the location through the touch
    let location = touch.location(in: self)
    // Get all nodes on that location
    let nodesAtPoint = nodes(at: location)
    
    // Loop through each node
    for node in nodesAtPoint {
      // Check for a SKSpriteNode
      if node is SKSpriteNode {
        // Cast the node and store on sprite property
        let sprite = node as! SKSpriteNode
        
        // Verify if it is a firework
        if sprite.name == "firework" {
          // Loop through fireworks array
          for parent in fireworks {
            // Get the children of the container parent
            let firework = parent.children[0] as! SKSpriteNode
            
            // Verify if name is selected and firework is different to the sprite color
            if firework.name == "selected" && firework.color != sprite.color {
              // Change the id to farework again
              firework.name = "firework"
              // Blend the color to 1, returning the color blending
              firework.colorBlendFactor = 1
            }
          }
          
          // Change the id to selected
          sprite.name = "selected"
          // Blend the color to 0, disable the color
          sprite.colorBlendFactor = 0
        }
      }
    }
  }
  
  func explode(firework: SKNode) {
    let emitter = SKEmitterNode(fileNamed: "explode")!
    emitter.position = firework.position
    addChild(emitter)
    
    firework.removeFromParent()
  }
  
  // This function is called through the motion (shake iPad)
  // Find this on GameViewController.swift
  func explodeFireworks() {
    var numExploded = 0
    
    for (index, fireworkContainer) in fireworks.enumerated().reversed() {
      let firework = fireworkContainer.children[0] as! SKSpriteNode
      
      if firework.name == "selected" {
        // destroy this firework!
        explode(firework: fireworkContainer)
        fireworks.remove(at: index)
        numExploded += 1
      }
    }
    
    switch numExploded {
    case 0:
      // nothing - rubbish!
      break
    case 1:
      score += 200
    case 2:
      score += 500
    case 3:
      score += 1500
    case 4:
      score += 2500
    default:
      score += 4000
    }
  }
}
