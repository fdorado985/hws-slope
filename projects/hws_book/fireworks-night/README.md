#  Fireworks Night

### Description
Learn about timers and color blends while making things go bang!

## Wrap Up
You will see on this project...
- `Timer`
- `Motion`
- `SKSpriteNode`
- `EmitterNode`
- `Touches`

## Demo
![fireworks_night_demo](screenshots/fireworks_night_demo.gif)
