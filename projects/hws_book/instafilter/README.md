#  Instafilter

## Description
This app add filters on images from your photo library, this let you modify the intensity with a slider.

## Wrap Up
You will see...
- `CoreImage`
- `CIContext`
- `CIFilter`
- `UIImagePickerViewController`
- `UISlider`

## Demo
![instafilter_demo](screenshots/instafilter_demo.gif)
