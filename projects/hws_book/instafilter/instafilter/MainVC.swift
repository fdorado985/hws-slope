//
//  MainVC.swift
//  instafilter
//
//  Created by Juan Francisco Dorado Torres on 6/22/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import CoreImage

class MainVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  // MARK: - Properties
  
  var currentImage: UIImage!
  var context: CIContext!
  var currentFilter: CIFilter!
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var intensity: UISlider!
  @IBOutlet weak var btnSave: UIButton!
  
  // MARK: - View Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Instafilter"
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(importPicture))
    
    context = CIContext()
    currentFilter = CIFilter(name: "CISepiaTone")
  }
  
  // MARK: - IBActions
  
  @IBAction func intensityChanged(_ sender: UISlider) {
    applyProcessing()
  }
  
  @IBAction func changeFilter(_ sender: UIButton) {
    let alert = UIAlertController(title: "Choose filter", message: nil, preferredStyle: .actionSheet)
    
    alert.addAction(UIAlertAction(title: "CIBumpDistortion", style: .default, handler: setFilter))
    alert.addAction(UIAlertAction(title: "CIGaussianBlur", style: .default, handler: setFilter))
    alert.addAction(UIAlertAction(title: "CIPixellate", style: .default, handler: setFilter))
    alert.addAction(UIAlertAction(title: "CISepiaTone", style: .default, handler: setFilter))
    alert.addAction(UIAlertAction(title: "CITwirlDistortion", style: .default, handler: setFilter))
    alert.addAction(UIAlertAction(title: "CIUnsharpMask", style: .default, handler: setFilter))
    alert.addAction(UIAlertAction(title: "CIVignette", style: .default, handler: setFilter))
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
    
    present(alert, animated: true)
  }
  
  @IBAction func save(_ sender: UIButton) {
    guard let image = imageView.image else {
      let alert = UIAlertController(title: "Save Error", message: "You need to pick an image first before save", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Ok", style: .default))
      present(alert, animated: true)
      return
      
    }
    UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
  }
  
  // MARK: - Functions
  
  @objc func importPicture() {
    let picker = UIImagePickerController()
    picker.allowsEditing = true
    picker.delegate = self
    present(picker, animated: true)
  }
  
  func applyProcessing() {
    let inputKeys = currentFilter.inputKeys
    if inputKeys.contains(kCIInputIntensityKey) {
      currentFilter.setValue(intensity.value, forKey: kCIInputIntensityKey)
    }
    
    if inputKeys.contains(kCIInputRadiusKey) {
      currentFilter.setValue(intensity.value * 200, forKey: kCIInputRadiusKey)
    }
    
    
    if inputKeys.contains(kCIInputScaleKey) {
      currentFilter.setValue(intensity.value * 10, forKey: kCIInputScaleKey)
    }
    
    
    if inputKeys.contains(kCIInputCenterKey) {
      currentFilter.setValue(CIVector(x: currentImage.size.width / 2, y: currentImage.size.height / 2), forKey: kCIInputCenterKey)
    }
    
    if let cgimg = context.createCGImage(currentFilter.outputImage!, from: currentFilter.outputImage!.extent) {
      let processedImage = UIImage(cgImage: cgimg)
      imageView.image = processedImage
    }
  }
  
  @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
    let alert: UIAlertController
    
    if let error = error {
      // We got back an error!
      alert = UIAlertController(title: "Save Error", message: error.localizedDescription, preferredStyle: .alert)
    } else {
      alert = UIAlertController(title: "Saved!", message: "You altered image has been saved to your photos.", preferredStyle: .alert)
    }
    
    alert.addAction(UIAlertAction(title: "Ok", style: .default))
    present(alert, animated: true)
  }
  
  func setFilter(action: UIAlertAction) {
    // Make sure we have a valid image before continuing!
    guard currentImage != nil else { return }
    
    currentFilter = CIFilter(name: action.title!)
    let beginImage = CIImage(image: currentImage)
    currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
    
    applyProcessing()
  }
  
  // MARK: - UIImagePickerController Delegate
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else { return }
    dismiss(animated: true)
    currentImage = image
    
    let beginImage = CIImage(image: currentImage)
    currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
    applyProcessing()
  }
}

