#  Animation

### Description
This app is easy, just we are gonna be using `Core Animation`, we'll see how to transform an image, make it rotate, much more...

## Wrap Up
Here you will see about:
- `switch/case`
- `Core Animation`
- `CGAffineTransform`
- `CGAffineTransform(scaleX:, y:)`
- `CGAffineTransform.identity`
- `CGAffineTransform(translationX:, y:)`
- `CGAffineTransform(rotationAngle:)`

## Demo
![animation_demo](screenshots/animation_demo.gif)
