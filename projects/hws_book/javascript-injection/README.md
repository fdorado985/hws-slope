#  Javascript Injection

The Javascript Injection extens Safari with a cool feature for Javascript Developers
This project builds the bridge between JavaScript and Swift, and now that bridge is built you can add your own Swift functionality on top.

## Demo
![javascript_injection_demo](screenshots/javascript_injection_demo.gif)

![javascript_injection_alert_demo](screenshots/javascript_injection_alert_demo.gif)
