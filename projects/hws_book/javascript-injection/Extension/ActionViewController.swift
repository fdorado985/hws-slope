//
//  ActionViewController.swift
//  Extension
//
//  Created by Juan Francisco Dorado Torres on 6/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MobileCoreServices

class ActionViewController: UIViewController {
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var txtScript: UITextView!
  
  // MARK: - Properties
  
  var pageTitle = ""
  var pageURL = ""
  
  // MARK: - View Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
    
    let notificationCenter = NotificationCenter.default
    notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
    notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    
    if let inputItem = extensionContext!.inputItems.first as? NSExtensionItem {
      if let itemProvider = inputItem.attachments?.first as? NSItemProvider {
        itemProvider.loadItem(forTypeIdentifier: kUTTypePropertyList as String) { [unowned self] (dict, error) in
          let itemDictionary = dict as! NSDictionary
          let javaScriptValues = itemDictionary[NSExtensionJavaScriptPreprocessingResultsKey] as! NSDictionary
          self.pageTitle = javaScriptValues["title"] as! String
          self.pageURL = javaScriptValues["URL"] as! String
          
          DispatchQueue.main.async {
            self.title = self.pageTitle
          }
        }
      }
    }
  }
  
  // MARK: - IBActions
  
  @IBAction func done() {
    let item = NSExtensionItem()
    let argument: NSDictionary = ["customJavaScript" : txtScript.text]
    let webDictionary: NSDictionary = [NSExtensionJavaScriptFinalizeArgumentKey : argument]
    let customJavaScript = NSItemProvider(item: webDictionary, typeIdentifier: kUTTypePropertyList as String)
    item.attachments = [customJavaScript]
    
    extensionContext!.completeRequest(returningItems: [item])
  }
  
  // MARK: - Functions
  
  @objc func adjustForKeyboard(notification: Notification) {
    let userInfo = notification.userInfo!
    let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
    
    if notification.name == Notification.Name.UIKeyboardWillHide {
      txtScript.contentInset = UIEdgeInsets.zero
    } else {
      txtScript.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
    }
    
    txtScript.scrollIndicatorInsets = txtScript.contentInset
    
    let selectedRange = txtScript.selectedRange
    txtScript.scrollRangeToVisible(selectedRange)
  }
  
}
