#  Pachinko

## Description
This is an easy game that just make balls appear and go to red|green holes to score up points!

## Wrap Up
On this project you will see:
- `SpriteKitNode`
- `Collisions`
- `Particles`
- `Add|Remove Node`
- `Create Labels`
- `SKPhysicsBody`
- much more...

## Demo
![pachinko_demo](screenshots/pachinko_demo.gif)

