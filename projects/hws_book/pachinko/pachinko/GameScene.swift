//
//  GameScene.swift
//  pachinko
//
//  Created by Juan Francisco Dorado Torres on 6/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
  
  // MARK: - Properties
  
  var scoreLabel: SKLabelNode!
  var editLabel: SKLabelNode!
  
  var score = 0 {
    didSet {
      scoreLabel.text = "Score: \(score)"
    }
  }
  
  var editingMode: Bool = false {
    didSet {
      if editingMode {
        editLabel.text = "Done"
      } else {
        editLabel.text = "Edit"
      }
    }
  }
  
  // MARK: - View Cycle
  
  // didMove(to:) is equivalent to viewDidLoad()
  override func didMove(to view: SKView) {
    // Here we get the image we want to set as background
    let background = SKSpriteNode(imageNamed: "background.jpg")
    // We are gonna center it just in the center of the screen
    background.position = CGPoint(x: 512, y: 384)
    // This determines how a node is drawn, replace is for "just draw it, ignoring any alpha values"
    background.blendMode = .replace
    // This is to draw our background behing everything else
    background.zPosition = -1
    // This add our node (image) to the scene
    addChild(background)
    
    makeSlot(at: CGPoint(x: 128, y: 0), isGood: true)
    makeSlot(at: CGPoint(x: 384, y: 0), isGood: false)
    makeSlot(at: CGPoint(x: 640, y: 0), isGood: true)
    makeSlot(at: CGPoint(x: 896, y: 0), isGood: false)
    
    // This lines will create a bouncer on scene.
    makeBouncer(at: CGPoint(x: 0, y: 0))
    makeBouncer(at: CGPoint(x: 256, y: 0))
    makeBouncer(at: CGPoint(x: 512, y: 0))
    makeBouncer(at: CGPoint(x: 768, y: 0))
    makeBouncer(at: CGPoint(x: 1024, y: 0))
    
    // Delegate to handle the collisions
    physicsWorld.contactDelegate = self
    
    // This add a physics body to the whole scene that is a line on each edge, that acts as a container for the scene.
    physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
    
    // Handle the LABEL
    scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
    scoreLabel.text = "Score: 0"
    scoreLabel.horizontalAlignmentMode = .right
    scoreLabel.position = CGPoint(x: 980, y: 700)
    addChild(scoreLabel)
    
    // Handle the EditLabel
    editLabel = SKLabelNode(fontNamed: "Chalkduster")
    editLabel.text = "Edit"
    editLabel.position = CGPoint(x: 80, y: 700)
    addChild(editLabel)
  }
  
  // MARK: - Functions
  
  func makeBouncer(at position: CGPoint) {
    // This just generate a node that is an image on our bundle, bounce
    let bouncer = SKSpriteNode(imageNamed: "bouncer")
    // This is to put the node just in the given position.
    bouncer.position = position
    // This will add a physics body to our bouncer
    bouncer.physicsBody = SKPhysicsBody(circleOfRadius: bouncer.size.width / 2)
    // This in false, will let our node static, otherwise, the object will be moved by the physics simulator based on gravity and collisions
    bouncer.physicsBody?.isDynamic = false
    // This is to make the bouncer appears on the scene
    addChild(bouncer)
  }
  
  func makeSlot(at position: CGPoint, isGood: Bool) {
    var slotBase: SKSpriteNode
    var slotGlow: SKSpriteNode
    
    if isGood {
      slotBase = SKSpriteNode(imageNamed: "slotBaseGood")
      slotGlow = SKSpriteNode(imageNamed: "slotGlowGood")
      slotBase.name = "good"
    } else {
      slotBase = SKSpriteNode(imageNamed: "slotBaseBad")
      slotGlow = SKSpriteNode(imageNamed: "slotGlowBad")
      slotBase.name = "bad"
    }
    
    slotBase.position = position
    slotGlow.position = position
    
    slotBase.physicsBody = SKPhysicsBody(rectangleOf: slotBase.size)
    slotBase.physicsBody?.isDynamic = false
    
    addChild(slotBase)
    addChild(slotGlow)
    
    let spin = SKAction.rotate(byAngle: .pi, duration: 10)
    let spinForever = SKAction.repeatForever(spin)
    slotGlow.run(spinForever)
  }
  
  func collisionBetween(ball: SKNode, object: SKNode) {
    if object.name == "good" {
      destroy(ball: ball)
      score += 1
    } else if object.name == "bad" {
      destroy(ball: ball)
      score -= 1
    }
  }
  
  func destroy(ball: SKNode) {
    if let fireParticles = SKEmitterNode(fileNamed: "FireParticles") {
      fireParticles.position = ball.position
      addChild(fireParticles)
    }
    ball.removeFromParent()
  }
  
  // MARK: - Touches Handler
  
  // This function appears on UIKit, as SpriteKit too, this will handle the touch events on your device
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    // Here we get where any of the screen touches
    if let touch = touches.first {
      // Here we find where the screen was touches in relation to self
      let location = touch.location(in: self)
      
      let objects = nodes(at: location)
      if objects.contains(editLabel) {
        editingMode = !editingMode
      } else {
        if editingMode {
          let size = CGSize(width: GKRandomDistribution(lowestValue: 16, highestValue: 128).nextInt(), height: 16)
          let box = SKSpriteNode(color: RandomColor(), size: size)
          box.zRotation = RandomCGFloat(min: 0, max: 3)
          box.position = location
          box.physicsBody = SKPhysicsBody(rectangleOf: box.size)
          box.physicsBody?.isDynamic = false
          addChild(box)
        } else {
          // This just generate a node that is an image on our bundle
          let ball = SKSpriteNode(imageNamed: "ballRed")
          // This will add a physics body to our ball
          ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.size.width / 2.0)
          // This is to let us know about every collision
          ball.physicsBody!.contactTestBitMask = ball.physicsBody!.collisionBitMask
          // This is for the bounciness... its level goes from 0 to 1
          ball.physicsBody?.restitution = 0.4
          // This gives a location for the new created box before
          ball.position = location
          // This will give a name that let us use as identifier for our ball
          ball.name = "ball"
          // This is to make the box appears on the scene
          addChild(ball)
        }
      }
    }
  }
  
  // MARK: - SKPhysicsContact Delegate
  
  func didBegin(_ contact: SKPhysicsContact) {
    guard let nodeA = contact.bodyA.node,
      let nodeB = contact.bodyB.node else { return }
    
    if nodeA.name == "ball" {
      collisionBetween(ball: nodeA, object: nodeB)
    } else if nodeB.name == "ball" {
      collisionBetween(ball: nodeB, object: nodeA)
    }
  }
}
