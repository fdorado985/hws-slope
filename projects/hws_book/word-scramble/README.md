#  Word Scramble

## Description
Create an anagram game, create different words with the letter of a given word, also you see will see closures and booleans in this project.

## Wrap Up

On this project you will see:
- How to add `textFields` to `UIAlertController`
- More about:
  - `String`
  - `Closures`
  - Method `return` values
  - `Booleans`
  - `NSRange` and more

## Demo
![word_scramble_demo](screenshots/word_scramble_demo.gif)
