//
//  ViewController.swift
//  word-scramble
//
//  Created by Juan Francisco Dorado Torres on 6/17/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import GameplayKit

class MainVC: UITableViewController {
  
  // MARK: - Properties
  
  var allWords = [String]()
  var usedWords = [String]()
  
  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(promptForAnswer))
    
    if let startWordsPath = Bundle.main.path(forResource: "start", ofType: "txt"),
      let startWords = try? String(contentsOfFile: startWordsPath) {
        allWords = startWords.components(separatedBy: "\n")
    } else {
      loadDefaultWords()
    }
    
    startGame()
  }
  
  // MARK: - Functions
  
  func loadDefaultWords() {
    allWords = ["silkworm"]
  }
  
  func startGame() {
    allWords = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: allWords) as! [String]
    title = allWords[0]
    usedWords.removeAll(keepingCapacity: true)
    tableView.reloadData()
  }
  
  @objc func promptForAnswer() {
    let alert = UIAlertController(title: "Enter answer", message: nil, preferredStyle: .alert)
    alert.addTextField()
    
    let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned self, alert] (action) in
      let answer = alert.textFields![0]
      self.submit(answer: answer.text!)
    }
    
    alert.addAction(submitAction)
    present(alert, animated: true)
  }
  
  func submit(answer: String) {
    let lowerAnswer = answer.lowercased()
    
    if isPossible(word: lowerAnswer) {
      if isOriginal(word: lowerAnswer) {
        if isReal(word: lowerAnswer) {
          usedWords.insert(answer, at: 0)
          let indexPath = IndexPath(row: 0, section: 0)
          tableView.insertRows(at: [indexPath], with: .automatic)
          return
        } else {
          showErrorMessage(title: "Word not recognised", message: "You can't just make them up, you know! also, 3 letter words are not available")
        }
      } else {
        showErrorMessage(title: "Word used already", message: "Be more original!")
      }
    } else {
      showErrorMessage(title: "Word not possible", message: "You can't spell that word from '\(title!.lowercased())', neither be the same one!")
    }
  }
  
  func showErrorMessage(title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default))
    present(alert, animated: true)
  }
  
  func isPossible(word: String) -> Bool {
    var tempWord = title!.lowercased()
    
    if tempWord == word {
      return false
    }
    
    for letter in word {
      if let pos = tempWord.range(of: String(letter)) {
        tempWord.remove(at: pos.lowerBound)
      } else {
        return false
      }
    }
    
    return true
  }
  
  func isOriginal(word: String) -> Bool {
    return !usedWords.contains(word)
  }
  
  func isReal(word: String) -> Bool {
    if word.count <= 3 {
      return false
    }
    
    let checker = UITextChecker()
    let range = NSMakeRange(0, word.utf16.count)
    let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
    return misspelledRange.location == NSNotFound
  }
  
  // MARK: - TableView Delegate|Datasource
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return usedWords.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Word", for: indexPath)
    cell.textLabel?.text = usedWords[indexPath.row]
    return cell
  }
}

