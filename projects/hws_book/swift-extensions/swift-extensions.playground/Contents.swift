//: Playground - noun: a place where people can play

import UIKit

// Creating a Swift Extension

extension Int {
    
    func plusOne() -> Int {
        return self + 1
    }
    
    mutating func plusTwo() {
        self += 2
    }
}

var myInt = 0
print("Original Value: \(myInt)")

print("No Mutating Value: \(myInt.plusOne())")
print("No Mutating Specific Value: \(5.plusOne())")

print("Original Value: \(myInt)")

print("Mutating Value: \(myInt.plusTwo())")
print("Original Value: \(myInt)")


// Protocol-Oriented programming for Beginners

extension Int {
    
    func squared() -> Int {
        return self * self
    }
}

let i: Int = 8
print(i.squared())


extension BinaryInteger {
    
    func squared() -> Self {
        return self * self
    }
}

let j: UInt = 10
print(j.squared())


// Extensions for brevity

var str = "Hello World "
str = str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

print(str)


extension String {
    
    var trimmed: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    mutating func trim() {
        self = trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}

str.trim()
str.trimmed

