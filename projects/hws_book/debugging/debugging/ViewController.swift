//
//  ViewController.swift
//  debugging
//
//  Created by Juan Francisco Dorado Torres on 6/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    
    basicDebuggingPrint()
    basicDebuggingAssert()
    basicDebugging()
  }
  
  // MARK: - Functions
  
  func basicDebuggingPrint() {
    print("Using print() ===========")
    print("I'm inside the viewDidLoad() method!")
    print(1, 2, 3, 4, 5)
    print(1, 2, 3, 4, 5, separator: "-")
    print("Some message", terminator: "")
    print("\n\n")
  }
  
  func basicDebuggingAssert() {
    print("Using assert() =========")
    assert(1 == 1, "Maths failure!")
    //assert(1 == 2, "Maths failure!")
  }
  
  func basicDebugging() {
    for i in 1 ... 100 {
      print("Got number \(i)")
    }
  }
}

