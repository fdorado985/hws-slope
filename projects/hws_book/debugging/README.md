#  Debugging

### Description
Everyone hits problems sooner or later, so learning to find and fix them is an important skill.

### Using Breakpoints
If this is completely fill color the breakpoint is enable
![breakpoint_enable](screenshots/breakpoint_enable.png)

If this is a little bit transparent, the breakpoint exists but is not enable
![breakpoint_disable](screenshots/breakpoint_disable.png)

### Edit Breakpoints
Right click on the breakpoint and choose edit breakpoint
![edit_breakpoint](screenshots/edit_breakpoint.png)

When you edit breakpoints you can add conditions to it
![add_condition_to_breakpoint](screenshots/add_condition_to_breakpoint.png)

### Add Breakpoint to Fatal Errors
If you have a fatal error must of the time you cannot see what is the error but using this exceptionBreakpoint the code will stop just in the line code where it crashes.
![add_exception_breakpoint](screenshots/add_exception_breakpoint.png)
![all_exceptions_breakpoint](screenshots/all_exceptions_breakpoint.png)

### View Hierarchy
One of the most useful stuffs in debugging is the view hierarchy... you can see all the views in a 3D model
![view_hierarchy](screenshots/view_hierarchy.png)

### Some commands for LLDB
- `p <variable>` - This prints the value of a variable

## Wrap Up
You will see here some ways to debug... this is using
- `print()`
- `print(separator:)`
- `print(terminator:)`
- `assert(:)`
