//
//  ViewController.swift
//  mvvm-binding
//
//  Created by Juan Francisco Dorado Torres on 6/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

struct User {
  var name: Observable<String>
}

class ViewController: UIViewController {
  
  @IBOutlet weak var username: BoundTextField!
  
  var user = User(name: Observable("Juan Dorado"))
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    username.bind(to: user.name)
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
      self.user.name.value = "Donatello TMNT"
    }
  }
}

/** It is designed to monitor one value.
 - Important:
 Make it generic will let you extend it to other types of values.*/
class Observable<ObservedType> {
  
  /**
   Value type that will be observable.
   
   - Important:
   Stored value private to avoid other folks touch this by accident, also because of two-way bindings. Ex.
   
   - If TextField changes it needs update the model, and if the model changes it needs to update the textField.
   This can easily lead to an endless loop.
   */
  private var _value: ObservedType?
  
  /**
   This will get called when the value changed, this will send the current value to whatever is watching it.
   
   - Important:
   This avoid the infinite loop created for the two-way binding.
   */
  var valueChanged: ((ObservedType?) -> ())?
  
  /**
   This property will be manipulated safely, this one is designed to be adjusted from anywhere, and will both change _value and send its new value out to the observer using its valueChanged clousure.
   */
  public var value: ObservedType? {
    get {
      return _value
    }
    
    set {
      _value = newValue
      valueChanged?(_value)
    }
  }
  
  /// Init that will let us create easily this object.
  init(_ value: ObservedType) {
    self._value = value
  }
  
  /**
   This will let you take care when for example the textField has changed its text by the user.
   
   - Important:
   This should automatically update the value we're storing buy it mustn't do so using value otherwise the property observer will be triggered and you'll be in a loop, that's why we are gonna use this function, to modify its _value in place, that won't trigger the didSet property observer.
   */
  func bindingChanged(to newValue: ObservedType) {
    _value = newValue
    print("Value is now \(newValue)")
  }
}

class BoundTextField: UITextField {
  
  /**
   Called when the editingChanged event happens.
   
   - Important:
   We can't call it directly from the textField.
   */
  var changedClosure: (() -> ())?
  
  /**
   This let us attach it as a selector for our button, this will just let us pass the call directly to the changedClousure() call.
   */
  @objc func valueChanged() {
    changedClosure?()
  }
  
  /**
   This function will let us:
   - Accept any Observable value that stores a string underneath, because that's what our textFields work with.
   - Add the textField as its own handler for the editingChanged event, pointing it at the valueChanged() method we just wrote.
   - Set the textField's changedClosure to some code that calls the bindingChanged() method on its observed object.
   - Set the observable's valueChanged closure to some code that updates the text in the textField.
   
   - parameters:
      - observable: <T> Value to be observed
   */
  func bind(to observable: Observable<String>) {
    addTarget(self, action: #selector(BoundTextField.valueChanged), for: .editingChanged)
    
    changedClosure = { [weak self] in
      observable.bindingChanged(to: self?.text ?? "")
    }
    
    observable.valueChanged = { [weak self] newValue in
      self?.text = newValue
    }
  }
}

