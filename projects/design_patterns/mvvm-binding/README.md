#  MVVM Binding

One of the most important topics using MVVM Arquitecture is that for this... you required binding, You could if you wanted attempt MVVM without bindings, but then by definition it's not MVVM.
Instead, you're probably closer to the Presentation Model - That's not necessarily bad, buy I think it's important to be aware of the distinction.

Here you will see how a class works to do this binding work.

### Demo
![Demo](screenshots/mvvm_binding_demo.gif)
