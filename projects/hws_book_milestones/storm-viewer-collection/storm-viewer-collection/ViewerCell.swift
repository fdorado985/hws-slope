//
//  ViewerCell.swift
//  storm-viewer-collection
//
//  Created by Juan Francisco Dorado Torres on 7/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewerCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var imgViewer: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    // MARK: - Configure Cell
    
    func configureCell(img: UIImage?, name: String) {
        imgViewer.image = img
        lblName.text = name
    }
}
