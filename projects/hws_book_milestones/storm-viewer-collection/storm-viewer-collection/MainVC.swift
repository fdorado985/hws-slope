//
//  MainVC.swift
//  storm-viewer-collection
//
//  Created by Juan Francisco Dorado Torres on 7/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ViewerCell"

class MainVC: UICollectionViewController {
    
    // MARK: - Properties
    
    var pictures = [String]()

    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Storm Viewer"
        navigationController?.navigationBar.prefersLargeTitles = true
        getImages()
    }
    
    // MARK: - Public
    
    func getImages() {
        let fm = FileManager.default
        if let path = Bundle.main.resourcePath,
            let items = try? fm.contentsOfDirectory(atPath: path) {
            for item in items {
                if item.hasPrefix("nssl") {
                    pictures.append(item)
                    pictures.sort()
                }
            }
        }
    }

    // MARK: - UICollectionView Delegate|DataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictures.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? ViewerCell else { return UICollectionViewCell() }
        
        let name = pictures[indexPath.row]
        let image = UIImage(named: name)
        cell.configureCell(img: image, name: name)
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let name = pictures[indexPath.row]
        let image = UIImage(named: name)
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "DetailVC") as? DetailVC else { return }
        vc.name = name
        vc.image = image
        navigationController?.pushViewController(vc, animated: true)
    }

}
