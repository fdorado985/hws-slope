//
//  DetailVC.swift
//  storm-viewer-collection
//
//  Created by Juan Francisco Dorado Torres on 7/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var viewerImage: UIImageView!
    
    // MARK: - Properties
    
    var name: String?
    var image: UIImage?
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = name
        viewerImage.image = image
        
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.hidesBarsOnTap = false
    }
    
    // MARK: - Hides Home Indicator
    
    override func prefersHomeIndicatorAutoHidden() -> Bool {
        return navigationController?.hidesBarsOnTap ?? false
    }

}
