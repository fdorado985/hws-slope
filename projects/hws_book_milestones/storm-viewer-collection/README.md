#  Storm Viewer - Collection
This is gonna be a copy of the `Storm Viewer` app the difference is that we are gonna use a `UICollectionViewController` instead a `UITableViewController`.

## Instructions
Easy... take the original `Storm Viewer` app and replace the `UITableViewController` with a `UICollectionViewController`.
