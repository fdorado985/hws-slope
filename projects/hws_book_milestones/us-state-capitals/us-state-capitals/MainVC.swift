//
//  MainVC.swift
//  us-state-capitals
//
//  Created by Juan Francisco Dorado Torres on 7/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UITableViewController {
    
    // MARK: - Properties
    
    var states = [State]()
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        getCountries()
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    // MARK: - Public
    
    func getCountries() {
        guard let path = Bundle.main.path(forResource: "us-state-capitals", ofType: "json"), let jsonString = try? String(contentsOfFile: path) else { return }
        
        let json = JSON(parseJSON: jsonString).arrayValue
        for element in json {
            let stat = element["state"].stringValue
            let city = element["city"].stringValue
            let lon = element["lon"].doubleValue
            let lat = element["lat"].doubleValue
            
            let currentState = State(state: stat, city: city, lon: lon, lat: lat)
            states.append(currentState)
        }
        
        tableView.reloadData()
    }
    
    // MARK: - UITableView Delegate|Datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return states.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StateCell", for: indexPath)
        let selectedState = states[indexPath.row]
        cell.textLabel?.text = selectedState.state
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedState = states[indexPath.row]
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "DetailVC") as? DetailVC else { return }
        vc.state = selectedState
        navigationController?.pushViewController(vc, animated: true)
    }
}

