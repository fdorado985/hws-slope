//
//  DetailVC.swift
//  us-state-capitals
//
//  Created by Juan Francisco Dorado Torres on 7/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import WebKit

class DetailVC: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var webview: WKWebView!
    
    // MARK: - Properties
    
    var state: State?
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = state?.state
        navigationItem.largeTitleDisplayMode = .never
        webview.loadHTMLString(getHTML(), baseURL: nil)
    }
    
    // MARK: - Public
    
    func getHTML() -> String {
        guard let state = state else { return "" }
        
        var html = "<html>"
        html += "<h1><strong>STATE INFO</strong></h1>"
        html += "<p><strong>State: </strong>\(state.state.uppercased())</p>"
        html += "<p><strong>Capital : </strong>\(state.city)</p>"
        html += "<h3>LOCATION</h3>"
        html += "<p><strong>Latitude : </strong>\(state.lat)</p>"
        html += "<p><strong>Longitude : </strong>\(state.lon)</p>"
        html += "</html>"
        
        return html
    }

}
