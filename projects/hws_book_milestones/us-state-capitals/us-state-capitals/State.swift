//
//  State.swift
//  us-state-capitals
//
//  Created by Juan Francisco Dorado Torres on 7/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class State {
    
    // MARK: - Properties
    
    private(set) var state: String
    private(set) var city: String
    private(set) var lon: Double
    private(set) var lat: Double
    
    // MARK: - init(:)
    
    init(state: String, city: String, lon: Double, lat: Double) {
        self.state = state
        self.city = city
        self.lon = lon
        self.lat = lat
    }
}
