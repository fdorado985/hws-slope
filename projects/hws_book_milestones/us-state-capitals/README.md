#  US State Capitals
This app will work with `JSON` inside the project to show them in a `UITableViewController`... then being shown a `Detail View` with the city and location.

## Instructions
Your challenge is to make an app that contains facts about countries: show a list of country names in a table view, then when one is tapped bring in a new screen that contains its capital city, size, population, currency, and any other facts that interest you. The type of facts you include is down to you – Wikipedia has a huge selection to choose from.

Before you start, here’s a tip that will probably be useful: - once you have a string containing `JSON`, you can convert it to a Data object using `yourString.data(using: String.Encoding.utf8)`.
