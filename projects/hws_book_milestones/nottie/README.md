#  Nottie - (Notes clone)
This is a simple clone of the `Notes` app available to save them using `UserDefaults`, also using `UIActivityController` to let share the note.

## Instructions
* Create a table view controller that lists notes. Place it inside a navigation controller.
* Tapping on a note should slide in a detail view controller that contains a full-screen text view.
* Notes should be loaded and saved using NSCoding. You can use UserDefaults if you want, or write to a file.
* Add some toolbar items to the detail view controller – “delete” and “compose” seem like good choices.
* Add an action button to the navigation bar in the detail view controller that shares the text using UIActivityViewController.

