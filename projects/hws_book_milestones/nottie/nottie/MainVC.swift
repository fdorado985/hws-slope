//
//  MainVC.swift
//  nottie
//
//  Created by Juan Francisco Dorado Torres on 7/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UITableViewController {
    
    // MARK: - Properties
    
    var notes = [Note]()
    
    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Notes"
        
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(addNoteTapped))
        let deleteBarButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteNoteTapped))
        navigationItem.rightBarButtonItems = [addBarButton, deleteBarButton]
        
        getNotes()
        tableView.reloadData()
    }
    
    // MARK: - Public
    
    @objc func addNoteTapped() {
        goToNote()
    }
    
    @objc func deleteNoteTapped() {
        tableView.isEditing = !tableView.isEditing
    }
    
    func goToNote(_ note: Note? = nil) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "NoteVC") as? NoteVC else { return }
        vc.delegate = self
        vc.note = note
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func save() {
        let jsonEncoder = JSONEncoder()
        if let savedData = try? jsonEncoder.encode(notes) {
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: "notes")
        } else {
            print("Failed to save notes.")
        }
    }
    
    func getNotes() {
        let defaults = UserDefaults.standard
        if let savedNotes = defaults.object(forKey: "notes") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                notes = try jsonDecoder.decode([Note].self, from: savedNotes)
            } catch {
                print("Failed to load notes")
            }
        }
        
        self.notes.sort { $0.date > $1.date }
    }
    
    func updateNotes(_ newNote: Note) {
        if self.notes.contains(where: { $0.date == newNote.date }) {
            self.notes.forEach {
                if $0.uuid == newNote.uuid {
                    $0.note = newNote.note
                    $0.date = newNote.date
                }
            }
        } else {
            self.notes.append(newNote)
        }
        
        self.notes.sort { $0.date > $1.date }
        save()
        tableView.reloadData()
    }
}

// MARK: - UITableView Delegate|Datasource

extension MainVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath)
        
        let selectedNote = notes[indexPath.row]
        let title = selectedNote.note.components(separatedBy: "\n").first
        cell.textLabel?.text = title
        cell.detailTextLabel?.text = selectedNote.date.description
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedNote = notes[indexPath.row]
        goToNote(selectedNote)
    }

    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            notes.remove(at: indexPath.row)
        }
        
        save()
        tableView.deleteRows(at: [indexPath], with: .none)
    }

}

// MARK: - Note Delegate

extension MainVC: NoteDelegate {
    
    func noteCreated(_ note: Note) {
        print("New note created")
        updateNotes(note)
    }
}

