//
//  NoteVC.swift
//  nottie
//
//  Created by Juan Francisco Dorado Torres on 7/21/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

protocol NoteDelegate {
    
    func noteCreated(_ note: Note)
}

class NoteVC: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var txtNote: UITextView!
    
    // MARK: - Properties
    
    var note: Note?
    var delegate: NoteDelegate?
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.largeTitleDisplayMode = .never
        let shareBarButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareNoteTapped))
        let saveBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(saveNoteTapped))
        navigationItem.rightBarButtonItems = [saveBarButton, shareBarButton]
        
        if note != nil {
            txtNote.text = note?.note
        }
    }
    
    // MARK: - Public
    
    @objc func shareNoteTapped() {
        let activityVC = UIActivityViewController(activityItems: [txtNote.text], applicationActivities: [])
        present(activityVC, animated: true)
    }
    
    @objc func saveNoteTapped() {
        if let note = note {
            note.date = Date()
            note.note = txtNote.text
        } else {
            note = Note(note: txtNote.text, date: Date())
        }
        
        delegate?.noteCreated(note!)
        navigationController?.popViewController(animated: true)
    }
}

extension NoteVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        print("Text changed: \(textView.text)")
    }
}
