//
//  Note.swift
//  nottie
//
//  Created by Juan Francisco Dorado Torres on 7/22/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Note: Codable {
    
    // MARK: - Properties
    
    let uuid: String
    var note: String
    var date: Date
    
    // MARK: - init(:)
    
    init(note: String, date: Date) {
        self.note = note
        self.date = date
        
        self.uuid = UUID().uuidString
    }
}
