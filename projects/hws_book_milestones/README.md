# Milestones
The milestones are little challenges that you can use to try what you have already learned... Here is the list of them... each one will have the instructions in case you want to do it by yourself before see the solution.

**Note:** You will not see a demo in here... why? Because it is better if you imagine what to do with the given steps instead of use a demo and try to copy.

## Challenges
1. [Learning-Swift-Language](learning-swift-language) - Simple playground to solve the Fizz Buzz test.
2. [Welcome-To-UIKit](list-flags) - List of flags to use `UITableViews`, `UIImageViews`, `UINavigationController`, `UIActivityController`.
3. [WebKit and Clousures](shopping-list) - This will be a simple shopping list where you'll be able to use `UIAlertController` and add some items on a `UITableView`.
4. [Hangman](hangman) - This app is gonna be a simple hangman game... you will use `GamePlayKit`.
5. [Storm-Viewer-Collection](storm-viewer-collection) - A clone for `Storm Viewer` where you replace `UITableViewController` with `UICollectionViewController`.
6. [US-State-Capitals](us-state-capitals) - This is a list of states in a `UITableView` using a `JSON` you can handle inside your project.
7. [Nottie](nottie) - This is a simple `Notes` clone able to use `UIActivityController` and `UserDefaults`.
