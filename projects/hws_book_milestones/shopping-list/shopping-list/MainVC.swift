//
//  MainVC.swift
//  shopping-list
//
//  Created by Juan Francisco Dorado Torres on 7/18/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UITableViewController {
    
    // MARK: - Properties
    
    var shoppingItems = [String]()
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        setupView()
    }
    
    // MARK: - Public
    
    func setupView() {
        let addAction = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addItemTapped))
        let shareAction = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(sharedListTapped))
        navigationItem.rightBarButtonItems = [addAction, shareAction]
    }
    
    @objc func addItemTapped() {
        let alert = UIAlertController(title: "New Item", message: "Type the new item to add it to the shopping list", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "New item to add..."
        }
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak self] (alertAction) in
            if let strongSelf = self {
                if let textField = alert.textFields?.first,
                    let newItem = textField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                    !newItem.isEmpty {
                    let indexPath = strongSelf.addItemToList(newItem)
                    strongSelf.tableView.insertRows(at: [indexPath], with: .automatic)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }
    
    func addItemToList(_ item: String) -> IndexPath {
        shoppingItems.append(item)
        shoppingItems.sort()
        let index = shoppingItems.index(of: item) ?? 0
        return IndexPath(row: index, section: 0)
    }
    
    @objc func sharedListTapped() {
        let fullList = shoppingItems.joined(separator: "\n")
        let vc = UIActivityViewController(activityItems: [fullList], applicationActivities: [])
        vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItems?[1]
        present(vc, animated: true)
        
    }
    
    // MARK: - UITableView Delegate|Datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shoppingItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
        
        let item = shoppingItems[indexPath.row]
        cell.textLabel?.text = item
        
        return cell
    }
}

