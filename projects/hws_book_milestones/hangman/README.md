#  Hangman
This will be a simple hangman game, where you need to guess what is the hidden word.

## Instructions
* You already know how to load a list of words from disk and choose one, because that’s exactly what we did in tutorial 5.
* You know how to prompt the user for text input, again because it was in tutorial 5. Obviously this time you should only accept single letters rather than whole words – use someString.characters.count for that.
* You can display the user’s current word and score using the title property of your view controller.
* You should create a usedLetters array as well as a wrongAnswers integer.
* When the player wins or loses, use UIAlertController to show an alert with a message.
