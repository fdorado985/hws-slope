//
//  ViewController.swift
//  hangman
//
//  Created by Juan Francisco Dorado Torres on 7/18/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import GameplayKit

class ViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var lblWord: UILabel!
    @IBOutlet weak var imgHangman: UIImageView!
    @IBOutlet weak var lbUsedLetters: UILabel!
    @IBOutlet weak var lbUsedWords: UILabel!
    
    // MARK: - Properties
    
    var allWords = [String]()
    var isGameOver = false
    var attempts = 0 {
        didSet {
            isGameOver = attempts == 7 ? true : false
        }
    }
    
    var wordToGuess = "" {
        didSet {
            wordToGuess.forEach {
                maskedWord += String($0).isAlphanumeric ? "?" : String($0)
            }
        }
    }
    
    var usedLetters = [String]() {
        didSet {
            lbUsedLetters.text = usedLetters.joined(separator: ", ")
        }
    }
    
    var usedWords = [String]() {
        didSet {
            lbUsedWords.text = usedWords.joined(separator: ", ")
        }
    }
    
    var maskedWord = "" {
        didSet {
            lblWord.text = maskedWord
        }
    }
    
    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchWordTapped))
        
        startGame()
    }
    
    // MARK: - Public
    
    func restartProperties() {
        imgHangman.image = UIImage(named: "backgroundBase")
        attempts = 0
        isGameOver = false
        usedLetters.removeAll()
        usedWords.removeAll()
        maskedWord = ""
    }
    
    func getTheWordToGuess() {
        if let path = Bundle.main.path(forResource: "words", ofType: "txt"),
            let words = try? String(contentsOfFile: path).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
            let shuffledWords = GKRandomSource().arrayByShufflingObjects(in: words.components(separatedBy: "\n")) as? [String] {
            allWords = shuffledWords
        }
        
        if !allWords.isEmpty {
            wordToGuess = allWords[0].uppercased()
        }
        
        print("Word to guess: \(wordToGuess)")
    }
    
    func startGame() {
        restartProperties()
        getTheWordToGuess()
    }
    
    func restartGame(action: UIAlertAction) {
        startGame()
    }
    
    func badAnswer() {
        attempts += 1
        if isGameOver {
            gameFinished(title: "Game Over", message: "You couldn't guess the word 😔")
        } else {
            imgHangman.fadeOut()
            imgHangman.image = UIImage(named: "hangman\(attempts)")
            imgHangman.fadeIn()
        }
    }
    
    func gameFinished(title: String, message: String? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let restartAction = UIAlertAction(title: "Play Again", style: .default, handler: restartGame)
        alert.addAction(restartAction)
        present(alert, animated: true)
    }
    
    func showGenericAlert(title: String, message: String? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok!", style: .default))
        present(alert, animated: true)
    }
    
    func searchForWord(_ typedWord: String) {
        if usedWords.contains(typedWord) || usedLetters.contains(typedWord) {
            showGenericAlert(title: "Again? 🤔", message:  "You have already used '\(typedWord)'")
        } else {
            findWord(with: typedWord.uppercased())
        }
    }
    
    func findWord(with typedWord: String) {
        if typedWord.count == 1 {
            // It is a letter
            validateLetter(typedWord)
        } else {
            // It is a word
            validateWord(typedWord)
        }
    }
    
    func validateWord(_ typedWord: String) {
        if wordToGuess == typedWord {
            maskedWord = wordToGuess
            gameFinished(title: "You have won!", message: "The word was \(wordToGuess)")
        } else {
            usedWords.append(typedWord)
            badAnswer()
        }
    }
    
    func validateLetter(_ typedLetter: String) {
        usedLetters.append(typedLetter)
        if wordToGuess.contains(typedLetter) {
            var newString = ""
            
            for counter in 0 ..< wordToGuess.count {
                let startIndex = wordToGuess.startIndex
                let indexCounter = wordToGuess.index(startIndex, offsetBy: counter)
                
                let guessChar = String(wordToGuess[indexCounter])
                let maskedChar = String(maskedWord[indexCounter])
                
                newString += (maskedChar == "?" && guessChar == typedLetter) ? guessChar : maskedChar
            }
            
            maskedWord = newString
            
            if maskedWord == wordToGuess {
                gameFinished(title: "You have won!", message: "The word was \(wordToGuess)")
            }
        } else {
            badAnswer()
        }
    }
    
    @objc func searchWordTapped() {
        let alert = UIAlertController(title: "Do you know the word?", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Type a letter or word if you know it"
        }
        alert.addAction(UIAlertAction(title: "Look for it 👀", style: .default, handler: { [weak self] (action) in
            if let strongSelf = self,
                let word = alert.textFields?.first?.text,
                !word.isEmpty {
                strongSelf.searchForWord(word)
            }
        }))
        present(alert, animated: true)
    }
    
    // MARK: - UIKeyboard
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            //bottomConstraint.constant = 50
        } else {
            //bottomConstraint.constant += keyboardViewEndFrame.height
        }
    }
}

