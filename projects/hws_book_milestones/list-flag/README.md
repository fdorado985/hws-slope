#  List Flag
This is the Milestone #2 where you will are going to see a list of flags in a `UITableView`, to see the flag and be able to share it.

## Instructions
1. Start with a Single View App template, then change its main ViewController class so that builds on UITableViewController instead.
2. Load the list of available flags from the app bundle. You can type them directly into the code if you want, but it’s preferable not to.
3. Create a new Cocoa Touch Class responsible for the detail view controller, and give it properties for its image view and the image to load.
4. You’ll also need to adjust your storyboard to include the detail view controller, including using Auto Layout to pin its image view correctly.
5. You will need to use UIActivityViewController to share your flag.
