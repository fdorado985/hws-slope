//
//  MainVC.swift
//  list-flag
//
//  Created by Juan Francisco Dorado Torres on 7/17/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UITableViewController {
    
    // MARK: - Properties
    
    var allFlags = [String]()
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        getFlags()
    }
    
    // MARK: - Public
    
    func getFlags() {
        if let flagsPath = Bundle.main.path(forResource: "flags", ofType: "txt"),
            let flags = try? String(contentsOfFile: flagsPath).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            allFlags = flags.components(separatedBy: "\n")
        }
        
        tableView.reloadData()
    }
    
    func loadFlag(_ name: String) {
        if let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailVC") as? DetailVC {
            detailVC.name = name
            navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    // MARK: - UITableView Delegate|Datasource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allFlags.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "flagcell", for: indexPath)
        
        let flag = allFlags[indexPath.row]
        cell.textLabel?.text = flag
        cell.imageView?.image = UIImage(named: flag)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let flag = allFlags[indexPath.row]
        loadFlag(flag)
    }
}

