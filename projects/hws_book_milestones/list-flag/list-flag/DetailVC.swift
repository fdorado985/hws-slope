//
//  DetailVC.swift
//  list-flag
//
//  Created by Juan Francisco Dorado Torres on 7/17/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var imgFlag: UIImageView!
    
    // MARK: - Properties
    
    var name: String?

    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(sharedTapped))
        loadFlag()
        
        
    }
    
    // MARK: - Public
    
    func loadFlag() {
        guard let name = name else { return }
        title = name.count == 2 ? name.uppercased() : name.capitalized
        imgFlag.image = UIImage(named: name)
        imgFlag.layer.borderWidth = 1
        imgFlag.layer.borderColor = UIColor.black.cgColor
    }
    
    @objc func sharedTapped() {
        let vc = UIActivityViewController(activityItems: [imgFlag.image!], applicationActivities: [])
        vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(vc, animated: true)
    }
}
