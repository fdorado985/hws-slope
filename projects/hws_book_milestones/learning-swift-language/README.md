# Fizz Buzz Challenge
### Description
This is a famous test commonly used to root out bad programmers during job interviews.

## Instructions:
* Write a function that accepts an integer as input and returns a string.
* If the integer is evenly divisible by 3 the function should return the string “Fizz”. • If the integer is evenly divisible by 5 the function should return “Buzz”.
* If the integer is evenly divisible by 3 and 5 the function should return “Fizz Buzz” • For all other numbers the function should just return the input number.

### Test
```swift
fizzbuzz(number: 3)
fizzbuzz(number: 5)
fizzbuzz(number: 15)
fizzbuzz(number: 16)
```

That should return `Fizz`, `Buzz`, `Fizz Buzz`, `16`

## Solution
The solution is on the playground... is up to you if you want to try before see the solution in here.
