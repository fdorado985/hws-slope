//: Playground - noun: a place where people can play

import UIKit

// Milestone 1
/*
 • Write a function that accepts an integer as input and returns a string.
 • If the integer is evenly divisible by 3 the function should return the string “Fizz”. • If the integer is evenly divisible by 5 the function should return “Buzz”.
 • If the integer is evenly divisible by 3 and 5 the function should return “Fizz Buzz” • For all other numbers the function should just return the input number.
 */

func fizzbuzz(value: Int) -> String {
    if value % 3 == 0 && value % 5 == 0 {
        return "Fizz Buzz"
    }
    
    if value % 5 == 0 {
        return "Buzz"
    }
    
    if value % 3 == 0 {
        return "Fizz"
    }
    
    return "\(value)"
}

fizzbuzz(value: 3)
fizzbuzz(value: 5)
fizzbuzz(value: 15)
fizzbuzz(value: 16)
